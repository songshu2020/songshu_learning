﻿
# **1、apache安装部署规划**
## **1.1、web服务器信息**

|主机名|服务器地址|安装路径|
| - | - | - |
|wy\_web1|172.1.168.42|/opt/apache2|
|wy\_web2|172.1.168.43|/opt/apache2|
## **1.2、apache安装规划**
安装软件：httpd-2.2.27.tar.gz		

安装目录：/opt/apache2

Apache 配置文件：/opt/apache2/conf/httpd.conf

Apache 主目录：/opt/apache2/htdocs

Apache 日志目录：/opt/apache2/logs

软件存放目录: /soft（上传httpd-2.2.27.tar.gz至 /soft目录）
# **2、apache安装部署**
```bash
#mkdir	/soft
#cd 	/soft
#tar  -fxvz httpd-2.2.27.tar.gz		//解压
#cd 	httpd-2.2.27/
#./configure --prefix=/opt/apache2 --enable-so --enable-mods-shared=less    //编译
# make 
# make install	//安装
```


# **3、apache启停**
\# /opt/apache2/bin/apachectl start  启动apache服务

\# /opt/apache2/bin/apachectl  stop	 停止apache服务

\# ps–ef | grephttpd查看apache服务

#netstat–an | grep 80	查看apache端口

注：修改serverName为主机名称
# **4、apache优化**
## **4.1、配置apache转发weblogic**
将weblogic转发模块mod\_wl\_22.so，拷贝到 /opt/apache2/modules/目录下

如果使用F5进行负载，打开apache配置文件，添加如下内容：

#vi /opt/apache2/conf/httpd.conf

ServerNamelocalhost:80

LoadModuleweblogic\_module modules/mod\_wl\_22.so

<Location /pweb>

<IfModulemod\_weblogic.c>

WebLogicHost 172.1.180.21

WebLogicPort 9001

MatchExpression /pweb

</IfModule>

</Location>


<Location /eweb>

<IfModulemod\_weblogic.c>

WebLogicHost 172.1.180.21

WebLogicPort 9002

MatchExpression /eweb

</IfModule>

</Location>

如果apache进行负载，打开apache配置文件，添加如下内容：

#vi /opt/apache2/conf/httpd.conf

ServerNamelocalhost:80

LoadModuleweblogic\_module modules/mod\_wl\_22.so

<Location /pweb>

<IfModulemod\_weblogic.c>

WebLogicCluster 172.1.180.22:9001,172.1.180.23:9001

MatchExpression /pweb

</IfModule>

</Location>

<Location /eweb>

<IfModulemod\_weblogic.c>

WebLogicCluster 172.1.180.22:9002,172.1.180.23:9002

MatchExpression /eweb

</IfModule>

</Location>
## **4.2、mpm性能优化**
在httpd.conf配置文件中打开配置文件httpd-mpm.conf文件的注释

httpd-mpm.conf修改如下：

vi/opt/apache2/conf/extra/httpd-mpm.conf

<IfModulempm\_prefork\_module>

StartServers        10//启动时，同时启动的进程数为10

ServerLimit         2000//Server进程限制2000   1000

MinSpareServers10//最小空闲进程数

MaxSpareServers     15//最大空闲进程数

MaxClients1000//进程最大连接数

MaxRequestsPerChild 10000//一个进程被使用10000次自动清除

</IfModule>
## **4.3、注释cgi**
打开配置文件httpd.conf文件，作如下修改：

vi/opt/apache2/conf/ httpd.conf

#<Directory "/opt/apache2/cgi-bin">

\#    AllowOverride None

\#    Options None

\#    Order allow,deny

\#    Allow from all

#</Directory>
## **4.4、目录遍历**
打开配置文件httpd.conf文件，作如下修改：

vi/opt/apache2/conf/ httpd.conf

<Directory "/opt/apache2/htdocs">

`    `Options -FollowSymLinks -Indexes

AllowOverride None

`    `Order allow,deny

`    `Allow from all

</Directory>
## **4.5、日志优化**
打开配置文件httpd.conf文件，作如下修改：

vi/opt/apache2/conf/ httpd.conf

#ErrorLog "logs/error\_log"			//将此行注释

#CustomLog "logs/access\_log" common	//将此行注释

在httpd.conf文件末尾添加如下内容：

ErrorLog "|/opt/apache2/bin/rotatelogs/opt/apache2/logs/errorlog.%y%m%d 86400 +480"

CustomLog "|/opt/apache2/bin/rotatelogs/opt/apache2/logs/accesslog.%y%m%d 86400 +480" common

## **4.6、关闭漏扫漏洞**
TraceEnable off （禁止trace）

ServerSignature off(telnet不显示apache 版本)

ServerTokens Prod (不显示apache版本号)


# **5 设置跳转页面**
/opt/apache2/htdocs/index.html

```html
<!DOCTYPE html PUBliC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta http-equiv="refresh" content="0;URL=https://www.baidu.com/">
<title>一户通</title>
</head>
<body>

</body>
</html>
```



# **6、标准的proxy代理安装**
./configure --prefix=/opt/apache2 --enable-so --enable-mods-shared=less -enable-proxy=shared



# **by zkl 操作系统Suse自带apache安装**
若service  apache2 start可以执行，说明Apache2已安装，直接修改httpd.conf即可，启动若报错，执行命令  

```bash
SuSE-SWSB-WEB-01:/etc/apache2 # service apache2 start
Starting httpd2 (prefork) Syntax error on line 58 of /etc/apache2/httpd.conf:
Invalid command 'ProxyRequests', perhaps misspelled or defined by a module not included in the server configuration

The command line was:
/usr/sbin/httpd2-prefork -f /etc/apache2/httpd.conf
                                                                                                                                                  failed
SuSE-SWSB-WEB-01:/etc/apache2 # service apache2 status
Checking for httpd2:                                                                                                                              unused
SuSE-SWSB-WEB-01:/etc/apache2 # a2enmod proxy       --启动proxy
执行reload命令：/etc/init.d/apache2 reload
SuSE-SWSB-WEB-01:/etc/apache2 # service apache2 status
Checking for httpd2:                                                                                                                              unused
SuSE-SWSB-WEB-01:/etc/apache2 # service apache2 start
Starting httpd2 (prefork) httpd2-prefork: Could not reliably determine the server's fully qualified domain name, using 172.16.51.27 for ServerName
                                                                                                                                                  done
SuSE-SWSB-WEB-01:/etc/apache2 #

SuSE-SWSB-WEB-01:/var/log/apache2 # a2enmod proxy_http    --启动proxy_http
```

