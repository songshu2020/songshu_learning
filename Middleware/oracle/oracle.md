# Oracle 数据库备份恢复

```bash
exp命令导出
可以全部导出，也可部分导出。
导出部分： 指定tables=
           指定query=
只导出表结构，存储过程，不到处数据   ： rows=N
如：exp  scott/tiger tables=emp query=\”wherejob=\’salesman\’ and sal\<1600\”
```



# Oracle 查看创建实例名

```bash
oracle@linux-DB:~> env| grep ORACLE_SID
ORACLE_SID=ipcc
创建oracle实例：以oracle用户登录服务器，输入命令 dbca 就可以调出“创建数据库实例窗口”
命令 netca   配置客户端
```



# Oracle数据库密码过期解锁问题

```bash
查看多少天后过期：SELECT * FROM dba_profiles s WHERE s.profile='DEFAULT' AND resource_name='PASSWORD_LIFE_TIME';
将密码有效期由默认的180天修改成“无限制”：
　　sql>ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;

如果报“ORA-28000:用户已被锁”，解锁
sqlplus sys/orcl as sysdba
SQL > alter user db_user account unlock;
SQL > commit;
修改用户webagent密码为webagent
SQL >alter user webagent identified by webagent;
SQL >commit;
```

# 锁表查看

```sql
1.--查询锁表的SQL:gv$session  gv$lock必须加g，全局4个节点上都有
SELECT a.username,decode(b.type,'TM','TABLE LOCK','TX','ROW LOCK',NULL) LOCK_LEVEL,  
 c.owner,c.object_name,c.object_type,  
 a.sid,a.serial#,a.terminal,a.machine,a.program,a.osuser  
FROM gv$session a,gv$lock b,dba_objects c  
WHERE b.sid = a.sid  
AND b.id1 = c.object_id(+)  
AND a.username is NOT Null; 
2.--查看正在执行的SQL
select a.USERNAME,a.SID,b.SQL_TEXT,b.SQL_FULLTEXT
 from gv$session a, gv$sqlarea b
where a.sql_address = b.ADDRESS;
3.--杀掉锁表的进程:
--alter system kill session 'sid,serial#';

/*
工程：解锁对象锁定
日期：2013-6-8
版本：v1.0
描述：解锁对象锁定
注意：使用sys管理员以sysdba角色登录
*/
--1.查询哪些用户下的哪些对象已锁定
select a.sid, b.serial#, 
decode(a.type, 
      'MR', 'Media Recovery', 
      'RT', 'Redo Thread', 
      'UN', 'User Name', 
      'TX', 'Transaction', 
      'TM', 'DML', 
      'UL', 'PL/SQL User Lock', 
      'DX', 'Distributed Xaction', 
      'CF', 'Control File', 
      'IS', 'Instance State', 
      'FS', 'File Set', 
      'IR', 'Instance Recovery', 
      'ST', 'Disk Space Transaction', 
      'TS', 'Temp Segment', 
      'IV', 'Library Cache Invalida-tion', 
      'LS', 'Log Start or Switch', 
      'RW', 'Row Wait', 
      'SQ', 'Sequence Number', 
      'TE', 'Extend Table', 
      'TT', 'Temp Table', 
      'Unknown') locktype, 
    c.object_name, 
    b.username, 
    b.osuser, 
decode(a.lmode, 0, 'None', 
                1, 'Null', 
                2, 'Row-S', 
                3, 'Row-X', 
                4, 'Share', 
                5, 'S/Row-X', 
                6, 'Exclusive', 'Unknown') lockmode, 
b.machine,d.spid,
'alter system kill session '''||a.sid||','||b.serial#||''';' unlocksql
from v$lock a,v$session b,all_objects c,v$process d 
where a.sid=b.sid and a.type in ('TM','TX') 
and c.object_id=a.id1 
and b.paddr=d.addr;

--2.用于解锁 alter system kill session 'SID,SERIAL#'
--alter system kill session 'sid,serial#';
```

# 数据库todate

```sql
#################    学习总结    ##########################
1.to_date学习
 select to_date('2014-04-20 01:00:00','yyyy-mm-dd hh24:mi:ss')  from dual
2.数据库long与日期互转
SELECT SYSDATE, (SYSDATE - to_date('1970-01-01','yyyy-mm-dd'))*24*60*60*1000 current_milli FROM dual
SELECT TO_date('1970-01-01 00:00:00','yyyy-mm-dd hh24:mi:ss') +  t.createdtime/1000/60/60/24 ,t.* FROM cc_lzbid t 
3.数据库表空间无法自动拓展报错（无法通过 1024 (在表空间 USERS 中) 扩展 ORA-01652）解决办法
 SELECT * FROM dba_temp_files
 ALTER DATABASE  tempfile '/u02/oradata/authplatfrom/temp01.dbf' AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED ;
 ALTER DATABASE  DATAFILE '/u02/oradata/authplatfrom/users01.dbf' AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED 
4.查询表空间使用情况 
  SELECT tablespace_name ,file_id, file_name ,round(bytes/(1024*1024),00) total_space 
   FROM  Dba_Data_Files 
   ORDER BY file_id 
   查看表空间是否具有自动扩展的能力：
		   SELECT T.TABLESPACE_NAME,D.FILE_NAME,
		D.AUTOEXTENSIBLE,D.BYTES,D.MAXBYTES,D.STATUS
		FROM DBA_TABLESPACES T,DBA_DATA_FILES D
		WHERE T.TABLESPACE_NAME =D.TABLESPACE_NAME
		ORDER BY TABLESPACE_NAME,FILE_NAME;
	----查询表空间使用情况---
			使用DBA权限登陆
			SELECT UPPER(F.TABLESPACE_NAME) "表空间名",
			D.TOT_GROOTTE_MB "表空间大小(M)",
			D.TOT_GROOTTE_MB - F.TOTAL_BYTES "已使用空间(M)",
			TO_CHAR(ROUND((D.TOT_GROOTTE_MB - F.TOTAL_BYTES) / D.TOT_GROOTTE_MB * 100,2),'990.99') "使用比",
			F.TOTAL_BYTES "空闲空间(M)",
			F.MAX_BYTES "最大块(M)"
			FROM (SELECT TABLESPACE_NAME,
			ROUND(SUM(BYTES) / (1024 * 1024), 2) TOTAL_BYTES,
			ROUND(MAX(BYTES) / (1024 * 1024), 2) MAX_BYTES
			FROM SYS.DBA_FREE_SPACE
			GROUP BY TABLESPACE_NAME) F,
			(SELECT DD.TABLESPACE_NAME,
			ROUND(SUM(DD.BYTES) / (1024 * 1024), 2) TOT_GROOTTE_MB
			FROM SYS.DBA_DATA_FILES DD
			GROUP BY DD.TABLESPACE_NAME) D
			WHERE D.TABLESPACE_NAME = F.TABLESPACE_NAME
			ORDER BY 4 DESC;
	---查询表使用情况：
	SELECT t.segment_name,(t.bytes/1024/1024) FROM dba_segments t WHERE t.segment_type='TABLE' AND t.owner <> 'SYS' ORDER BY 2 DESC 
	
5.replace用法：将字段中的'"'替换为空
	UPDATE IP_ADDRESS t SET LOCATION = replace(t.location,'"','') WHERE t.v_start = '3740490513'
6.instr用法
	SELECT instr(substr(t.comp,2,length(t.comp)-2),'"') AS zkl,t.* FROM IP_ADDRESS t
7.debug 存储过程报错ORA-0131的解决办法
	GRANT DEBUG ANY PROCEDURE , DEBUG CONNECT SESSION TO sunuser;
8、查看数据库编码
	SELECT userenv('language') FROM dual;
9、查看sequence下一个值
	select ACTIVEINFOSEQ.NEXTVAL from dual
10、bioffice统计SQL
	select * from (
	select 'PC端申购总金额',cast(sum(amount)/100000000 as decimal(10,4)) aa,'1' ee from pjnl where transcode = 'BuyApply' and jnlstate = '0' union
	select '移动端申购总金额',cast(sum(amount)/100000000 as decimal(10,4)) ba,'2' from mjnl where transcode = 'ZX0013' and jnlstate = '0' union
	select 'PC端赎回总金额',cast(sum(amount)/100000000 as decimal(10,4)) ca,'3' from pjnl where transcode = 'RedeemApply' and jnlstate = '0' union
	select '移动端赎回总金额',cast(sum(amount)/100000000 as decimal(10,4)) da,'4' from mjnl where transcode = 'ZX0014' and jnlstate = '0' union
	select '百合宝保有量金额',cast(sum(hold)/100000000 as decimal(10,4)) ea,'5' from pcifprofit where navdate = to_date((select to_char(a.aa,'yyyy-MM-dd') from (select distinct(navdate) aa from pcifprofit order by navdate desc) a where rownum = 1),'yyyy-MM-dd')
	) order by ee

	select a.aa 日期,a.ab 注册人数 from (select to_char(opendate,'yyyy-mm-dd') aa,count(distinct(cifseq)) ab from puser  group by to_char(opendate,'yyyy-mm-dd') order by to_char(opendate,'yyyy-mm-dd') desc) a where rownum < '31'
11、查询数据库索引
		SELECT * FROM user_indexes 
----检索出重复数据
SELECT * FROM tcustlist a WHERE ROWID > (SELECT MIN(ROWID) FROM tcustlist b WHERE  b.atelnum=a.atelnum )		

12、ORA-12704字符集不匹配 解决办法（CAST('无' AS NVARCHAR2(100))）
SELECT T.LZBID,
       T.MOBILEP,
       T.IDNAME,
       CASE 
         WHEN T.CERTIFN IS NULL THEN
          CAST('无' AS NVARCHAR2(100))
         ELSE
          CAST(t.certifn AS NVARCHAR2(100))
       END FROM CC_LZBID t WHERE t.SOURCE = '3d100'
		
#############查看用户和默认表空间的关系
select username,default_tablespace from dba_users;
#############查看当前用户能访问的表
select * from user_tables; 
#############Oracle查询用户表
select * from user_all_tables;
select table_name,status from user_all_tables;

#############Oracle查询用户视图
select * from user_views;
#############查询所有函数和储存过程：
select * from user_source;
#############查询所有用户：
select * from all_users;
#############select * from dba_users
#############查看当前用户连接：
select * from v$Session;
#############查看用户角色
SELECT * FROM USER_ROLE_PRIVS;
#############查看当前用户权限：
select * from session_privs;
#############20150206 VTM L010交易不能插库解决办法
GRANT RESOURCE TO webagent;
grant create session to zkl;

#############查看所有用户所拥有的角色
SELECT * FROM DBA_ROLE_PRIVS;
#############查看所有角色
select * from dba_roles;
#############查看数据库名
SELECT NAME FROM V$DATABASE;
#############查看UNDO表空间使用情况
SELECT TABLESPACE_NAME,
       STATUS,
       SUM(BYTES) / 1024 / 1024 "Bytes(M)"  FROM DBA_UNDO_EXTENTS  GROUP BY TABLESPACE_NAME,
       STATUS;
#############查看所有表空间使用情况
select a.file_id "FileNo",
       a.tablespace_name "Tablespace_name",
       a.bytes "Bytes",
       a.bytes - sum(nvl(b.bytes, 0)) "Used",
       sum(nvl(b.bytes, 0)) "Free",
       sum(nvl(b.bytes, 0)) / a.bytes * 100 "%free"
  from dba_data_files a, dba_free_space b
 where a.file_id = b.file_id(+)
 group by a.tablespace_name, a.file_id, a.bytes
 order by a.tablespace_name;

############# 查看oracle数据库状态 ##############
 SELECT status from v$instance;
 
 #############查看字符的ASCII码
 SELECT ascii('0') FROM dual;

############### 创建Job ######################
DECLARE call_move_logindata_pro NUMBER;
begin
  SYS.dbms_job.submit(call_move_logindata_pro,
                      'movedata_cc_logins_store;',
                      to_date('13-07-2016 2:30:00', 'dd-mm-yyyy hh24:mi:ss'),
                      'trunc(sysdate)+1+(2*60+30)/(24*60)');     ----每天凌晨2点执行此任务
  commit;
end;
/
##########################################################
 
 
1. 日期和字符转换函数用法（to_date,to_char）
         
select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') as nowTime from dual;   //日期转化为字符串  
SELECT to_char(systimestamp,'yyyy-MM-dd hh24:mi:ss.ff') FROM dual;
select to_char(sysdate,'yyyy') as nowYear   from dual;   //获取时间的年   
select to_char(sysdate,'mm')    as nowMonth from dual;   //获取时间的月   
select to_char(sysdate,'dd')    as nowDay    from dual;   //获取时间的日   
select to_char(sysdate,'hh24') as nowHour   from dual;   //获取时间的时   
select to_char(sysdate,'mi')    as nowMinute from dual;   //获取时间的分   
select to_char(sysdate,'ss')    as nowSecond from dual;   //获取时间的秒 
    
select to_date('2004-05-07 13:23:44','yyyy-mm-dd hh24:mi:ss')    from dual//
2.      
    select to_char( to_date(222,'J'),'Jsp') from dual      
    
    显示Two Hundred Twenty-Two   
3.求某天是星期几      
   select to_char(to_date('2002-08-26','yyyy-mm-dd'),'day') from dual;      
   星期一      
   select to_char(to_date('2002-08-26','yyyy-mm-dd'),'day','NLS_DATE_LANGUAGE = American') from dual;      
   monday      
   设置日期语言      
   ALTER SESSION SET NLS_DATE_LANGUAGE='AMERICAN';      
   也可以这样      
   TO_DATE ('2002-08-26', 'YYYY-mm-dd', 'NLS_DATE_LANGUAGE = American')   
4. 两个日期间的天数      
    select floor(sysdate - to_date('20020405','yyyymmdd')) from dual;   
5. 时间为null的用法      
   select id, active_date from table1      
   UNION      
   select 1, TO_DATE(null) from dual;      
   
   注意要用TO_DATE(null)   
6.月份差   
   a_date between to_date('20011201','yyyymmdd') and to_date('20011231','yyyymmdd')      
   那么12月31号中午12点之后和12月1号的12点之前是不包含在这个范围之内的。      
   所以，当时间需要精确的时候，觉得to_char还是必要的 
      
7. 日期格式冲突问题      
    输入的格式要看你安装的ORACLE字符集的类型, 比如: US7ASCII, date格式的类型就是: '01-Jan-01'      
    alter system set NLS_DATE_LANGUAGE = American      
    alter session set NLS_DATE_LANGUAGE = American      
    或者在to_date中写      
    select to_char(to_date('2002-08-26','yyyy-mm-dd'),'day','NLS_DATE_LANGUAGE = American') from dual;      
    注意我这只是举了NLS_DATE_LANGUAGE，当然还有很多，      
    可查看      
    select * from nls_session_parameters      
    select * from V$NLS_PARAMETERS   
8.      
   select count(*)      
   from ( select rownum-1 rnum      
       from all_objects      
       where rownum <= to_date('2002-02-28','yyyy-mm-dd') - to_date('2002-      
       02-01','yyyy-mm-dd')+1      
      )      
   where to_char( to_date('2002-02-01','yyyy-mm-dd')+rnum-1, 'D' )      
        not in ( '1', '7' )      
   
   查找2002-02-28至2002-02-01间除星期一和七的天数      
   在前后分别调用DBMS_UTILITY.GET_TIME, 让后将结果相减(得到的是1/100秒, 而不是毫秒).   
9. 查找月份     
    select months_between(to_date('01-31-1999','MM-DD-YYYY'),to_date('12-31-1998','MM-DD-YYYY')) "MONTHS" FROM DUAL;      
    1      
   select months_between(to_date('02-01-1999','MM-DD-YYYY'),to_date('12-31-1998','MM-DD-YYYY')) "MONTHS" FROM DUAL;      
    1.03225806451613 
       
10. Next_day的用法      
    Next_day(date, day)      
    
    Monday-Sunday, for format code DAY      
    Mon-Sun, for format code DY      
    1-7, for format code D   
11      
   select to_char(sysdate,'hh:mi:ss') TIME from all_objects      
   注意：第一条记录的TIME 与最后一行是一样的      
   可以建立一个函数来处理这个问题      
   create or replace function sys_date return date is      
   begin      
   return sysdate;      
   end;      
   
   select to_char(sys_date,'hh:mi:ss') from all_objects;   
     
12.获得小时数      
     extract()找出日期或间隔值的字段值
    SELECT EXTRACT(HOUR FROM TIMESTAMP '2001-02-16 2:38:40') from offer      
    SQL> select sysdate ,to_char(sysdate,'hh') from dual;      
    
    SYSDATE TO_CHAR(SYSDATE,'HH')      
    -------------------- ---------------------      
    2003-10-13 19:35:21 07      
    
    SQL> select sysdate ,to_char(sysdate,'hh24') from dual;      
    
    SYSDATE TO_CHAR(SYSDATE,'HH24')      
    -------------------- -----------------------      
    2003-10-13 19:35:21 19   
       
13.年月日的处理      
   select older_date,      
       newer_date,      
       years,      
       months,      
       abs(      
        trunc(      
         newer_date-      
         add_months( older_date,years*12+months )      
        )      
       ) days 
       
   from ( select      
        trunc(months_between( newer_date, older_date )/12) YEARS,      
        mod(trunc(months_between( newer_date, older_date )),12 ) MONTHS,      
        newer_date,      
        older_date      
        from ( 
              select hiredate older_date, add_months(hiredate,rownum)+rownum newer_date     
              from emp 
             )      
      )   
14.处理月份天数不定的办法      
   select to_char(add_months(last_day(sysdate) +1, -2), 'yyyymmdd'),last_day(sysdate) from dual   
16.找出今年的天数      
   select add_months(trunc(sysdate,'year'), 12) - trunc(sysdate,'year') from dual   
   闰年的处理方法      
   to_char( last_day( to_date('02'    | | :year,'mmyyyy') ), 'dd' )      
   如果是28就不是闰年   
17.yyyy与rrrr的区别      
   'YYYY99 TO_C      
   ------- ----      
   yyyy 99 0099      
   rrrr 99 1999      
   yyyy 01 0001      
   rrrr 01 2001   
18.不同时区的处理      
   select to_char( NEW_TIME( sysdate, 'GMT','EST'), 'dd/mm/yyyy hh:mi:ss') ,sysdate      
   from dual;   
19.5秒钟一个间隔      
   Select TO_DATE(FLOOR(TO_CHAR(sysdate,'SSSSS')/300) * 300,'SSSSS') ,TO_CHAR(sysdate,'SSSSS')      
   from dual   
   2002-11-1 9:55:00 35786      
   SSSSS表示5位秒数   
20.一年的第几天      
   select TO_CHAR(SYSDATE,'DDD'),sysdate from dual
        
   310 2002-11-6 10:03:51   
21.计算小时,分,秒,毫秒      
    select      
     Days,      
     A,      
     TRUNC(A*24) Hours,      
     TRUNC(A*24*60 - 60*TRUNC(A*24)) Minutes,      
     TRUNC(A*24*60*60 - 60*TRUNC(A*24*60)) Seconds,      
     TRUNC(A*24*60*60*100 - 100*TRUNC(A*24*60*60)) mSeconds      
    from      
    (      
     select      
     trunc(sysdate) Days,      
     sysdate - trunc(sysdate) A      
     from dual      
   )   

   select * from tabname      
   order by decode(mode,'FIFO',1,-1)*to_char(rq,'yyyymmddhh24miss');      
   
   //      
   floor((date2-date1) /365) 作为年      
   floor((date2-date1, 365) /30) 作为月      
   d(mod(date2-date1, 365), 30)作为日.
23.next_day函数      返回下个星期的日期,day为1-7或星期日-星期六,1表示星期日
   next_day(sysdate,6)是从当前开始下一个星期五。后面的数字是从星期日开始算起。      
   1 2 3 4 5 6 7      
   日 一 二 三 四 五 六    
   
   --------------------------------------------------------------- 
   
   select    (sysdate-to_date('2003-12-03 12:55:45','yyyy-mm-dd hh24:mi:ss'))*24*60*60 from ddual
   日期 返回的是天 然后 转换为ss
     
24,round[舍入到最接近的日期](day:舍入到最接近的星期日)
   select sysdate S1,
   round(sysdate) S2 ,
   round(sysdate,'year') YEAR,
   round(sysdate,'month') MONTH ,
   round(sysdate,'day') DAY from dual
25,trunc[截断到最接近的日期,单位为天] ,返回的是日期类型
   select sysdate S1,                     
     trunc(sysdate) S2,                 //返回当前日期,无时分秒
     trunc(sysdate,'year') YEAR,        //返回当前年的1月1日,无时分秒
     trunc(sysdate,'month') MONTH ,     //返回当前月的1日,无时分秒
     trunc(sysdate,'day') DAY           //返回当前星期的星期天,无时分秒
   from dual
26,返回日期列表中最晚日期
   select greatest('01-1月-04','04-1月-04','10-2月-04') from dual
27.计算时间差
     注:oracle时间差是以天数为单位,所以换算成年月,日
     
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03','yyyy-mm-dd hh24:mi:ss'))/365) as spanYears from dual        //时间差-年
      select ceil(moths_between(sysdate-to_date('2007-11-02 15:55:03','yyyy-mm-dd hh24:mi:ss'))) as spanMonths from dual        //时间差-月
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03','yyyy-mm-dd hh24:mi:ss'))) as spanDays from dual             //时间差-天
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03','yyyy-mm-dd hh24:mi:ss'))*24) as spanHours from dual         //时间差-时
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03','yyyy-mm-dd hh24:mi:ss'))*24*60) as spanMinutes from dual    //时间差-分
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03','yyyy-mm-dd hh24:mi:ss'))*24*60*60) as spanSeconds from dual //时间差-秒
28.更新时间
     注:oracle时间加减是以天数为单位,设改变量为n,所以换算成年月,日
     select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),to_char(sysdate+n*365,'yyyy-mm-dd hh24:mi:ss') as newTime from dual        //改变时间-年
     select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),add_months(sysdate,n) as newTime from dual                                 //改变时间-月
     select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),to_char(sysdate+n,'yyyy-mm-dd hh24:mi:ss') as newTime from dual            //改变时间-日
     select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),to_char(sysdate+n/24,'yyyy-mm-dd hh24:mi:ss') as newTime from dual         //改变时间-时
     select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),to_char(sysdate+n/24/60,'yyyy-mm-dd hh24:mi:ss') as newTime from dual      //改变时间-分
     select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),to_char(sysdate+n/24/60/60,'yyyy-mm-dd hh24:mi:ss') as newTime from dual   //改变时间-秒
29.查找月的第一天,最后一天
     SELECT Trunc(Trunc(SYSDATE, 'MONTH') - 1, 'MONTH') First_Day_Last_Month,
       Trunc(SYSDATE, 'MONTH') - 1 / 86400 Last_Day_Last_Month,
       Trunc(SYSDATE, 'MONTH') First_Day_Cur_Month,
       LAST_DAY(Trunc(SYSDATE, 'MONTH')) + 1 - 1 / 86400 Last_Day_Cur_Month
   FROM dual;
30、oracle关键字转义
select ascii('&')  from dual;   -- "'"为39
select CHR(39)  from dual;
科技部运维电话存储过程统计中用到了表名是变量，关键字转义的思想。

31、七年前
select add_months(sysdate, -(12*7)) from dual;
select sysdate,sysdate - interval '7' year from dual;

查看未提交事物 
SELECT  S.SID
       ,S.SERIAL#
       ,S.USERNAME
       ,S.OSUSER 
       ,S.PROGRAM 
       ,S.EVENT
       ,TO_CHAR(S.LOGON_TIME,'YYYY-MM-DD HH24:MI:SS') 
       ,TO_CHAR(T.START_DATE,'YYYY-MM-DD HH24:MI:SS') 
       ,S.LAST_CALL_ET 
       ,S.BLOCKING_SESSION   
       ,S.STATUS
       ,( 
              SELECT Q.SQL_TEXT 
              FROM    V$SQL Q 
              WHERE  Q.LAST_ACTIVE_TIME=T.START_DATE 
              AND    ROWNUM<=1) AS SQL_TEXT   
FROM   V$SESSION S, 
       V$TRANSACTION T  
WHERE  S.SADDR = T.SES_ADDR;

32、更新表中的某个字段
	一.建临时表
	CREATE TABLE DWQUERY.CC_ZKL_20220121  ( 
		ID          	NVARCHAR2(255) NOT NULL,
		certifn       	NVARCHAR2(255) NOT NULL
		)
	GO

	二:插入脱敏数据
	insert into DWQUERY.CC_ZKL_20220121
	SELECT id ,REPLACE(certifn,SUBSTR(certifn,4,6),'******') FROM DWQUERY.CC_ZKL

	三.更新数据
	merge into DWQUERY.CC_ZKL a
	using  DWQUERY.CC_ZKL_20220121 b 
	on   (a.id = b.id) 
	when matched then 
	update set a.certifn=b.certifn 
```

# 查询Oracle正在执行的sql语句

```sql
SELECT b.sid,
       b.username,
       b.serial#,
       spid,
       paddr,
       sql_text,
       b.machine
FROM gv$process a, gv$session b, gv$sqlarea c
WHERE a.addr = b.paddr
   AND b.sql_hash_value = c.hash_value
   AND b.username='UDSF_YQJG'
   and b.machine<>'LZBANKUCM2\WIN-93MNG54TNQA';
--正在执行的sql，可以通过 gv$session 视图获取address   
select * from gv$sqltext t where t.ADDRESS='0000000A2A7EAA40';

select * from gv$session t where t.USERNAME='UDSF_YQJG'
and t.MACHINE='V_MDC_PROD_CHECK_APP_01';
--获取事件
select * from gv$event_name t where t.EVENT#='403'

--查询Oracle正在执行的sql语句及执行该语句的用户
SELECT b.sid oracleID,
       b.username 登录Oracle用户名,
       b.serial#,
       spid 操作系统ID,
       paddr,
       sql_text 正在执行的SQL,
       b.machine 计算机名
FROM v$process a, v$session b, v$sqlarea c
WHERE a.addr = b.paddr
   AND b.sql_hash_value = c.hash_value

--查看正在执行sql的发起者的发放程序
SELECT OSUSER 电脑登录身份,
       PROGRAM 发起请求的程序,
       USERNAME 登录系统的用户名,
       SCHEMANAME,
       B.Cpu_Time 花费cpu的时间,
       STATUS,
       B.SQL_TEXT 执行的sql
FROM V$SESSION A
LEFT JOIN V$SQL B ON A.SQL_ADDRESS = B.ADDRESS
                   AND A.SQL_HASH_VALUE = B.HASH_VALUE
ORDER BY b.cpu_time DESC

--查出oracle当前的被锁对象
SELECT l.session_id sid,
       s.serial#,
       l.locked_mode 锁模式,
       l.oracle_username 登录用户,
       l.os_user_name 登录机器用户名,
       s.machine 机器名,
       s.terminal 终端用户名,
       o.object_name 被锁对象名,
       s.logon_time 登录数据库时间
FROM v$locked_object l, all_objects o, v$session s
WHERE l.object_id = o.object_id
   AND l.session_id = s.sid
ORDER BY sid, s.serial#;

--kill掉当前的锁对象可以为
alter system kill session '66, 59675';
alter system kill session 'sid, s.serial#';
```

# Oracle查询今天/昨天/本周/上周/本月/上月数据

```sql
--Oracle查询今天、昨天、本周、上周、本月、上月数据
--查询今天数据：
SELECT COUNT(1) FROM T_CALL_RECORDS WHERE TO_CHAR(T_RKSJ,'YYYY-MM-DD')=TO_CHAR(SYSDATE,'YYYY-MM-DD')；

--查询昨天数据：
SELECT COUNT(1) FROM T_CALL_RECORDS WHERE TO_CHAR(T_RKSJ,'YYYY-MM-DD')=TO_CHAR(SYSDATE-1,'YYYY-MM-DD')；

--查询本周数据：
SELECT COUNT(1) FROM T_CALL_RECORDS WHERE T_RKSJ >= TRUNC(NEXT_DAY(SYSDATE-8,1)+1) AND T_RKSJ < TRUNC(NEXT_DAY(SYSDATE-8,1)+7)+1；

--查询上周数据：
SELECT COUNT(1) FROM T_CALL_RECORDS WHERE T_RKSJ >= TRUNC(NEXT_DAY(SYSDATE-8,1)-6) AND T_RKSJ < TRUNC(NEXT_DAY(SYSDATE-8,1)+1)；

--查询本月数据：
SELECT COUNT(1) FROM T_CALL_RECORDS WHERE TO_CHAR(T_RKSJ,'YYYY-MM')=TO_CHAR(SYSDATE,'YYYY-MM')；

--查询上月数据：
SELECT COUNT(1) FROM T_CALL_RECORDS WHERE TO_CHAR(T_RKSJ,'YYYY-MM')=TO_CHAR(ADD_MONTHS(SYSDATE,-1),'YYYY-MM')；
```

# oracle中delete的数据进行恢复--杨勇

```sql
oracle中delete的数据进行恢复的语句：
1.SELECT TIMESTAMP_TO_SCN(TO_TIMESTAMP('2018-05-18 15:00:00',
                                     'yyyy-mm-dd hh24:mi:ss'))
  FROM DUAL;
2.先把时间改了，执行这个，得到一串数字
3.SELECT *
  FROM (SELECT * FROM TABLE AS OF SCN 11039575163566) A；
4.然后把表名和那串数字复制到里面，就查到了
5.就是这个时间点，那个表的数据。
6.函数前面前面加SYS.
7.提示的是未找到基于指定时间的快照，是你的测试环境的undo空间太小了，没存下。
```

# Oracle数据库导入导出

Oracle数据库导入导出，详见：

《[Oracle数据库导入导出](Oracle数据库导入导出/Oracle数据库导入导出.md)》

# oracle_sql性能优化(整理版）

oracle_sql性能优化(整理版），详见《oracle_sql性能优化(整理版）.doc》
