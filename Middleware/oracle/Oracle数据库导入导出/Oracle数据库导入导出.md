﻿# **oracle数据库导入数据：**

|![](Oracle数据库导入导出.001.png)|
| - |

# **Oracle  使用tzs 的脚本导入导出数据库中的一张表**
在路径：F:\学习总结\tzs   下，通过控制文件 \*.ctl  来控制导入导出
# **sqlldr命令学习**
有效的关键字: 

userid --    ORACLE username/password 

control -    控制文件 

log -        记录的日志文件 

\*            表示数据文件在控制文件之后，如果是独立的数据文件，则把文件代替\* 

bad -        坏数据文件，记录错误的未加载数据 

data -       数据文件,\* data参数只能指定一个数据文件,如果控制文件也通过infile指定了数据文件,并且指定多个, 

`             `则sqlldr在执行时,先加载data参数指定的数据文件,控制文件中第一个infile指定的数据文件被忽略, 

`             `但后续的infile指定的数据文件继续有效 

discard -    丢弃的数据文件,默认情况不产生,必须指定 

discardmax - 允许丢弃数据的最大值        (全部默认) 

skip --      跳过记录数,从数据文件中,从第一行开始要计算要跳过的行数,\*,对于多表加载的情况,如果有when条件 

`             `判断的话，或者直接路径下的多表加载，如果要加载的记录数不同，则该参数无效. 

load --      给你一万行的数据，却只要求你导入10行 

errors -     允许的错误记录数,超过则终止任务         (默认50) 

rows --      常规路径导入时:指绑定数组中的行数;直接路径导入时,指一次从数据文件只读取的行数，该参数同时受bindsize制约,如果rows\*每行实际占用大小超出bindsize最大可用值，则rows自动降低达到bindsize最大可用值（每次提交的记录数，默认: 常规路径 64, 所有直接路径） 

bindsize -- 为绑定数组指定的最大可用空间，用来存贮一次读取的rows的记录,该值不能太小,至少要放入一条逻辑记录 但设置太大也没什么作用。 每次提交记录的缓冲区的大小(字节为单位，默认256000) 

bind array size=(number of rows)\*(sun(fixed field lengths)+sum(maximum varying field lengths)+((number of varying 

`                          `length fields)\*(size of length indicator))) 

silent --    禁止输出信息 (header,feedback,errors,discards,partitions) 

sqlldr scott/scott control=ldr\_case9.ctl silent=header 这样就可以不输出头部信息了. 

direct -     使用直通路径方式导入,不走buffer cache,通过direct path api发送数据到服务器端的加载引擎，加载引擎按照数据块的格式处理数据并直接写向数据文件,因此效率较高(默认FALSE) 

parfile --   高密度的sqlldr导入,省得写参数，就建个脚本吧 

parallel -- 并行导入 仅在直接路径加载时有效(默认FALSE) 

file --      并行加载时会用到该参数,指定file参数,要加载的内容即只向指定的数据文件写入数据，减少i/o 

skip\_unusable\_indexes--默认为false,如果是true,则加载完数据时，就算此表索引不可用,数据加载完不会改变此索引状态,oracle 数据库中也有此同名参数，但顺序是先看sqlldr,再数据库 

skip\_index\_maintenance--    是否跳过索引维护，默认false,直接路径加载有效，如果设置为true,因加载完数据不维护索引,因此索引会失效. 

readsize----    缓冲区大小,默认值:1048576单位字节,最大不超过20m,该参数仅当从数据文件读取时有效，如果是从近制文件读取数 据，则默认为64k 

external\_table: not\_used:不使用外部表，通过常规路径或直接路径加载数据 

`                 `generate\_only:sqlldr并不执行加载,而是生成创建外部表的sql和处理数据的sql，并保存在log文件中,用户可 

`                  `以修改后拿到sqlplus中执行 

`                 `execute:执行外部表并加载数据 

columnarrayrows: 指定直接路径加载时流缓冲区的行数 

`                `-- Number of rows for direct path column array(默认5000) 

streamsize :    -- Size of direct path stream buffer in bytes(默认256000) 

`                `指定直接路径加载时流缓冲区的大小 

multithreading 是否启用多线程，多cpu为true,单cpu false,直接路径加载时有效 

`           `--   use multithreading in direct path 

resumable --   会话等待空闲空间分配,在执行sqlldr时，如果余下空间不足，false参数则直接报错退出,如果设置为true, 

`                               `则等待，让dba手动处理,达到resumable\_timeout参数中指定的超时时间，再退出 

`                `enable or disable resumable for current session(默认FALSE) 

resumable\_name 会话标示名，通过查询user\_resumable或dba\_resumable二个字典来获取信息, 

`               `-- text string to help identify resumable statement 

resumable\_timeout: 会话超时,在多少时间未能执行sqlldr则退出,设置true有效 

`           `-- wait time (in seconds) for RESUMABLE(默认7200) 

date\_cache --   日期转换用缓存在，用于提高转换效率.仅在直接路径加载时有用 

`             `size (in entries) of date conversion cache(默认1000)

成功运行的案例见   F:\学习总结\tzs
## **控制文件示例：**
```
load data  					

infile './111.txt'			

APPEND				  ---在表中追加新记录，1、insert 为缺省方式，在数据装在开始时要求表为空  2、append 在表中追加新纪录  3、  replace 删除旧记录（用delete from table 语句），替换成新装载的记录  4、truncate  删除旧记录（用truncate table 语句），替换成新装载的记录

into table acl\_bankinfo 		

FIELDS TERMINATED BY '|'	  --数据中每行记录由“|”分割

trailing nullcols	 ---表的字段没有对应的只是允许为空	

(

`	`---FILLER\_1 FILLER,

`	`ID "trim(:ID) ",

`	`BANKCODE "trim(:BANKCODE) ",

FEETYPE constant "nuanqifei",     --- 该列默认值插入

`  	`BANKNAME "trim(:BANKNAME) "



`  `last\_login DATE"YYYY-MM-DD HH24:MI:SS" --指定接受日期的格式，相当于to\_date()函数转换

)
```


# **sqlplus命令学习**
以下语句可以将一张表中需要的数据放到文件中，通过SET 将不需要的输出信息屏蔽

```bash
sqlplus -s hurodb/hurodb@hurodb<<EOF

SET feedback off

SET newpage none

SET pagesize 50000

SET linesize 20000

SET verify off

SET pagesize 0

SET term off

SET trims ON

SET heading  off

SET trimspool ON

SET trimout ON

SET timing off

SET verify off

SET colsep |

set term off

spool /home/app/zkl.txt

select distinct ASUMBUSICLASS from tCsrWorkSumBusi order by ASUMBUSICLASS;

spool off

EOF
```



## **另一种方法**
将需要执行的sql语句放在文件sql.txt ，执行下面的shell ，可以重复执行（循环），得到想要的结果


CONSTR="hurodb/hurodb@hurodb"

sqlplus ${CONSTR} <<EOF

@sql.txt

exit

eof

将导入结果的日志放入指定的文件，linux中的shell如下：

CONSTR="zkl/zkl@10.0.0.3:1521/hurodb"

sqlplus -s ${CONSTR} <<EOF

spool /home/oracle/result.log

prompt -----初始化数据 start--- 



@EEOnile-Data.txt

prompt -----初始化数据 end---

prompt done; 

spool off;  

exit

EOF

windows下的bat脚本如下：

sqlplus zkl/zkl@10.0.0.3:1521/hurodb @G:\\oracle\\EEOnile-Data.txt > g:\\oracle\\result.txt

pause

注意：sqlplus到result.txt为一行，否则有问题
# **Oracle数据库命令导出数据**
输入命令env | grep oracle显示

|![](Oracle数据库导入导出.002.png)|
| - |

$cd $ORACLE\_HOME 

$exp userid=rcpmisf/rcpmisf file=/home/oracle/db-backup/rcpmisf20140102.dmp

后面加 full=y 表示将所有的表空间数据等全部导出。

使用sqlplus输入用户名与密码

**注意，此前要设定导出文件的字符集，否则有乱码。**

查看字符集: select userenv(‘language’) from dual;

如   AMERICAN\_AMERICA.ZHS16GBK

执行命令：export  **NLS\_LANG**=AMERICAN\_AMERICA.ZHS16GBK


命令导入数据：

$imp userid=rcpmisf/rcpmisf file=/home/oracle/db-backup/rcpmisf20140102.dmp

imp userid=amadmin/password1 file=./lzbnakam.dmp fromuser=lzbankam
## **查看dmp文件的编码：**
cat '/home/oracle/db-backup/rcpmisf20140102.dmp' |od -x|head -1|awk '{print $2 $3}'|cut -c 3-6

查看对应字符集：

SQL> select nls\_charset\_name(to\_number(‘0345’,’xxxx’)) from dual;

SQL> select to\_char(nls\_charset\_id('ZHS16GBK'),'xxxx') from dual;
## **导出（入）在本机已经创建好客户端的数据库**
exp webchat/webchat@HURODB\_146 file=c:\webchat.dmp owner=(webchat)

imp system/oracle@webchat file=c:\webchat.dmp full=y

exp <ccadmin/abc@98.11.0.178:1521/authplatfrom> file=g:\ip.dmp log=g:\log.log tables=ip\_address

imp ccadmin/DBccadmin123@98.10.2.231:1521/yhtdb file=G:\oracle\billpass\ip\_address\ip.dmp log=G:\oracle\billpass\ip\_address\imp.log tables=ip\_address commit=y

exp udsf/lzbank@98.11.10.21:1521/lzudsf file=./data.dmp log=./log.log tables=f\_csp\_busalpayflow query=\"where data\_time\>=\'2021-01-01\'\"

**注意query参数，linux下注意 > 和 "   '   需要转义**
# **无法删除用户rcpmisf解决办法：（system/oracle）**
sqlplus sys/orcl as sysdba

set echo on;                   //查询当前连接该用户的session

select username,sid,serial# from v$session;  //查出session

alter system kill session'6,7411';   //杀掉session，有多个杀多个

drop user rcpmisf cascade;   //删除用户

查看表的字段：desc tablename;

查看数据库字符集：select userenv ('language') from dual;

`     `select nls\_charset\_name(to\_number('0354','xxxx')) from dual;

# **泄数组数据中心需要字段**
1. `   `||'|@|'||

1. 使用sed加正则表达式
