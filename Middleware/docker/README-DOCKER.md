# docker安装

## redhat下安装：

下载docker：https://download.docker.com/linux/static/stable/

执行 install.sh

## windows下安装：

https://download.docker.com/win/static/stable/x86_64/

docker-compose 通过github下载

# docker玩法（用法）

## 1、快速构建一次性测试环境

```shell
# 启动 MySQL
docker run --name mysql-test -e MYSQL_ROOT_PASSWORD=test -p 3306:3306 -d mysql:5.7
# 启动 PostgreSQL
docker run --name postgres-test -e POSTGRES_PASSWORD=test -p 5432:5432 -d postgres:13

# docker stop mysql-test postgres-test && docker rm mysql-test postgres-test
```

## 2、定制化开发工具箱

### **场景一：运行特定版本的 Node.js**

```shell
docker run -it --rm node:16 node -v
docker run -it --rm node:18 node -v
```

### **场景二、通过挂载工作目录，将本地代码和数据直接在容器中运行。**

```shell
docker run -it --rm -v $(pwd):/workdir python:3.10 bash
pip install pandas numpy matplotlib
python your_script.py
```

## 3、本地私有云模拟

### **场景一、使用 MinIO，轻松在本地搭建一个兼容 S3 API 的存储服务：**

```shell
docker run -d -p 9000:9000 -p 9001:9001 --name minio \
  -e "MINIO_ROOT_USER=admin" \
  -e "MINIO_ROOT_PASSWORD=admin123" \
  quay.io/minio/minio server /data --console-address ":9001"
```

连接方式与 AWS S3 一致，非常适合本地开发和调试。

### **场景二、搭建 RabbitMQ 消息队列**

```sh
docker run -d -p 5672:5672 -p 15672:15672 --name rabbitmq rabbitmq:management
```

通过访问http://localhost:15672，即可打开 RabbitMQ 管理界面。

## 4、构建定制化自动化任务平台

```shell
FROM mysql:5.7
RUN apt-get update && apt-get install -y cron
COPY backup.sh /usr/local/bin/backup.sh
RUN chmod +x /usr/local/bin/backup.sh
CMD ["cron", "-f"]
```

## 5、家庭服务器搭建

### 场景 1：NAS 系统

使用 Nextcloud，轻松搭建私有云存储：

```sh
docker run -d \
  -v nextcloud-data:/var/www/html \
  -p 8080:80 \
  nextcloud
```

### 场景 2：多媒体中心

通过 Plex 搭建家庭流媒体服务：

```sh
docker run -d \
  --name plex \
  -e PLEX_CLAIM=your-plex-claim \
  -p 32400:32400 \
  -v /path/to/media:/media \
  plexinc/pms-docker
```

## 6、快速部署指定版本的服务

快速部署指定版本的服务，检查问题是否与升级相关

场景 1：还原特定系统环境
场景 2：还原特定版本的服务端

## 7、游戏和娱乐

Docker 还能用于“非正式”场景，比如运行游戏服务端。

# docker方式启动：

## pull：

docker pull redis 

最新版redis

## start：run:

```bash
docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7

docker run -itd --name redis-ruoyi -p 6379:6379 redis

docker run -itd --name nginx-ruoyi -p 80:80 nginx:1.21.1
```

*# 挂载多个* $ docker run -v [主机目录1]:[容器目录1] -v [主机目录2]:[容器目录2]  [镜像名称]

## 进入交互式界面：

docker exec -it 37c20bd11673 /bin/bash



## build：

docker build -f ./Dockerfile -t demo:v1 ./

## commit:

`docker commit` 命令用于将容器的当前状态保存为一个新的 Docker 镜像。

`docker commit` 命令通常用于创建镜像来保存容器的状态，以便在将来可以重用或分发该镜像。

### 语法

```shell
docker commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]
```

将容器保存为一个新镜像，镜像名定义为`exportiso:v1`

docker commit 容器ID 镜像名称:标签 

docker commit 43df7dc8f3c7 python388schedule:1.1

## save:

docker save 镜像名称:标签 -o 文件名.tar 

docker save python388schedule:1.1 -o python388schedule-1.1.tar



## portainer图形化界面管理docker



# docker常用命令 - 菜鸟教程

### 容器生命周期管理

- [run - 创建并启动一个新的容器。](https://www.runoob.com/docker/docker-run-command.html)
- [start/stop/restart - 这些命令主要用于启动、停止和重启容器。](https://www.runoob.com/docker/docker-start-stop-restart-command.html)
- [kill - 立即终止一个或多个正在运行的容器](https://www.runoob.com/docker/docker-kill-command.html)
- [rm - 于删除一个或多个已经停止的容器。](https://www.runoob.com/docker/docker-rm-command.html)
- [pause/unpause - 暂停和恢复容器中的所有进程。](https://www.runoob.com/docker/docker-pause-unpause-command.html)
- [create - 创建一个新的容器，但不会启动它。](https://www.runoob.com/docker/docker-create-command.html)
- [exec - 在运行中的容器内执行一个新的命令。](https://www.runoob.com/docker/docker-exec-command.html)
- [rename - 重命名容器。](https://www.runoob.com/docker/docker-rename-command.html)

### 容器操作

- [ps - 列出 Docker 容器](https://www.runoob.com/docker/docker-ps-command.html)
- [inspect - 获取 Docker 对象（容器、镜像、卷、网络等）的详细信息。](https://www.runoob.com/docker/docker-inspect-command.html)
- [top - 显示指定容器中的正在运行的进程。](https://www.runoob.com/docker/docker-top-command.html)
- [attach - 允许用户附加到正在运行的容器并与其交互。](https://www.runoob.com/docker/docker-attach-command.html)
- [events - 获取 Docker 守护进程生成的事件。](https://www.runoob.com/docker/docker-events-command.html)
- [logs - 获取和查看容器的日志输出。](https://www.runoob.com/docker/docker-logs-command.html)
- [wait - 允许用户等待容器停止并获取其退出代码。](https://www.runoob.com/docker/docker-wait-command.html)
- [export - 将容器的文件系统导出为 tar 归档文件。](https://www.runoob.com/docker/docker-export-command.html)
- [port - 显示容器的端口映射信息。](https://www.runoob.com/docker/docker-port-command.html)
- [stats - 实时显示 Docker 容器的资源使用情况。](https://www.runoob.com/docker/docker-stats-command.html)
- [update - 更新 Docker 容器的资源限制，包括内存、CPU 等。](https://www.runoob.com/docker/docker-update-command.html)

### 容器的root文件系统（rootfs）命令

- [commit - 允许用户将容器的当前状态保存为新的 Docker 镜像。](https://www.runoob.com/docker/docker-commit-command.html)
- [cp - 用于在容器和宿主机之间复制文件或目录。](https://www.runoob.com/docker/docker-cp-command.html)
- [diff - 显示 Docker 容器文件系统的变更。](https://www.runoob.com/docker/docker-diff-command.html)

### 镜像仓库

- [login/logout - 管理 Docker 客户端与 Docker 注册表的身份验证。](https://www.runoob.com/docker/docker-login-command.html)
- [pull - 从 Docker 注册表（例如 Docker Hub）中拉取（下载）镜像到本地。](https://www.runoob.com/docker/docker-pull-command.html)
- [push - 将本地构建的 Docker 镜像推送（上传）到 Docker 注册表（如 Docker Hub 或私有注册表）。](https://www.runoob.com/docker/docker-push-command.html)
- [search - 用于在 Docker Hub 或其他注册表中搜索镜像。](https://www.runoob.com/docker/docker-search-command.html)

### 本地镜像管理

- [images - 列出本地的 Docker 镜像。](https://www.runoob.com/docker/docker-images-command.html)
- [rmi - 删除不再需要的镜像。](https://www.runoob.com/docker/docker-rmi-command.html)
- [tag - 创建本地镜像的别名（tag）。](https://www.runoob.com/docker/docker-tag-command.html)
- [build - 从 Dockerfile 构建 Docker 镜像。](https://www.runoob.com/docker/docker-build-command.html)
- [history - 查看指定镜像的历史层信息。](https://www.runoob.com/docker/docker-history-command.html)
- [save - 将一个或多个 Docker 镜像保存到一个 tar 归档文件中。](https://www.runoob.com/docker/docker-save-command.html)
- [load - 从由 docker save 命令生成的 tar 文件中加载 Docker 镜像。](https://www.runoob.com/docker/docker-load-command.html)
- [import - 从一个 tar 文件或 URL 导入容器快照，从而创建一个新的 Docker 镜像。](https://www.runoob.com/docker/docker-import-command.html)

### info|version

- [info - 显示 Docker 的系统级信息，包括当前的镜像和容器数量。](https://www.runoob.com/docker/docker-info-command.html)
- [version - 显示 Docker 客户端和服务端的版本信息。](https://www.runoob.com/docker/docker-version-command.html)

### Docker Compose

- [docker compose run - 启动一个新容器并运行一个特定的应用程序。](https://www.runoob.com/docker/docker-compose-run-command.html)
- [docker compose rm - 启动一个新容器并删除一个特定的应用程序。](https://www.runoob.com/docker/docker-compose-rm-command.html)
- [docker compose ps - 从 docker compose 检查 docker 容器状态。](https://www.runoob.com/docker/docker-compose-ps-command.html)
- [docker compose build - 构建 docker compose 文件。](https://www.runoob.com/docker/docker-compose-bulid-command.html)
- [docker compose up - 运行 docker compose 文件。](https://www.runoob.com/docker/docker-compose-up-command.html)
- [docker compose ls - 列出 docker compose 服务。](https://www.runoob.com/docker/docker-compose-ls-command.html)
- [docker compose start - 启动 docker compose 文件创建的容器。](https://www.runoob.com/docker/docker-compose-start-command.html)
- [docker compose restart - 重启 docker compose 文件创建的容器。](https://www.runoob.com/docker/docker-compose-restart-command.html)

### 网络命令

- **`docker network ls`**: 列出所有网络。
- **`docker network create <network>`**: 创建一个新的网络。
- **`docker network rm <network>`**: 删除指定的网络。
- **`docker network connect <network> <container>`**: 连接容器到网络。
- **`docker network disconnect <network> <container>`**: 断开容器与网络的连接。

详细内容查看：[docker network 命令](https://www.runoob.com/docker/docker-network-command.html")

### 卷命令

- **`docker volume ls`**: 列出所有卷。
- **`docker volume create <volume>`**: 创建一个新的卷。
- **`docker volume rm <volume>`**: 删除指定的卷。
- **`docker volume inspect <volume>`**: 显示卷的详细信息。
