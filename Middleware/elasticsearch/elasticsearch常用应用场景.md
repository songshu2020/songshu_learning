Elasticsearch 主要应用于**搜索场景**。场景如：

- 日志搜索

- 应用内搜索

- Elasticsearch作为二级索引

- 运用后台管理系统查询

下面将介绍 Elasticsearch 在开发中的常见应用场景。 

# 日志搜索

日志搜索是最常见的应用。   
其组合技术为：Kafka、Logstash、Elasticsearch、Kibana

该组合整体的技术方案如下：

![](./ELK.png)

值得一提的是，对于一些日志量特别大的公司，已经把 Elasticsearch 更换为其他的 NoSql。典型的如 ClickHouse。

# 应用内搜索

应用内搜索 App 中随处可见。  
其组合技术为：Mysql、DTS（Canal、Debezium、云厂商服务）、MQ（Kafka）、Elasticsearch对于单表，我们一般不会借助 MQ，而是直接将数据写入 Elasticsearch。该组合整体的技术方案如下：

![](./640.png)

对于复杂的多表同步，一般会将消息打入 MQ，然后在 Application 侧组装消息，最后再写入 Elasticsearch:

![](./641.png)

需要一提的是，DTS 国内用得多的是 Canal，不过国际主流应该是 Debezium。
现在业内对于这种数据同步需求，也有公司在使用 Flink CDC对于这种数据异构，我们需要特别注意数据的一致性问题。

# Elasticsearch 作为二级索引

Elasticsearch 除了 Text 字段，其他字段默认都开启 doc_values，也就是我们常说的正排索引，正排索引是基于列式存储，因此在查询、排序、聚合等查询性能上会比 Mysql 优秀。因此在业务中，我们碰到复杂的查询会借助 Elasticsearch 或其他 NoSQL 构建二级索引。
其组合技术为：Mysql、DTS（Canal、Debezium、云厂商服务）、Elasticsearch整体的技术方案如下：

![](./642.png)



# 运营后台管理系统

运营后台管理系统，往往是非常复杂的条件查询。对应这种，非常常见的是将运营所需要的数据异构到 Elasticsearch，借助 Elasticsearch 提供复杂条件查询 其组合技术为：Mysql、DTS（Canal、Debezium、云厂商服务）、Elasticsearch整体的技术方案如下：

![](./643.png)