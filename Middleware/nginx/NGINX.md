

转载了一篇比较好的关于nginx安装配置的文章到本人csdn，链接如下：

https://blog.csdn.net/zkl718/article/details/141064084





以下内容摘自：https://mp.weixin.qq.com/s/SLa1MQ5KxTNul5AuGJUNeQ

# 全面掌握Nginx：从基础安装到高级负载均衡和高可用性配置

原创 小白典 [小白典](javascript:void(0);) *2024年06月11日 18:32* *江苏*

### **基本概念**

##### **1. Nginx介绍**

Nginx是转为性能优化而开发的，它是一个高性能的HTTP和反向代理web服务器，同时也提供了IMAP/POP3/SMTP服务，特点是占有内存少，并发处理能力强，以高性能低资源消耗而闻名，有大量网站都在使用Nginx

##### **2. 无代理**

不使用代理时，我们所发送的请求都是通过网络直接传达给目标服务器的

![图片](640.png)

##### **3. 正向代理**

正向代理是由客户端设置的，是指将请求发送给代理服务器，再由代理服务器转发给目标服务器，举个栗子：我们在玩游戏时，尤其是大型网络游戏，都会选择迅游或UU游戏加速器，其实开启加速器就是在本地设置正向代理

![图片](641.png)

##### **4. 反向代理**

反向代理是由目标服务器端设置的，指我们将请求发送给目标服务器时，请求会先到达代理服务器，再将其转发给目标服务器，因为不是我们主动配置，这个过程我们是无从感知的，这种代理配置是最常见的功能之一，我们使用百度、京东、淘宝等，其实都有使用反向代理，只是我们不知道而已

![图片](642.png)

###### **4.1 负载均衡**

负载均衡相关概念可以看一下[此文章](http://mp.weixin.qq.com/s?__biz=MzU3NzYwNjI3MA==&mid=2247484997&idx=1&sn=e557d026ff92d73dd00263145d326a17&chksm=fd034154ca74c84217317b8ad8c6b3a26f55a8f1095cd44bcd71770ae56b496d9e47482e6cca&scene=21#wechat_redirect)

Nginx提供了四种负载均衡策略，默认轮询算法，除此之外还有第三方的策略：fair、url_hash

- **4.1.1 轮询**

  假设有10个请求，负载均衡采用默认方式(即轮询)，请求结果会出现如下图所示的情况，每个请求按时间顺序逐一分配到不同的服务器，若服务器宕掉，会自动剔除

  ![图片](643.png)

- **4.1.2 加权轮询**

  假设有10个请求，负载均衡采用的是加权轮询方式，请求结果会出现如下图所示的情况，需要给每台服务器配置权重，权重和访问比率成正比，一般用于服务器性能不均的情况，权重越大越容易被访问，权重数值只是代表机率，下图示例中并非指访问6次必定有3次请求是由②号服务器处理

  ![图片](644.png)

- **4.1.3 least_conn**

  把请求转发给连接数较少的服务器，轮询策略把请求平均的转发给各个服务器，使它们的负载大致相同，但是，有些请求占用的时间较长，会导致其所在的服务器负载较高，这种情况下，least_conn策略就可以达到更好的负载均衡效果，如下图所示新请求会分给不太繁忙的服务器

  ![图片](645.png)

- **4.1.4 ip_hash**

  假如有5个请求，负载均衡采用的是ip_hash方式，请求结果会出现如下图所示的情况，每个请求按访问IP的hash值进行分配，这样每个访客固定访问一个服务器，可以解决Session不共享的问题，但是服务器重启后，hash值会重新计算，不过这种策略已经很少用了，现在基本都是使用Redis做Session共享

  ![图片](646.png)

- **4.1.5 fair**(第三方)

  fair策略是扩展策略，默认不被编译进nginx内核，使用需先进行编译安装，它根据后端服务器的响应时间判断负载情况，从中选出负载最轻的机器进行分流，这种策略具有很强的自适应性，但实际网络环境往往没那么简单

- **4.1.6 url_hash**(第三方)

  和ip_hash策略相似，按访问url的hash值进行分配，使每个url定向到同一个服务器，要配合缓存命中来使用，使用url_hash可以使同一个请求到达同一台服务器，一旦缓存了资源，再此收到请求后就会直接从缓存中读取

  ###### **4.2 动静分离**

指动态请求跟静态请求分开，可以理解成使用 Nginx处理静态页面，Tomcat 处理动态页面。动静分离大致分为两种，一种是完全把静态文件独立成单独的域名放在独立的服务器上；另一种是动态跟静态文件混在一起发布，通过Nginx进行拆分。可以通过location指定不同的后缀名实现不同的请求转发。通过expires参数设置，可以使浏览器缓存过期时间，减少与服务器之前的请求和流量，加快网页解析速度

![图片](647.png)

###### **4.3 高可用(HA)集群**

高可用HA（High Availability）是分布式系统架构设计中必须考虑的因素之一，指通过设计减少系统的停工时间。为防止Nginx失效，需要建立一个备用Nginx，主Nginx和备用Nginx上都要运行HA监控程序，通过传送诸如“I am alive”的信息来监控对方的运行状况，当备用Nginx不能在一定的时间内收到这样的信息时，它就接管主Nginx的服务IP并继续提供服务；当备用Nginx又从主Nginx收到“I am alive”的信息时，它就释放服务IP地址，由主Nginx接管工作并继续提供服务

![图片](648.png)

##### **Nginx安装**

###### **1. 安装编译工具及库文件**

```
dnf install make gcc-c++ pcre pcre-devel zlib zlib-devel openssl openssl-devel -y
```

Nginx安装需要进行编译，编译依赖gcc环境

Nginx的http模块使用**pcre**来解析正则表达式，pcre-devel是pcre的一个二次开发库

Nginx使用**zlib**对http包的内容进行gzip

Nginx支持http，也支持https(即在SSL协议上传输http)，openssl包括SSL协议库、应用程序以及密码算法库

###### **2.安装Nginx**

Nginx下载地址：点击进入下载页（http://nginx.org/en/download.html），选择所需版本

2.1 下载安装包

```sh
wget http://nginx.org/download/nginx-1.18.0.tar.gz
```

2.2 解压

```sh
tar -zxvf nginx-1.18.0.tar.gz
```

2.3 配置

```sh
# 先切到nginx-1.18.0目录下
cd nginx-1.18.0
# 在Nginx根目录下执行以下命令
./configure
make
make install
```

安装后可以通过 nginx -V查看

```shell
└─# nginx -V             
nginx version: nginx/1.22.0
built with OpenSSL 3.0.4 21 Jun 2022 (running with OpenSSL 3.3.2 3 Sep 2024)
TLS SNI support enabled
configure arguments: --with-cc-opt='-g -O2 -ffile-prefix-map=/build/nginx-vFfTjh/nginx-1.22.0=. -fstack-protector-strong -Wformat -Werror=format-security -fPIC -Wdate-time -D_FORTIFY_SOURCE=2' --with-ld-opt='-Wl,-z,relro -Wl,-z,now -fPIC' --prefix=/usr/share/nginx --conf-path=/etc/nginx/nginx.conf --http-log-path=/var/log/nginx/access.log --error-log-path=/var/log/nginx/error.log --lock-path=/var/lock/nginx.lock --pid-path=/run/nginx.pid --modules-path=/usr/lib/nginx/modules --http-client-body-temp-path=/var/lib/nginx/body --http-fastcgi-temp-path=/var/lib/nginx/fastcgi --http-proxy-temp-path=/var/lib/nginx/proxy --http-scgi-temp-path=/var/lib/nginx/scgi --http-uwsgi-temp-path=/var/lib/nginx/uwsgi --with-compat --with-debug --with-pcre-jit --with-http_ssl_module --with-http_stub_status_module --with-http_realip_module --with-http_auth_request_module --with-http_v2_module --with-http_dav_module --with-http_slice_module --with-threads --with-http_addition_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_sub_module
```

###### **3、编译nginx，添加vts模块(监控)**

```shell
# 上传包到服务器（服务器可连接广域网的话直接wget即可）
# 解压
tar -zxvf nginx-1.17.10.tar.gz nginx-module-vts-0.1.17.tar.gz
# 将vts解压后的目录移到需要放的目录（目录根据实际情况修改）
mv nginx-module-vts-0.1.17 /opt
# 编译nginx
cd nginx-1.17.10
## 生成makefile，注意需要附加上之前查询出的编译信息，在末尾添加上vts模块路径
./configure --prefix=/usr/local/nginx --with-http_ssl_module --with-http_flv_module --with-http_stub_status_module --with-http_gzip_static_module --with-http_realip_module --with-pcre --add-module=/home/lua-nginx-module-0.10.9rc7 --add-module=/home/ngx_devel_kit-0.3.0 --with-stream --add-module=/opt/nginx-module-vts-0.1.17
## make命令执行构建
make
## 查看构建完成后的编译信息，看是否有vts模块
[root@test nginx-1.17.10]# ./objs/nginx -V
nginx version: nginx/1.17.10
built by gcc 4.8.5 20150623 (Red Hat 4.8.5-44) (GCC) 
built with OpenSSL 1.0.2k-fips  26 Jan 2017
TLS SNI support enabled
configure arguments: --prefix=/usr/local/nginx --with-http_ssl_module --with-http_flv_module --with-http_stub_status_module --with-http_gzip_static_module --with-http_realip_module --with-pcre --add-module=/home/lua-nginx-module-0.10.9rc7 --add-module=/home/ngx_devel_kit-0.3.0 --with-stream --add-module=/opt/nginx-module-vts-0.1.17

http段中添加   vhost_traffic_status_zone;
        # nginx监控
        location /status {
            vhost_traffic_status_display;
            vhost_traffic_status_display_format html;
        }

```

2.4 启动Nginx

先查看Nginx安装路径，在sbin目录下启动Nginx

```
# 查看路径
whereis nginx
# 进入Nginx安装目录下的sbin目录
cd /usr/local/nginx/sbin
# 启动Nginx
./nginx
```



Nginx启动后无任何反馈，通过浏览器访问IP:80，进行验证



#### **Nginx常用命令**

```
# 在sbin目录下执行，或者在命令前加上路径
cd /usr/local/nginx/sbin
# 启动
./nginx
# 查看配置文件是否正确
./nginx -t
# 重新加载配置文件
./nginx -s reload
# 查看nginx进程
ps aux|grep nginx
# 安全退出
./nginx -s quit
# 停止Nginx
./nginx -s stop
```

![图片](649.png)

当执行命令出现如下报错，

```
nginx: [error] open() "/usr/local/nginx/logs/nginx.pid" failed (2: No such file or directory)
```

首先判断Nginx是否已启动，`ps -ef | grep nginx`查看进程，若是Nginx未启动，启动后通常会恢复正常；若问题依然存在，则使用nginx -c参数指定nginx.conf文件的位置，执行以下命名即可解决

```
/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
```

![图片](1640.png)

#### **Nginx配置文件说明**

配置文件是在conf目录下nginx.conf文件

举例：在三台服务器上分别启动一个项目



访问三个项目如下



在192.166.66.21上安装并运行Nginx

Nginx的配置文件核心区域

```sh
# 全局配置 # 作用域是全局
events # Nginx工作事件配置
http # HTTP配置
	upstream # 负载均衡服务配置
	server # 主机配置
	location # URL匹配
```

全局配置：Nginx服务器整体运行的配置

主要包括配置运行Nginx服务器的用户（组）、worker process数，进程PID及日志存放路径，还有类型以及配置文件的引入等

`events`：Nginx服务器与用户的网络连接

主要包括是否开启对多work process下的网络连接进行序列化，是否允许同时接收多个网络连接，选取哪种事件驱动模型来处理连接请求，每个 word process可以同时支持的最大连接数等

`http`：主要包括代理、缓存和日志定义等大部分功能以及第三方模块的配置

`upstream`：负责均衡策略、服务器地址及权重配置等

`server`：配置虚拟主机的相关参数，一个http中可以有多个server

`location`：配置请求的路由，对特定的请求进行配置

编辑` /usr/local/nginx/conf/nginx.conf`文件

##### **1. 负载均衡配置**

###### **1.1 轮询&加权轮询**

在http配置中添加如下配置

```sh
# upstream是配置负载均衡的，名称是自定义的，server是指服务地址
# weight是指权重，默认轮询策略的值是相同的，让weight值出现差异就是在配置加权轮询
    upstream dyd{
        server 192.166.66.21:8080 weight=1;
        server 192.166.66.22:8081 weight=6;
        server 192.166.66.23:8082 weight=3;
        }
# 修改server配置，添加proxy_pass地址，地址是负载均衡名称
    server {
        listen       80;
        server_name  localhost;
        location / {
            root   html;
            index  index.html index.htm;
            proxy_pass http://dyd;
        }
    }
# 因为是三台服务，需要新增两个server，如果三台服务器的名称不一样，则需要修改server_name
    server {
        listen       80;
        server_name  localhost;
        location / {
            root   html;
            index  index.html index.htm;
            proxy_pass http://dyd;
        }
    }
    server {
        listen       80;
        server_name  localhost;
        location / {
            root   html;
            index  index.html index.htm;
            proxy_pass http://dyd;
        }
	}
```

配置完成后，保存退出，重新加载配置`./nginx -s reload`

最后我们只访问192.166.66.21，便可访问到另外两个项目，每次刷新页面，根据权重不同Nginx会轮询访问三个项目。这就是我们每次发出请求，Nginx根据权重去访问了不同的服务而得到的结果



```sh
# 若服务器名称相同，重新加载配置文件时会出现如下警告，这个不影响项目运行，可以忽略，或者修改hostname
[root@localhost sbin]# ./nginx -s reload
nginx: [warn] conflicting server name "localhost" on 0.0.0.0:80, ignored
nginx: [warn] conflicting server name "localhost" on 0.0.0.0:80, ignored
```

###### **1.2least_conn**

```sh
# least_conn策略配置
	upstream dyd{
	    least_conn;
        server 192.166.66.21:8080;
        server 192.166.66.22:8081;
        }
```

least_conn策略可以实现在某些请求耗时较长才能完成的情况下，更公平地控制应用程序实例上的负载

###### **1.3 ip_hash**

```sh
# ip_hash策略配置
    upstream dyd{
        ip_hash;
        server 192.166.66.22:8081;
        server 192.166.66.23:8082;
        }
```

配置后只能访问到一个项目，因为一旦有服务接收到并处理了请求后，之后所有的请求都将由它负责，不会再分给其它服务器，除非或服务重启

###### **1.4 fair**

```sh
# fair策略配置
	upstream dyd{
        fair;
        server 192.166.66.22:8081;
        server 192.166.66.23:8082;
        }
```

按照服务器端的响应时间来分配请求，根据服务器的响应时间判断负载情况，从中选出负载最轻的机器进行分流

###### **1.5 url_hash**

```sh
# url_hash策略配置
	upstream dyd{
        hash $request_URL;
        server 192.166.66.22:8081;
        server 192.166.66.23:8082;
        }
```

同ip_hash相似，按访问url的hash值进行分配，使每个url定向到同一个服务器，需配合缓存命中来使用，url_hash配置后只能一台服务器，因为一旦在服务器中缓存了资源，再此收到来自此URL的请求后就会直接从缓存中读取

##### **2. 动静分离**

在系统中创建两个目录，路径及名称请自定义，如下图，在根目录创建test目录，并在test目录下分别创建images、dynamic目录，在images中放置几张图片，在dynamic目录中放置一个html文件

![图片](2640.png)

Nginx配置文件`/usr/local/nginx/conf/nginx.conf`修改为如下信息，请根据自建文件目录做适当修改

```sh
#配置主机端口及地址，server_name可以是主机名也可以是IP地址
	server{
        listen       80;
        server_name  localhost;
# 配置静态文件请求路径
        location /images/ {
            root   /test/;
            index  index.html index.htm;
            # 设置目录浏览
            autoindex   on;
       }
# 配置动态文件请求路径
        location /dynamic/ {
            root   /test/;
            index  index.html index.htm;
        }
```

配置Nginx文件后会启动Nginx，通过浏览器访问静态文件，如下图，可以成功访问到

![图片](3640.png)

通过浏览器访问html文件，如下图也可以成功访问到

![图片](4640.png)

##### **3. 高可用(HA)集群**

高可用集群解决的是保障应用程序持续对外提供服务的能力，高可用集群并不是保护用户的业务数据，而是保护用户的业务程序对外不间断提供服务的能力，使用高可用集群的目的是把因软件、硬件、人为因素等造成的故障对业务的影响降低到最低程度

###### **3.1 准备工作**

1. 准备两台服务器

2. 两台服务器都需要安装上Nginx

3. 两台服务器都要安装Keepalived，安装命令`dnf install keepalived -y`

   安装keepalived后会在`/etc/keepalived`目录下生成一个配置`keepalived.conf`

   在两个服务器上个启动一个项目



配置负载均衡，方法就不再具体介绍了，根据上文介绍稍作修改即可，两个服务器都需要配置

```sh
    upstream dyd{
        server 192.166.66.21:8080;
        server 192.166.66.22:8081;
        }
    server {
        listen       80;
        server_name  localhost;
        location / {
            root   html;
            index  index.html index.htm;
            proxy_pass http://dyd;
        }
    }
    server {
        listen       80;
        server_name  localhost;
        location / {
            root   html;
            index  index.html index.htm;
            proxy_pass http://dyd;
        }
    }
```

配置完成使用浏览器进行验证，直接通过`192.166.66.21`和`192.166.66.22`都应该能够访问到两个项目，每次刷新页面项目会变更



###### **3.2 配置keepalived**

1. 配置keepalived.conf文件，修改为以下内容

   ```sh
   ! Configuration File for keepalived
   
   global_defs {
      notification_email {
        acassen@firewall.loc
        failover@firewall.loc
        sysadmin@firewall.loc
      }
      notification_email_from Alexandre.Cassen@firewall.loc
   # 当前服务IP地址
      smtp_server 192.166.66.21
      smtp_connect_timeout 30
   # 需要在/etc/hosts中添加本地解析127.0.0.1 duan
      router_id duan
   }
   
   vrrp_script chk_http_port {
   # 脚本路径
      scrip "/usr/local/src/nginx_check.sh"
   # 检查脚本执行间隔时间(s)和权重
      interval 2
      weight 2
   }
   
   vrrp_instance VI_1 {
   # 备用服务器上需要将MASTER改为BACKUP
       state MASTER
   # 网卡名
       interface ens33
   # 主、备服务器的virtual_router_id必须相同	
       virtual_router_id 51
   # 主、备服务器要有不同的优先级，主服务器的值应该大于备用服务器的值
       priority 100
       advert_int 1
       authentication {
           auth_type PASS
           auth_pass 1111
       }
       virtual_ipaddress {
   # VIP(即虚拟IP地址)，需要与当前网络的网段保持一致
           192.166.66.100
       }
   }
   ```

   主、备服务器的脚本内容如下，脚本信息是负责监控主、备服务器是否正常，以便及时切换服务

   ```sh
   #! /bin/bash
   A=`ps -C  nginx -no-header | WC -l`
   if [ $A -eq 0 ];then
       /usr/local/nginx/sbin/nginx
           sleep 2
           if [ `ps -C nginx --noheader | WC -l` -eq 0 ];then
               killall keepalived
           fi
   fi
   ```

2. 脚本文件配置后，依次启动nginx和keepalived，两台服务都要启动

   ```sh
   # 启动nginx
   cd /usr/local/nginx/sbin
   ./nginx
   # 启动keepalived
   systemctl start keepalived
   ```

3. 验证

   启动主、备服务器后通过`192.166.66.21`、`192.166.66.22`以及`192.166.66.100`应该都能访问到项目，即使任一服务器上的Nginx和keepalived停止运行了，通过VIP地址192.166.66.100始终都可以访问到项目

   

   当项目在主服务器192.166.66.21上启动后ip中会出现虚拟IP地址，下图是主服务器192.168.66.21上的IP信息

   

   当把主服务器192.166.66.21上的Nginx和keepalived停止后虚拟IP会消失，下图是主服务器192.168.66.21上的IP信息

   

   查看备用服务器192.166.66.22上出现了虚拟IP，这是因为已成功切换为备用服务器，当启动主服务器上的Nginx和keepalived后IP又会从备用服务器192.166.66.22上消失，在主服务器上显示，下图是备用服务器192.168.66.22上的IP信息

   

   如果有些迷糊，实操一遍肯定就会理解啦！

   总之，任一Nginx停止运行，另外一个都会自动接管虚拟IP地址(VIP)，所以不管哪台服务器上的Nginx停止运行，我们都可以通过虚拟IP(VIP)访问到项目

   