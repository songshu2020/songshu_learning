# Prometheus Exporter 介绍

Prometheus 的 **Exporter** 是用来将第三方系统的监控数据暴露给 Prometheus 的工具。Exporter 将目标系统的指标数据转换为 Prometheus 可以抓取的格式（通常是 HTTP 接口暴露的 `/metrics` 端点）。Prometheus 社区提供了丰富的 Exporter，覆盖了各种常见的监控场景。

以下是 Prometheus 中常见的 Exporter 分类和介绍：

---

## 1. 主机和操作系统监控

### Node Exporter
- **用途**: 用于监控主机的硬件和操作系统指标，如 CPU、内存、磁盘、网络等。
- **指标示例**:
  - `node_cpu_seconds_total`: CPU 使用时间。
  - `node_memory_MemFree_bytes`: 空闲内存大小。
  - `node_filesystem_size_bytes`: 文件系统大小。
- **GitHub**: [prometheus/node_exporter](https://github.com/prometheus/node_exporter)

### IPMI-Exporter

Prometheus 本身不支持直接监控 IPMI 数据，但可以通过 IPMI Exporter 来采集 IPMI 数据。IPMI Exporter 会调用 ipmitool 或**freeipmi** 工具来获取硬件信息，并将其暴露为 Prometheus 可抓取的指标。

已经实现了通过consul去发现target，传递modul给ipmi_exporter，但未实现将用户名/密码传递给ipmi_exporter（需要再研究，或者研读源码）。

配置文件说明：

```yaml
配置文件说明
modules:
定义 IPMI Exporter 的模块。每个模块可以指定一组 IPMI 采集器（collectors）。
这里定义了一个名为 default 的模块，用于默认配置。

user 和 pass:
默认的用户名和密码。这些值会被 Prometheus 通过 relabel_configs 动态传递的参数覆盖。

target:
默认的目标地址（BMC/IPMI 地址）。这个值也会被 Prometheus 动态传递的参数覆盖。

collector:

指定要采集的 IPMI 数据类别。常见的采集器包括：
bmc：BMC 信息。
sensors：传感器数据（如温度、风扇转速等）。
sel：系统事件日志（System Event Log）。
chassis：机箱状态（如电源状态）。
dcmi：数据中心管理接口（DCMI）信息。
fru：现场可更换单元（FRU）信息。

以下采集器配置到配置文件中发现有些不生效，以实际情况为主。
bmc_info
功能: 采集 BMC 的基本信息，如固件版本、IP 地址等。监控 BMC 的状态和配置。
sensor
功能: 采集传感器数据，如温度、电压、风扇转速等。监控硬件健康状况。
sel
功能: 采集系统事件日志（SEL）。用于故障排查和系统审计。
chassis
功能: 采集机箱状态信息，如电源状态。监控机箱电源状态。
fru
功能: 采集现场可更换单元（FRU）信息。用于硬件资产管理和维护。
dcmi
功能: 采集数据中心管理接口（DCMI）数据。用于数据中心环境监控。
lan
功能: 采集 BMC 的网络配置信息。用于网络配置和故障排查。
user
功能: 采集 BMC 用户账户信息。用于用户权限管理和安全配置。
pef
功能: 采集平台事件过滤器（PEF）配置。用于事件管理和告警配置。
sdr
功能: 采集传感器数据记录（SDR）库信息。用于传感器配置和管理。
sel_time
功能: 采集 SEL 时间戳信息。用于时间同步和日志管理。
sensor_thresholds
功能: 采集传感器阈值配置。用于传感器监控和告警配置。
chassis_config
功能: 采集机箱配置信息。用于机箱配置和管理。
serial
功能: 采集串行接口配置信息。 用于串行通信配置和调试。
firewall
功能: 采集 BMC 防火墙配置信息。用于安全配置和访问控制。
pet
功能: 采集平台事件陷阱（PET）信息。用于事件监控和告警。
sol
功能: 采集串行 over LAN (SOL) 配置信息。用于远程服务器管理和故障排查。
raw
功能: 发送原始 IPMI 命令。用于高级用户和开发者进行自定义操作和调试。
oem
功能: 采集 OEM 特定的 IPMI 信息。用于特定硬件厂商的定制功能。
sdrmd
功能: 采集传感器数据记录管理（SDRMD）信息。
ekanalyzer
功能: 采集 IPMI 事件日志分析信息。用于日志分析和故障排查。
```



### nvidia_gpu_exporter

下载连接如下（utkuozdemir，与官网不一致）：

Nvidia GPU exporter for prometheus, using nvidia-smi binary to gather metrics.

源码中包含了Grafana的json文件。

[Releases · utkuozdemir/nvidia_gpu_exporter · GitHub](https://github.com/utkuozdemir/nvidia_gpu_exporter/releases)

---

## 2. 数据库监控

### MySQL Exporter
- **用途**: 监控 MySQL 数据库的性能和状态。
- **指标示例**:
  - `mysql_global_status_connections`: 当前连接数。
  - `mysql_global_status_slow_queries`: 慢查询数量。
- **GitHub**: [prometheus/mysqld_exporter](https://github.com/prometheus/mysqld_exporter)

### PostgreSQL Exporter
- **用途**: 监控 PostgreSQL 数据库的性能和状态。
- **指标示例**:
  - `pg_stat_activity_count`: 当前活动连接数。
  - `pg_stat_user_tables_n_tup_ins`: 表中插入的行数。
- **GitHub**: [prometheus-community/postgres_exporter](https://github.com/prometheus-community/postgres_exporter)

### Redis Exporter
- **用途**: 监控 Redis 数据库的性能和状态。
- **指标示例**:
  - `redis_connected_clients`: 当前连接的客户端数量。
  - `redis_used_memory_bytes`: Redis 使用的内存大小。
- **GitHub**: [oliver006/redis_exporter](https://github.com/oliver006/redis_exporter)

### MongoDB Exporter
- **用途**: 监控 MongoDB 数据库的性能和状态。
- **指标示例**:
  - `mongodb_up`: MongoDB 是否正常运行。
  - `mongodb_opcounters_insert`: 插入操作的数量。
- **GitHub**: [percona/mongodb_exporter](https://github.com/percona/mongodb_exporter)

---

## 3. 网络监控

### Blackbox Exporter
- **用途**: 监控网络服务的可用性，支持 HTTP、HTTPS、DNS、TCP、ICMP 等协议。
- **指标示例**:
  - `probe_success`: 探测是否成功。
  - `probe_duration_seconds`: 探测耗时。
- **GitHub**: [prometheus/blackbox_exporter](https://github.com/prometheus/blackbox_exporter)

### SNMP Exporter
- **用途**: 通过 SNMP 协议监控网络设备（如路由器、交换机）。
- **指标示例**:
  - `snmp_ifHCInOctets`: 网络接口的输入流量。
  - `snmp_sysUpTime`: 设备的运行时间。
- **GitHub**: [prometheus/snmp_exporter](https://github.com/prometheus/snmp_exporter)

---

## 4. Web 服务器和应用监控

### Apache Exporter
- **用途**: 监控 Apache HTTP 服务器的性能和状态。
- **指标示例**:
  - `apache_accesses_total`: 总访问次数。
  - `apache_workers`: 当前工作线程数。
- **GitHub**: [Lusitaniae/apache_exporter](https://github.com/Lusitaniae/apache_exporter)

### Nginx Exporter
- **用途**: 监控 Nginx 服务器的性能和状态。
- **指标示例**:
  - `nginx_connections_active`: 当前活跃连接数。
  - `nginx_requests_total`: 总请求数。
- **GitHub**: [nginxinc/nginx-prometheus-exporter](https://github.com/nginxinc/nginx-prometheus-exporter)



### NGINX-Exporter

#### Nginx 监控工具对比

以下是 `nginx-vts-exporter`、`nginx-lua-prometheus` 和 `nginx-prometheus-exporter` 的详细对比：

| 特性             | nginx-vts-exporter                                           | nginx-lua-prometheus                                         | nginx-prometheus-exporter                                    |
| ---------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **简介**         | 基于 `nginx-module-vts` 模块的 Exporter                      | 基于 Lua 脚本的 Prometheus 指标收集工具                      | 官方推荐的 Nginx 基础指标 Exporter                           |
| **依赖模块**     | `nginx-module-vts`                                           | `ngx_http_lua_module` 或 OpenResty                           | 无额外模块依赖                                               |
| **功能**         | 提供详细的虚拟主机和位置级别的流量统计                       | 支持自定义指标，灵活性高                                     | 提供基础的 Nginx 状态指标（如请求数、连接数等）              |
| **适用场景**     | 需要详细的流量和性能监控                                     | 需要高度自定义的监控指标                                     | 需要基础的 Nginx 监控                                        |
| **性能影响**     | 较低                                                         | 较高（Lua 脚本可能增加 Nginx 的负载）                        | 最低                                                         |
| **配置复杂度**   | 中等（需要编译 Nginx 模块）                                  | 较高（需要熟悉 Lua 脚本）                                    | 最低（直接使用 Nginx 状态模块）                              |
| **指标丰富度**   | 高（支持虚拟主机和位置级别的详细指标）                       | 高（可自定义指标）                                           | 低（仅提供基础指标）                                         |
| **社区支持**     | 较活跃                                                       | 较活跃                                                       | 官方支持，社区活跃                                           |
| **安装方式**     | 需要编译 Nginx 并启用 `nginx-module-vts`                     | 需要安装 OpenResty 或 Lua 模块                               | 直接运行二进制文件                                           |
| **指标暴露方式** | 通过 HTTP 接口暴露 JSON 格式数据                             | 通过 Lua 脚本直接暴露 Prometheus 格式数据                    | 通过 HTTP 接口暴露 Prometheus 格式数据                       |
| **典型指标**     | - 请求数<br>- 连接数<br>- 流量<br>- 响应时间                 | - 自定义请求数<br>- 自定义错误率<br>- 自定义延迟             | - 请求数<br>- 连接数<br>- 活跃连接数                         |
| **GitHub 仓库**  | [nginx-vts-exporter](https://github.com/hnlq715/nginx-vts-exporter) | [nginx-lua-prometheus](https://github.com/knyar/nginx-lua-prometheus) | [nginx-prometheus-exporter](https://github.com/nginxinc/nginx-prometheus-exporter) |

---

#### 总结

- **nginx-vts-exporter**：适合需要详细虚拟主机和位置级别流量统计的场景，但需要编译 Nginx 模块。
- **nginx-lua-prometheus**：适合需要高度自定义指标的场景，但需要熟悉 Lua 脚本并可能增加 Nginx 负载。
- **nginx-prometheus-exporter**：适合只需要基础 Nginx 监控的场景，配置简单，性能影响最小。

根据你的具体需求选择合适的工具：

- 如果需要详细的流量统计，选择 **nginx-vts-exporter**。
- 如果需要自定义指标，选择 **nginx-lua-prometheus**。
- 如果只需要基础监控，选择 **nginx-prometheus-exporter**。



### HAProxy Exporter
- **用途**: 监控 HAProxy 负载均衡器的性能和状态。
- **指标示例**:
  - `haproxy_up`: HAProxy 是否正常运行。
  - `haproxy_server_bytes_out_total`: 服务器输出的字节数。
- **GitHub**: [prometheus/haproxy_exporter](https://github.com/prometheus/haproxy_exporter)

### nacos

在 Nacos 的配置文件 `application.properties` 或 `application.yml` 中，确保以下配置已启用：

```properties
#***********Expose prometheus and health **************************#
management.endpoints.web.exposure.include=prometheus,health
#management.endpoints.web.exposure.include=*
management.metrics.export.prometheus.enabled=true
```

ip:8848/nacos/actuator/prometheus即可获取

---

## 5. 消息队列和流处理监控

### Kafka Exporter
- **用途**: 监控 Apache Kafka 集群的性能和状态。
- **指标示例**:
  - `kafka_topic_partitions`: 主题的分区数量。
  - `kafka_consumer_lag`: 消费者延迟。
- **GitHub**: [danielqsj/kafka_exporter](https://github.com/danielqsj/kafka_exporter)

### RabbitMQ Exporter
- **用途**: 监控 RabbitMQ 消息队列的性能和状态。
- **指标示例**:
  - `rabbitmq_queue_messages`: 队列中的消息数量。
  - `rabbitmq_connections`: 当前连接数。
- **GitHub**: [kbudde/rabbitmq_exporter](https://github.com/kbudde/rabbitmq_exporter)

---

## 6. 云服务和容器监控

### AWS Exporter
- **用途**: 监控 AWS 云服务的资源使用情况。
- **指标示例**:
  - `aws_ec2_cpu_utilization`: EC2 实例的 CPU 使用率。
  - `aws_rds_free_storage_space`: RDS 实例的剩余存储空间。
- **GitHub**: [prometheus/cloudwatch_exporter](https://github.com/prometheus/cloudwatch_exporter)

### cAdvisor
- **用途**: 监控容器资源使用情况（CPU、内存、网络、磁盘等）。
- **指标示例**:
  - `container_cpu_usage_seconds_total`: 容器的 CPU 使用时间。
  - `container_memory_usage_bytes`: 容器的内存使用量。
- **GitHub**: [google/cadvisor](https://github.com/google/cadvisor)

---

## 7. 存储监控

### Ceph Exporter
- **用途**: 监控 Ceph 分布式存储系统的性能和状态。
- **指标示例**:
  - `ceph_osd_up`: OSD 是否正常运行。
  - `ceph_pool_bytes_used`: 存储池的已用字节数。
- **GitHub**: [digitalocean/ceph_exporter](https://github.com/digitalocean/ceph_exporter)

---

## 8. 自定义 Exporter

如果现有的 Exporter 不能满足需求，可以使用 Prometheus 提供的 **Client Libraries** 开发自定义 Exporter。支持的语言包括：
- Go: [prometheus/client_golang](https://github.com/prometheus/client_golang)
- Python: [prometheus/client_python](https://github.com/prometheus/client_python)
- Java: [prometheus/client_java](https://github.com/prometheus/client_java)

---

## 总结

Prometheus 的 Exporter 生态系统非常丰富，覆盖了从主机、数据库、网络设备到云服务和容器的各种监控场景。选择合适的 Exporter 可以快速实现监控需求，而对于特殊场景，也可以通过自定义 Exporter 来扩展功能。

如果需要更详细的 Exporter 列表，可以参考 [Prometheus 官方文档](https://prometheus.io/docs/instrumenting/exporters/) 或 [ExporterHub](https://exporterhub.io/)。