# Prometheus监控介绍
## 1、概述 - 什么是普罗米修斯？

Prometheus是一个开源系统监控和警报工具包，受启发于Google的Brogmon监控系统（相似的Kubernetes是从Google的Brog系统演变而来），从2012年开始由前Google工程师在Soundcloud以开源软件的形式进行研发，并且于2015年早期对外发布早期版本。2016年5月继Kubernetes之后成为第二个正式加入CNCF基金会的项目，同年6月正式发布1.0版本。2017年底发布了基于全新存储层的2.0版本，能更好地与容器平台、云平台配合。

《[Prometheus、Zabbix 和 HertzBeat 的对比](./Prometheus、Zabbix和HertzBeat的对比.md)》

## 2、特点
普罗米修斯的主要特点是：

● 支持多维数据模型由指标名称和键值对标识的时间序列数据  
● 内置时间序列库TSDB（Time Serices Database）  
● 支持PromQL（Promethues Query Language），对数据的查询和分析、图形展示和监控告警。  
● 不依赖分布式存储；单个服务器节点是自治的  
● 支持HTTP 的拉取(pull)方式收集时间序列数据  
● 通过中间网关Pushgateway推送时间序列  
● 通过服务发现或静态配置2种方式发现目标  
● 支持多种可视化和仪表盘，如：grafana   

## 3、核心组件

● Prometheus Server，主要用于抓取数据和存储时序数据，另外还提供查询和 Alert Rule 配置管理。  
● client libraries，用于检测应用程序代码的客户端库。  
● push gateway，用于批量，短期的监控数据的汇总节点，主要用于业务数据汇报等。  
● exporters 收集监控样本数据，并以标准格式向 Prometheus 提供。例如：收集服务器系统数据的   node_exporter, 收集 MySQL 监控样本数据的是 MySQL exporter 等等。  

一般来说可以将Exporter分为2类：  
**直接采集**：这一类Exporter直接内置了对Prometheus监控的支持，比如cAdvisor，Kubernetes，Etcd，Gokit等，都直接内置了用于向Prometheus暴露监控数据的端点。  
**间接采集**：间接采集，原有监控目标并不直接支持Prometheus，因此我们需要通过Prometheus提供的Client Library编写该监控目标的监控采集程序。例如： Mysql Exporter，JMX Exporter，Consul Exporter等。   

《[Prometheus Exporter 介绍](./Prometheus-Exporter介绍.md)》 

《[Grafana学习](./Grafana学习.md)》  Grafana学习总结。

● 用于告警通知管理的 alertmanager 。  

## 4、基础架构

![](prometheus架构图.png)

从这个架构图，也可以看出 Prometheus 的主要模块包含， Server, Exporters, Pushgateway, PromQL, Alertmanager, WebUI 等。

它大致使用逻辑是这样：

1. Prometheus server 定期从静态配置的 targets 或者服务发现的 targets 拉取数据（Targets是Prometheus采集Agent需要抓取的采集目标）

2. 当新拉取的数据大于配置内存缓存区的时候，Prometheus 会将数据持久化到磁盘（如果使用 remote storage 将持久化到云端）。

3. Prometheus 可以配置 rules，然后定时查询数据，当条件触发的时候，会将 alerts 推送到配置的 Alertmanager。

4. Alertmanager 收到警告的时候，可以根据配置（163，钉钉等），聚合，去重，降噪，最后发送警告。在Prometheus Server中支持基于PromQL创建告警规则，如果满足PromQL定义的规则，则会产生一条告警，而告警的后续处理流程则由AlertManager进行管理。在AlertManager中我们可以与邮件，Slack等等内置的通知方式进行集成，也可以通过Webhook自定义告警处理方式。AlertManager即Prometheus体系中的告警处理中心。国外使用pagerduty，国内使用flashduty.

5. 可以使用 API， Prometheus Console 或者 Grafana 查询和聚合数据。

   **PushGateway** ：由于Prometheus数据采集基于Pull模型进行设计，因此在网络环境的配置上必须要让Prometheus Server能够直接与Exporter进行通信。 当这种网络需求无法直接满足时，就可以利用PushGateway来进行中转。可以通过PushGateway将内部网络的监控数据主动Push到Gateway当中。而Prometheus Server则可以采用同样Pull的方式从PushGateway中获取到监控数据。

## 5、Prometheus与Zabbix的对比

实在不知道怎么选？参考如下：  
物理机、硬件设备的监控推荐使用Zabbix  
而docker容器，Kubernetes监控推荐用Prometheus  
云服务器厂商自带有监控系统，有的监控不全面，也可以搭配zabbix和Prometheus来一起使用。  

《[Prometheus、Zabbix 和 HertzBeat 的对比](./Prometheus、Zabbix和HertzBeat的对比.md)》

《[大规模设备监控方案（优化版）](./大规模设备监控方案（优化版）.md)》

# 安装

二进制安装Prometheus 、 docker安装prometheus   

参照：

```go
先登录  https://www.yuque.com/linge365/wye95i   再输入密码io6x

wget https://github.com/prometheus/prometheus/releases/download/v2.37.6/prometheus-2.37.6.linux-amd64.tar.gz

```

## 各应用功能(Grafana/cadvisor/node-exporter)

●  Prometheus 采集数据   
●  Grafana 用于图表展示   
●  alertmanager 用于接收 Prometheus 发送的告警信息   
●  node-exporter 用于收集操作系统和硬件信息的metrics   
●  cadvisor 用于收集docker的相关metrics   

## 指标(Metric)的4种类型

Prometheus 底层存储上其实并没有对指标做类型的区分，都是以时间序列的形式存储，但是为了方便用户的使用和理解不同监控指标之间的差异，Prometheus 定义了：

 counter（计数器） 、gauge（仪表盘） 、 histogram（直方图） 以及 summary （摘要）这四种 Metrics 类型。

Gauge/Counter 是数值指标，代表数据的变化情况，Histogram/Summary 是统计类型的指标，表示数据的分布情况。  
在Exporter返回的样本数据中，其注释中也包含了该样本的类型。例如：  

```sh
# HELP process_resident_memory_bytes Resident memory size in bytes.
# TYPE process_resident_memory_bytes gauge
process_resident_memory_bytes 1.3586432e+08
```

Counter：只增不减的计数器  
Gauge：可增可减的仪表盘  
使用Histogram（直方图）和Summary（摘要）分析数据分布情况



## 各类exporters

《[Prometheus Exporter 介绍](./Prometheus-Exporter介绍.md)》

所有可以向Prometheus提供监控样本数据的程序都可以被称为一个Exporter。而Exporter的一个实例称为target，如下所示，Prometheus通过轮询的方式定期从这些target中获取样本数据:

![](./prometheus-pull数据.png)

注：安装好Exporter后会暴露一个http://ip:端口/metrics的HTTP服务，通过Prometheus添加配置- targets: ['node_exporter:9100']（默认会加上/metrics），Prometheus就可以采集到这个http://ip:端口/metrics里面所有监控样本数据

### 常用的exporters

![](./常用的Exporter.jpg)

### 各类exporters的来源

#### 社区提供的

[各类exporters链接](https://prometheus.io/docs/instrumenting/exporters/)

windows-exporter  [下载地址](https://github.com/prometheus-community/windows_exporter)

实操过程中发现新版本的windows-exporter安装后无法启动服务，安装老版本的可以（windows_exporter-0.18.1-amd64.msi）

#### 用户自定义的

除了直接使用社区提供的Exporter程序以外，用户还可以基于Prometheus提供的Client Library创建自己的Exporter程序，目前Promthues社区官方提供了对以下编程语言的支持：Go、Java/Scala、Python、Ruby。同时还有第三方实现的如：Bash、C++、Common Lisp、Erlang,、Haskeel、Lua、Node.js、PHP、Rust等。

### 将Exporter分为两类

#### 直接采集型

这类Exporter直接内置了相应的应用程序，用于向Prometheus直接提供Target数据支持。这样设计的好处是，可以更好地监控各自系统的内部运行状态，同时也适合更多自定义监控指标的项目实施。例如cAdvisor、Kubernetes等，它们均内置了用于向Prometheus提供监控数据的端点。

#### 间接采集型

原始监控目标并不直接支持Prometheus，需要我们使用Prometheus提供的Client Library编写该监控目标的监控采集程序，用户可以将该程序独立运行，去获取指定的各类监控数据值。例如，由于Linux操作系统自身并不能直接支持Prometheus，用户无法从操作系统层面上直接提供对Prometheus的支持，因此单独安装Node exporter，还有数据库或网站HTTP应用类等Exporter。

### Exporter规范

所有的Exporter程序都需要按照Prometheus的规范，返回监控的样本数据。以Node Exporter为例。

以#开始的行通常都是注释内容。这些样本数据集合说明如下：

● 以#HELP开始的行，表示metric的帮助与说明注释，可以包含当前监控指标名称和对应的说明信息。  
● 以#TYPE开始的行，表示定义metric类型，可以包含当前监控指标名称和类型，类型有Counter（计数器）、Gauge（仪表盘）、Histogram（直方图）、Summary（摘要）和Untyped。  
● 非#开头的行，就是监控样本数据  

每一个监控指标之前都会有一段类似于如下形式的信息：

```powershell
# HELP node_cpu Seconds the cpus spent in each mode.
# TYPE node_cpu counter
node_cpu{cpu="cpu0",mode="idle"} 362812.7890625
# HELP node_load1 1m load average.
# TYPE node_load1 gauge
node_load1 3.0703125
```

其中HELP用于解释当前指标的含义，TYPE则说明当前指标的数据类型。在上面的例子中node_cpu的注释表明当前指标是cpu0上idle进程占用CPU的总时间，CPU占用时间是一个只增不减的度量指标，从类型中也可以看出node_cpu的数据类型是计数器(counter)，与该指标的实际含义一致。又例如node_load1该指标反映了当前主机在最近一分钟以内的负载情况，系统的负载情况会随系统资源的使用而变化，因此node_load1反映的是当前状态，数据可能增加也可能减少，从注释中可以看出当前指标类型为仪表盘(gauge)，与指标反映的实际含义一致。

除了这些以外，在当前页面中根据物理主机系统的不同，你还可能看到如下监控指标：

- node_boot_time：系统启动时间
- node_cpu：系统CPU使用量
- nodedisk*：磁盘IO
- nodefilesystem*：文件系统用量
- node_load1：系统负载
- nodememeory*：内存使用量
- nodenetwork*：网络带宽
- node_time：当前系统时间
- go_*：node exporter中go相关指标
- process_*：node exporter自身进程相关运行指标

### 黑盒监控和白盒监控

监控系统需要能够有效地支持白盒监控和黑盒监控。通过**白盒**能够了解其内部的实际运行状态，通过对监控指标的观察能够预判可能出现的问题，从而对潜在的不确定因素进行优化。而**黑盒监控**，常见的如HTTP探针，TCP探针等，可以在系统或者服务在发生故障时能够快速通知相关的人员进行处理。通过建立完善的监控体系，从而达到以下目的：

- **长期趋势分析**：通过对监控样本数据的持续收集和统计，对监控指标进行长期趋势分析。例如，通过对磁盘空间增长率的判断，我们可以提前预测在未来什么时间节点上需要对资源进行扩容。

- **对照分析**：两个版本的系统运行资源使用情况的差异如何？在不同容量情况下系统的并发和负载变化如何？通过监控能够方便的对系统进行跟踪和比较。

- **告警**：当系统出现或者即将出现故障时，监控系统需要迅速反应并通知管理员，从而能够对问题进行快速的处理或者提前预防问题的发生，避免出现对业务的影响。

- **故障分析与定位**：当问题发生后，需要对问题进行调查和处理。通过对不同监控监控以及历史数据的分析，能够找到并解决根源问题。

- **数据可视化**：通过可视化仪表盘能够直接获取系统的运行状态、资源使用情况、以及服务运行状态等直观的信息。



## AlertManager

在Prometheus Server中支持基于PromQL创建告警规则，如果满足PromQL定义的规则，则会产生一条告警，而告警的后续处理流程则由AlertManager进行管理。在AlertManager中我们可以与邮件，Slack等等内置的通知方式进行集成，也可以通过Webhook自定义告警处理方式。AlertManager即Prometheus体系中的告警处理中心。

## PushGateway

由于Prometheus数据采集基于Pull模型进行设计，因此在网络环境的配置上必须要让Prometheus Server能够直接与Exporter进行通信。 当这种网络需求无法直接满足时，就可以利用PushGateway来进行中转。可以通过PushGateway将内部网络的监控数据主动Push到Gateway当中。而Prometheus Server则可以采用同样Pull的方式从PushGateway中获取到监控数据。

# 服务发现



![prometheus服务发现过程](./prometheus服务发现过程.png)

## Consul

安装（集群模式） - 未实现

配置持久化 - 未实现

