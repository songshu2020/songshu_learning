# MYSQL启停:

```bash
1、查状态、启停命令：service mysql status/start (mysql安装包中有support-files，将下面的mysqld拷贝到/etc/rc.d/init.d目录下可执行service命令) <br>
2、修改密码：mysqladmin -u root password "root" <br>
3、解决不能连接的问题
> mysql> update user set host='%' where host='::1';
>Query OK, 1 row affected (0.00 sec)
>Rows matched: 1  Changed: 1  Warnings: 0

>mysql> flush privileges;
>Query OK, 0 rows affected (0.00 sec)

update user set password='root' where host='%' and user='root';

修改任意主机以用户root和密码连接mysql服务器
grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;
flush privileges;
```

# MySql修改root密码

```bash
一：进入MySQL  client  **
mysql>update mysql.user set password=PASSWORD("新密码") where User="root";
二：

mysql>flush privileges;
mysql>quit
```

# docker方式启动

```bash
docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7
```






# 一级标题   

## 二级标题

###  三级标题

#### 四级标题

##### 五级标题



* 项目一
+ 项目二
- 项目三

换行，在两行中间直接回车不换行    
方法一：上面一行后面按多个空格然后回车

方法二：上面一行空一行在写下一行

* 表格  


表头一  |  表头二   | 表头三
:----|----:|:---:
内容一 |内容二 | 内容三

* 引用 

>一级引用
>>二级引用

* 强调

*强调* 或者 _强调_ (斜体)    
**强调**或者 __强调__  (粗体)    

* 显示代码           
  `prinf('代码');`
  
* 插入连接：[[链接地址]](http://127.0.0.1)     
  
* 插入图片：[[本地图片]](file:///D:/LZKF/Workspace_Maven/spring5/doc/ascii.jpg)  

 

 

