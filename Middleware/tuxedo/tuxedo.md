Tuxedo使用共享内存存储公告牌，用来公告进程状态信息和需要在进程间共享或传递的数据。  

-------------------------------------------------------------

# TUXCONFIG

Tuxedo的配置文件称为UBBCONFIG或ubb，包含了域（Domain）、逻辑机器(Machine)、服务器组(Group)、服务进程(Server)、服务(Service)的定义。运行前，需要把UBBCONFIG装载成二进制文件，称为TUXCONFIG。  

Tuxedo服务启动时，执行tpsvrinit()函数，可以打开一些如数据库之类的资源供以后使用  
Tuxedo服务停止时，执行tpsvrdown()函数，关闭资源  
服务程序调用tpreturn()函数来结束服务请求，并返回一个缓冲区，必要时，将它传给客户程序。  

--------------------------------------------------------

ATMI环境支持的C/S通信方式：请求/应答式通信、回话通信、队列通信、事件代理通信、消息通知  
    请求/应答式通信：同步调用(tpcall)、异步调用(tpacall)、嵌套调用、转发调用(tpforward)  
                       转发调用和嵌套调用类似，不同的是最里层的嵌套服务可以直接给客户程序一个响应，而不必按照调用栈 逐级返回。  

   回话方式：tpsend()/tprecv()  基于事件，分通告和代理  
              void (**p)(): 定义了一个指向函数指针的指针p  
              tpsetunsol(p) : 将p指向的函数func设置为客户机的事件处理器。  
              tpchkunsol(): 检查意外事件    

```properties
事件代理： tppost()/tpsubscribe()  消息发布/订阅
            Tuxedo提供了两个事件代理器(TMUSREVT  TMSYSEVT)来处理订阅请求。

队列存储： tpenqueue() / tpdequeue()
          Tuxedo/Q用到了Tuxedo提供的两个服务器：消息队列服务器(TMQUEUE)和消息转发服务器(TMQFORWARD)
```

---------------------------

多系统多机之间通信需要每台机器上都有一个Bridge进程，通过TCP/IP通信，Bridge进程维持一个长连接，一旦建立不会断掉。   

说明：  
WS（Workstation Extension Product）用于指TUXEDO产品的客户端部分  
WSC Workstation Client  
WSL（Workstation Listener） TUXEDO系统自带的一个SERVER，它侦听一个指定的端口，WSC最初与该SERVER建立连接  
WSH（Workstation Handler）TUXEDO系统自带的一个SERVER，由它处理WSC与TUXEDO SERVER之间的通讯。  
Bulletin Board（公告板）TUXEDO把系统的配置保存在一个共享内存中，该共享内存称为公告板（BB）  
BBL TUXEDO的管理进程，主要对公告板等进行管理  

Workstation Client与TUXEDO SERVER建立连接的过程为：  

```ini
1．    WSC 调用tpinit()或tpchkauth()  
2．    WSC采用在WSNADDR中指定的IP地址与服务端的WSL建立连接  
3．    WSL为该WSC指定一个WSH，并把该WSH的侦听端口返回给WSC  
4．    WSC采用返回的端口与指定的WSH建立连接，并与WSL断开连接，这之后WSC与TUXEDO SERVER之间的通讯通过WSH进行处理，与WSL无关。  
5．    tpinit()或tpchkauth()调用返回。   
```



----------------------------------------------------------