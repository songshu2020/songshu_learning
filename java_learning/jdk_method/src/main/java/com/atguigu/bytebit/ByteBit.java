package com.atguigu.bytebit;

import java.io.UnsupportedEncodingException;

/**
 * ByteBit
 *
 * @Author: 马伟奇
 * @CreateTime: 2020-05-05
 * @Description:
 */
public class ByteBit {
    public static void main(String[] args) throws UnsupportedEncodingException {
//        String a = "abcd";
//    	System.out.println(Integer.toBinaryString(-8));
        String a = "a";
        byte[] bytes = a.getBytes("GBK");
        for (byte aByte : bytes) {
            int c = aByte;
            char tmpChar = (char) c;
            System.out.println(tmpChar + ":" + c);
            // byte 字节，对应的bit是多少
            String s = Integer.toBinaryString(c);
            System.out.println(s);
        }
//        char[] charArray = a.toCharArray();
//        for (int i = 0; i < charArray.length; i++) {
//			char tmpChar = charArray[i];
//			String binaryString = Integer.toBinaryString(tmpChar);
//			System.out.println(String.format("%s  的ascii为：%d， binaryString:%s", tmpChar, (int)tmpChar, binaryString));
//		}
    }
}