package com.lzyh.jdk.resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

/**
 * 读取properties配置文件，两种方式，src下和绝对路径
 * @author lz10272
 *
 */
public class ResourcesBundleTest {
	private Logger logger = LogManager.getLogger(getClass());

	@Test
	public void testProperties() {
//		ResourceBundle bundle = ResourceBundle.getBundle("log4j");//src下
		ResourceBundle bundle = ResourceBundle.getBundle("com.lzyh.jdk.resources.test");//package下
		Locale locale = bundle.getLocale();
		
		logger.info("locale-->>" + locale);
		Set<String> keySet = bundle.keySet();
		for (String key : keySet) {
			String value = bundle.getString(key);
			logger.info(String.format("log4j key:%s,value:%s", key, value));
		}
	}
	
	@Test
	public void testFileProperties() throws IOException {
		String filePath = "D:\\LZKF\\Workspace_Maven\\spring5\\src\\test\\resources\\txdemo\\jdbc.properties";
		InputStream stream = new FileInputStream(filePath);
		ResourceBundle bundle = new PropertyResourceBundle(stream );
		
		Set<String> keySet = bundle.keySet();
		for (String key : keySet) {
			String value = bundle.getString(key);
			logger.info(String.format("log4j key:%s,value:%s", key, value));
		}
	}
}
