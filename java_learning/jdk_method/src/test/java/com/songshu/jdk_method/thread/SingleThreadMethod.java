package com.songshu.jdk_method.thread;

import java.util.concurrent.CountDownLatch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.songshu.jdk_method.utils.DateUtil;



public class SingleThreadMethod implements Runnable {

	private Logger logger = LogManager.getLogger(getClass());
	
	private String myParam;
	private CountDownLatch countDown;
	
	public void run() {
		myrun();
		
		getCountDown().countDown();
	}

	private void myrun() {
		logger.info(String.format("开始运行-->>%s, getMyParam-->>%s, threadGroup-->>%s", DateUtil.getFullTimeStamp(), getMyParam(), Thread.currentThread().getThreadGroup()));
		SynchronizedClass.mySynchronizedMethod(getMyParam());
	}

	public String getMyParam() {
		return myParam;
	}

	public void setMyParam(String myParam) {
		this.myParam = myParam;
	}

	public CountDownLatch getCountDown() {
		return countDown;
	}

	public void setCountDown(CountDownLatch countDown) {
		this.countDown = countDown;
	}
	
}
