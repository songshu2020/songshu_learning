package com.songshu.jdk_method;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

/**
    *  字符ascii
 * @author lz10272
 *
 */
public class CharTest {
	private Logger logger = LogManager.getLogger(getClass());

	@Test
	public void test() {
		for (int i = 0; i < 256; i++) {
			char a = (char) i;
			logger.info("{}-->>{}", i, a);
		}
	}
	
	@Test
	public void testZkl() {
		String str = "周开良";
		char[] charArray = str.toCharArray();
		
		logger.info(str + ". toCharArray-->" + Arrays.toString(charArray));
		for (int i = 0; i < charArray.length; i++) {
			char c = charArray[i];
			String octalString = Integer.toOctalString(c);
			String string = Integer.toString(c, 10);
			logger.info("{}转换10进制为{}", c, string);
			
			Integer octInteger = new Integer(octalString);
			int decInt = Integer.parseInt(octInteger.toString(), 8);
			
			logger.info("八进制{}转换为十进制为{}",octalString,decInt);
		}
	}
	
	/**
	    *  进制转换
	 */
	@Test
	public void jinzhiTest() {
		char a = 'a';
		String decString = Integer.toString(a, 10);
		logger.info("字符a的十进制ascii为{}", decString);
		logger.info("字符a的八进制ascii为{}", Integer.toOctalString(a));
		logger.info("字符a的二进制ascii为{}", Integer.toBinaryString(a));
		logger.info("字符a的十六进制ascii为{}", Integer.toHexString(a));
		
		int valueOf = Integer.valueOf(decString);
		String octalString = Integer.toOctalString(valueOf);
		logger.info("十进制{}转换八进制为{}",decString, octalString);
		logger.info("十进制{}转换二进制为{}",decString, Integer.toBinaryString(valueOf));
		logger.info("十进制{}转换十六进制为{}",decString, Integer.toHexString(valueOf));

		
	}
	
	@Test
	public void jinzhiTest2() {
//		0xCD6D0
		String str = "周开良啊一";
		char[] charArray = str.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			char c = charArray[i];
			int intC = c;
			String hexStr = Integer.toString(intC, 16);
			logger.info("i:{}, c:{}, ascii(DEC):{},ascii(HEX):{}", i, c, intC, hexStr);
		}
		String hex = "D6D0";
		
		int parseInt = Integer.parseInt(hex, 16);
		logger.info("{}转换为十进制为：{}", hex, parseInt);
		logger.info("十进制21608转换为十六进制为:{}", Integer.toString(21608, 16));
		logger.info("十进制1515转换为十六进制为:{}", Integer.toString(1515, 16));
		logger.info("十进制21608转换为十六进制为:{}", Integer.toHexString(21608));
		String hex2 = "4E00";
		int parseInt2 = Integer.parseInt(hex2, 16);
		char hex2Char = (char) parseInt2;
		logger.info("{}转换为十进制为：{}，字符为：{}", hex2, parseInt2, hex2Char);


	}
}
