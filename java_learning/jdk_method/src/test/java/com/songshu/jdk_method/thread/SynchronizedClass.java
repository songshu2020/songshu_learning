package com.songshu.jdk_method.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 增加synchronized修饰符后，多线程执行mySynchronizedMethod()方法代码块时会排队执行
 * @author lz10272
 *
 */
public class SynchronizedClass {
	private static Logger logger = LogManager.getLogger(SynchronizedClass.class);


	public static
	synchronized
	void mySynchronizedMethod(String param) {
		String threadName = Thread.currentThread().getName();
		logger.info(String.format("param:%s, threadName:%s", param, threadName));
		
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}
}
