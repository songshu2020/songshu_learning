package com.songshu.jdk_method;

/**
 * 
 * unicode字符集
 * @author lz10272
 *
 */
public class UnicodeTest {
	/**
	 * Unicode转中文方法
	 * 
	 * @param unicode
	 * @return
	 */
	private static String unicodeToCn(String unicode) {
		/** 以 \ u 分割，因为java注释也能识别unicode，因此中间加了一个空格 */
		String[] strs = unicode.split("\\\\u");
		String returnStr = "";
		// 由于unicode字符串以 \ u 开头，因此分割出的第一个字符是""。
		for (int i = 1; i < strs.length; i++) {
			String string = strs[i];
			Integer valueOf = Integer.valueOf(string, 16);
			int intValue = valueOf.intValue();
			returnStr += (char) intValue;
		}
		return returnStr;
	}

	/**
	 * 中文转Unicode
	 * 
	 * @param cn
	 * @return
	 */
	private static String cnToUnicode(String cn) {
		char[] chars = cn.toCharArray();
		String returnStr = "";
		for (int i = 0; i < chars.length; i++) {
			returnStr += "\\u" + Integer.toString(chars[i], 16);
		}
		return returnStr;
	}

	// 测试
	public static void main(String[] args) {
		// Unicode码
		String aa = "\\u5916\\u56fd\\u4eba\\u88ab\\u4e2d\\u56fd\\u8fd9\\u4e00\\u5927\\u52a8\\u4f5c\\u6298\\u670d";
		// 转中文
		String cnAa = unicodeToCn(aa);
		System.out.println("Unicode转中文结果： " + cnAa);// 转Unicode
		String unicodeAa = cnToUnicode(cnAa);
		System.out.println("中文转Unicode结果： " + unicodeAa);
		System.out.println("-->" + cnToUnicode("周开良严"));

		// 4E00-9FA5是基本汉字,只占一个字符，也就是一个char，也就是2字节，也就是16位
		String s = "一";// Unicode编码:4E00
		String s1 = "龥";// Unicode编码:9FA5
		// ?是汉字扩展字符,占两个字符，也就是两个char，也就是4字节，也就是32位
		String s2 = "?";// Unicode编码:20000
		System.err.println("测试字符s:" + s);
		System.err.println("测试字符s1:" + s1);
		System.err.println("测试字符s2:" + s2);
		System.err.println("测试字符s长度:" + s.length());
		System.err.println("测试字符s2长度:" + s2.length());
		System.out.println("s转为二进制:" + Integer.toBinaryString(s.charAt(0)));
		System.out.println("s2转为二进制:" + Integer.toBinaryString(s2.charAt(0)) );
		System.out.println("s2转为unicode:" + cnToUnicode(s2));

//		char c = '?';// 当我们设置一个占多字符的汉字给char的时候，编译器会报错
	}

}
