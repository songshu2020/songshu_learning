package com.songshu.jdk_method;

import org.junit.Test;

public class IntegerTest {

	@Test
	public void test() {
		Integer xx = 2; // 装箱 调⽤了 Integer.valueOf(2)
		int yy = xx; // 拆箱 调用了 X.intValue()
		
		Integer x = new Integer(123);
		Integer y = new Integer(123);
		System.out.println(x);
		System.out.println(y);
		System.out.println((x == y) + "   ========="); // false
		System.out.println(x.intValue() == y.intValue());
		Integer z = Integer.valueOf(123);
		Integer k = Integer.valueOf(123);
		System.out.println(z == k); // true
		
		int valueOf = Integer.valueOf(222);
		System.out.println(valueOf);
	}

}
