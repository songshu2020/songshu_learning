package com.atguigu.spring5.demo4.reflect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

/**
 * 反射概念学习
 * 
 * @author lz10272
 *
 */
public class ReflectTest {
	protected static Logger logger = LogManager.getLogger(ReflectTest.class);

	@SuppressWarnings("rawtypes")
	@Test
	public void testReflect() throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, IOException, InterruptedException {
		Class userDTOCls = Class.forName("com.atguigu.spring5.demo4.reflect.UserDTO");
		String packageName = userDTOCls.getPackage().getName();
		logger.info(String.format("runtime Class--->>%s, packageName-->>%s", userDTOCls, packageName));
		Object o = userDTOCls.newInstance();
		
		logger.info("====================methods=======================");
//		Method[] methods = userDTOCls.getMethods();
		Method[] methods = userDTOCls.getDeclaredMethods();
		for (Method method : methods) {
			Class<? extends Method> runtimeClass = method.getClass();
			int modifiers = method.getModifiers();
			String name = method.getName();
			Class<?> returnType = method.getReturnType();
			logger.info(String.format("runtimeClass:%s, modifiers:%s, name:%s, returnType:%s", runtimeClass, modifiers, name, returnType));
		}
		logger.info("====================contructors=======================");
//		Constructor[] constructors = userDTOCls.getConstructors();//public
		Constructor[] constructors = userDTOCls.getDeclaredConstructors();//
		for (Constructor constructor : constructors) {
			int modifiers = constructor.getModifiers();
			String name = constructor.getName();
			Parameter[] parameters = constructor.getParameters();
			logger.info(String.format("name:%s, modifiers:%s, parameters:%s", name, modifiers, Arrays.toString(parameters)));
		}
		logger.info("====================fields=======================");
//		Field[] fields = userDTOCls.getFields();
		Field[] fields = userDTOCls.getDeclaredFields();
		for (Field field : fields) {
			String name = field.getName();
			int modifiers = field.getModifiers();
			Class<?> type = field.getType();
			logger.info(String.format("name:%s, value:%s, modifiers:%s, type:%s", name, "", modifiers, type));
		}
		logger.info("====================fields===set===get=================");
		Field nameField = userDTOCls.getDeclaredField("name");
		nameField.setAccessible(true);
		nameField.set(o, "周开良");
		Object object = nameField.get(o);
		logger.info("object value:" + object);
		
		InputStream systemResourceAsStream = ClassLoader.getSystemResourceAsStream("log4j.properties");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(systemResourceAsStream));
		String content = "";
		String tempStr = "";
		while((tempStr = bufferedReader.readLine()) != null) {
			content += tempStr + "\n";
		}
		logger.info(content);
		
	}
}
