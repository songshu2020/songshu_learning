package com.atguigu.spring5.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Task2 {
	
	protected static Logger logger = LogManager.getLogger(Task2.class);

	@Autowired
	private SingleThread singleThread;
	
	public void test() {
		String name = Thread.currentThread().getName();
		logger.info("2222222当前线程名：{}", name);
		
		try {
			if (singleThread.IS_RUNNING) {
				logger.info("single正在执行2222");
			} else {
				singleThread.singleTest("2222");
			}
		} catch (InterruptedException e) {
			logger.error(e,e);
		}
	}
}
