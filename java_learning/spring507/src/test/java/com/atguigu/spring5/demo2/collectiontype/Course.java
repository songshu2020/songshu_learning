package com.atguigu.spring5.demo2.collectiontype;

//课程类
public class Course {
    private String cname; //课程名称
    private String chubanshe;//出版社
    public void setChubanshe(String chubanshe) {
		this.chubanshe = chubanshe;
	}


	public void setCname(String cname) {
		System.out.println("设置Course属性");
        this.cname = cname;
    }

	@Override
	public String toString() {
		return "Course [cname=" + cname + ", chubanshe=" + chubanshe + "]";
	}
    

}
