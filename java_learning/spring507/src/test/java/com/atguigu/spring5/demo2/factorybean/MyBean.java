package com.atguigu.spring5.demo2.factorybean;

import org.springframework.beans.factory.FactoryBean;

import com.atguigu.spring5.demo2.collectiontype.Course;

public class MyBean implements FactoryBean<Course> {

    //定义返回bean
    public Course getObject() throws Exception {
        Course course = new Course();
        course.setCname("abc");
        course.setChubanshe("");
        return course;
    }

    public Class<?> getObjectType() {
        return null;
    }

    public boolean isSingleton() {
        return false;
    }
}
