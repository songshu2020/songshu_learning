package com.atguigu.spring5.demo3.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class UserDaoImpl2 implements UserDao {
	
	protected Logger logger = LogManager.getLogger(this.getClass());

	
	public void add() {

		logger.info("调用了自己的实现类");
	}

}
