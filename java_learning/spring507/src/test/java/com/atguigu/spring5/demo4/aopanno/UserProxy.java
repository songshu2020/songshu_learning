package com.atguigu.spring5.demo4.aopanno;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

//增强的类
@Component
@Aspect  //生成代理对象
@Order(3)
public class UserProxy {
	
	protected Logger logger = LogManager.getLogger(this.getClass());


    //相同切入点抽取
    @Pointcut(value = "execution(* com.atguigu.spring5.demo4.aopanno.User.add(..))")
    public void pointdemo() {

    }

    //前置通知
    //@Before注解表示作为前置通知
    @Before(value = "pointdemo()")
    public void before() {
        logger.info("before.........");
    }

    //后置通知（返回通知）
    @AfterReturning(value = "execution(* com.atguigu.spring5.demo4.aopanno.User.add(..))")
    public void afterReturning() {
        logger.info("afterReturning.........");
    }

    //最终通知
    @After(value = "execution(* com.atguigu.spring5.demo4.aopanno.User.add(..))")
    public void after() {
        logger.info("after.........");
    }

    //异常通知
    @AfterThrowing(value = "execution(* com.atguigu.spring5.demo4.aopanno.User.add(..))" ,throwing = "ex")
    public void afterThrowing(Throwable ex) {
        logger.info("afterThrowing.........");
        logger.warn(ex.getMessage(),ex);
    }

    //环绕通知
    @Around(value = "execution(* com.atguigu.spring5.demo4.aopanno.User.add(..))")
    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        logger.info("环绕之前.........");

        //被增强的方法执行
        proceedingJoinPoint.proceed();

        logger.info("环绕之后.........");
    }
}
