package com.atguigu.spring5.txdemo.dao;

import java.util.List;

import com.atguigu.spring5.txdemo.entity.UserDO;

public interface ZklDao {
	
	
	/**
	 * 增加
	 * @param user
	 * @return
	 */
	public int add(UserDO user);
	
	/**
	 * 修改
	 * @param user
	 * @return
	 */
	public int update(UserDO user);
	
	/**
	 * 删除
	 * @param user
	 * @return
	 */
	public int delete(UserDO user);
	
	
	
	public int[] batchAdd(List<UserDO> userDoList);
	
	public int[] batchDelete();

	/**
	 * 查询返回string等基础类型
	 * 
	 * @return
	 */
	public String queryString();

	/**
	 * 查询返回自定义对象
	 * 
	 * @param id
	 * @return
	 */
	public UserDO queryUserDo(String id);

	/**
	 * 查询所有
	 * 
	 * @return
	 */
	public List<UserDO> queryAll();
}
