package com.atguigu.spring5.txdemo.entity;

public class UserDO {
	private String id;
	private String username;
	private double money;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	@Override
	public String toString() {
		return "UserDO [id=" + id + ", username=" + username + ", money=" + String.valueOf(money) + "]";
	}
	

}
