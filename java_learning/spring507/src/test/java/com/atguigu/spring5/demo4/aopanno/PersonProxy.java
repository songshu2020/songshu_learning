package com.atguigu.spring5.demo4.aopanno;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
//@Aspect
//@Order(1)
public class PersonProxy {
	protected Logger logger = LogManager.getLogger(this.getClass());
    //后置通知（返回通知）
    @Before(value = "execution(* com.atguigu.spring5.demo4.aopanno.User.add(..))")
    public void beforeReturning() {
        logger.info("Person Before.........");
    }
    
    @After(value = "execution(* com.atguigu.spring5.demo4.aopanno.User.add(..))")
    public void afterReturning() {
    	logger.info("Person After.........");
    }
}
