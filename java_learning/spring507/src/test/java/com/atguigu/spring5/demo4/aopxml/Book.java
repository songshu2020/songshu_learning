package com.atguigu.spring5.demo4.aopxml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Book {
	protected Logger logger = LogManager.getLogger(this.getClass());

    public void buy() {
        logger.info("buy.............");
        int i = 10/1;
    }
}
