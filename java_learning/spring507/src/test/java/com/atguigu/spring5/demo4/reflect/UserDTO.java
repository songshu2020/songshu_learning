package com.atguigu.spring5.demo4.reflect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class UserDTO implements Serializable {

	private String name;
	private int sex;
	private String region;
	private String idno;
	private boolean canJi;
	private Date borth;
	private ArrayList<String> friends;
	
	
	public UserDTO(String name, int sex, String region, String idno, boolean canJi, Date borth, ArrayList<String> friends) {
		super();
		this.name = name;
		this.sex = sex;
		this.region = region;
		this.idno = idno;
		this.canJi = canJi;
		this.borth = borth;
		this.friends = friends;
	}
	
	UserDTO() {
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getIdno() {
		return idno;
	}
	public void setIdno(String idno) {
		this.idno = idno;
	}
	public boolean isCanJi() {
		return canJi;
	}
	public void setCanJi(boolean canJi) {
		this.canJi = canJi;
	}
	public Date getBorth() {
		return borth;
	}
	public void setBorth(Date borth) {
		this.borth = borth;
	}
	public ArrayList<String> getFriends() {
		return friends;
	}
	public void setFriends(ArrayList<String> friends) {
		this.friends = friends;
	}
	@Override
	public String toString() {
		return "UserDTO [name=" + name + ", sex=" + sex + ", region=" + region + ", idno=" + idno + ", canJi=" + canJi + ", borth=" + borth + ", friends=" + friends + "]";
	}
	
}