package com.atguigu.spring5.demo4.aopxml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

public class BookProxy {

	protected Logger logger = LogManager.getLogger(this.getClass());

	public void before() {
		logger.info("before.........");
	}

	public void after() {
		logger.info("after.........");
	}

	public Object around(ProceedingJoinPoint proceedingJoinPoing) throws Throwable {
		logger.info("环绕之前.........");
		Object proceed = proceedingJoinPoing.proceed();
		logger.info("环绕之后.........");
		return proceed;
	}
	
	public void throwing(Throwable ex) {
		logger.warn(ex,ex);
	}
}
