package com.atguigu.spring5.txdemo.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atguigu.mybatis.taccount.dao.TaccountMapper;
import com.atguigu.mybatis.taccount.domain.TaccountExample;
import com.atguigu.spring5.txdemo.dao.ZklDaoImpl;
import com.atguigu.spring5.txdemo.entity.UserDO;

/**
 * JdbcTemplate 实现对数据库操作，包括增删改查。
 * 
 * @author lz10272
 * @version 2021-4-13 08:56:29
 *
 */
public class ZklTest {

	protected Logger logger = LogManager.getLogger(this.getClass());

	@Test
	public void testGetObject() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("txdemo/bean1.xml");
		ZklDaoImpl zklDaoImpl = (ZklDaoImpl) context.getBean("zklDaoImpl");
		String queryString = zklDaoImpl.queryString();
		logger.info("queryString-->>" + queryString);

		UserDO queryUserDo = zklDaoImpl.queryUserDo("2");
		logger.info("queryUserDo-->>" + queryUserDo);

		List<UserDO> queryAll = zklDaoImpl.queryAll();
		logger.info("queryAll-->>" + queryAll);

	}

	@Test
	public void testCURD() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("txdemo/bean1.xml");
		ZklDaoImpl zklDaoImpl = (ZklDaoImpl) context.getBean("zklDaoImpl");
		
		UserDO user = new UserDO();
		user.setId("223");
		user.setUsername("哈哈哈哈哈哈哈哈");
		user.setMoney(new Double("1234565.83339"));
		
//		int add = zklDaoImpl.add(user);
//		logger.info("insert结果-->>" + add);
		
		
//		int update = zklDaoImpl.update(user);
//		logger.info("update结果-->>" + update);
		
		
//		int delete = zklDaoImpl.delete(user);
//		logger.info("delete结果-->>" + delete);


		UserDO user1 = new UserDO();
		user1.setId("777");
		user1.setUsername("dafd");
		user1.setMoney(new Double("987.11"));
		
		
		UserDO user2 = new UserDO();
		user2.setId("888");
		user2.setUsername("最终哈哈哈");
		user2.setMoney(new Double("111.55"));
		
		UserDO user3 = new UserDO();
		user3.setId("999");
		user3.setUsername("哈哈");
		user3.setMoney(new Double("888"));
		
		List<UserDO> userDoList = new ArrayList<UserDO>();
		userDoList.add(user1);
		userDoList.add(user2);
		userDoList.add(user3);
		int[] batchAdd = zklDaoImpl.batchAdd(userDoList );
		logger.info("batchAdd结果-->>" + Arrays.toString(batchAdd));
		
	}
	
	
	@Test
	public void testMybatis() {
		logger.info("start init context");
//		PropertyConfigurator.configure(this.getClass().getResource("/log4j.properties"));
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mybatis/applicationContext.xml");
		TaccountMapper mapper = (TaccountMapper) context.getBean("taccountMapper");
		
		TaccountExample example = null;
		long countByExample = mapper.countByExample(example);
		logger.info("countByExample结果-->>" + countByExample);

	}
}
