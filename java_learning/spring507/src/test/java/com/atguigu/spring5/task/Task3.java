package com.atguigu.spring5.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Task3 {
	
	protected static Logger logger = LogManager.getLogger(Task3.class);

	@Autowired
	private SingleThread singleThread;
	
	public void test() {
		String name = Thread.currentThread().getName();
		logger.info("3333333当前线程名：{}", name);
		
		try {
			if (singleThread.IS_RUNNING) {
				logger.info("single正在执行3333");
			} else {
				singleThread.singleTest("3333");
			}
		} catch (InterruptedException e) {
			logger.error(e,e);
		}
	}
}
