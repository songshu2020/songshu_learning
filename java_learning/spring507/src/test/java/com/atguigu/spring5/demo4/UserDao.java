package com.atguigu.spring5.demo4;

public interface UserDao {
    public int add(int a,int b);
    public String update(String id);
    public void myMethod();
}
