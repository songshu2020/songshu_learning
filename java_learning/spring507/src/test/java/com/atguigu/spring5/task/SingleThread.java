package com.atguigu.spring5.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class SingleThread {

	private Logger logger = LogManager.getLogger(getClass());
	
	public volatile boolean IS_RUNNING = false;
	
	public void singleTest(String str) throws InterruptedException {
		IS_RUNNING = true;
		logger.info("single test start.{}", str);
		Thread.sleep(30000);
		logger.info("single test end.{}", str);
		IS_RUNNING = false;
	}
}
