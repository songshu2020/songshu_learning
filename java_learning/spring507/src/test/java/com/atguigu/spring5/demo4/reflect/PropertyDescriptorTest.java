package com.atguigu.spring5.demo4.reflect;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 实体类测试，反射（reflect）
 * @author lz10272
 * @version 2021-4-7 11:38:22
 */
public class PropertyDescriptorTest {
	
	protected static Logger logger = LogManager.getLogger(PropertyDescriptorTest.class);

	public static void main(String[] args) throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		PropertyDescriptorTest test = new PropertyDescriptorTest();
		test.test();
	}

	private void test() throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		UserDTO dto = new UserDTO();
		Field[] declaredFields = dto.getClass().getDeclaredFields();
		for (int i = 0; i < declaredFields.length; i++) {
			Field field = declaredFields[i];
			logger.info("fieldName-->" + field.getName() + " -- " + field.toGenericString());
			Class<?> type = field.getType();
			Type genericType = field.getGenericType();
			
			logger.info("type-->>" + type);
			logger.info("genericType-->>" + genericType.getTypeName());
			PropertyDescriptor pd = new PropertyDescriptor(field.getName(), dto.getClass());
			Method readMethod = pd.getReadMethod();
			Method writeMethod = pd.getWriteMethod();
			if ("name".equals(field.getName())) {
				writeMethod.invoke(dto, "你好pd");
			}
			logger.info(String.format("读方法-readMethod.getName():%s, 写方法-writeMethod.getName():%s", readMethod.getName(),writeMethod.getName() ));
		}
		logger.info(dto);
	}

}
