package com.atguigu.spring5.demo4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserDaoImpl implements UserDao {
	protected  Logger logger = LogManager.getLogger(this.getClass());

	public int add(int a, int b) {
        logger.info("add方法执行了.....");
        return a+b;
    }

    public String update(String id) {
        logger.info("update方法执行了....id:" + id);
        return id;
    }

	public void myMethod() {
		 logger.info("myMethod方法执行了....");
	}
}
