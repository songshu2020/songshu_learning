package com.atguigu.spring5.demo4.aopanno;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

//被增强的类
@Component
public class User {
	protected Logger logger = LogManager.getLogger(this.getClass());

    public void add() {
        int i = 10/0;
        logger.info("add.......result-->>" + i);
    }
}
