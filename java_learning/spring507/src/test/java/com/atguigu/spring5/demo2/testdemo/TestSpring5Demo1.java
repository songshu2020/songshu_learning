package com.atguigu.spring5.demo2.testdemo;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.druid.pool.DruidDataSource;
import com.atguigu.spring5.demo2.autowire.Emp;
import com.atguigu.spring5.demo2.bean.Orders;
import com.atguigu.spring5.demo2.collectiontype.Book;
import com.atguigu.spring5.demo2.collectiontype.Course;
import com.atguigu.spring5.demo2.collectiontype.Stu;

public class TestSpring5Demo1 {

    @Test
    public void testCollection1() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("demo2/bean1.xml");
        Stu stu = context.getBean("stu", Stu.class);
        stu.test();
    }

    @Test
    public void testCollection2() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("demo2/bean2.xml");
        Book book1 = context.getBean("book", Book.class);//singleton   prototype
        Book book2 = context.getBean("book", Book.class);
//        book1.test();
//        book2.test();
        System.out.println(book1);
        System.out.println(book2);
    }

    @Test
    public void test3() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("demo2/bean3.xml");
        Course course = context.getBean("myBean", Course.class);
        System.out.println(course);
    }

    @Test
    public void testBean3() {
//        ApplicationContext context =
//                new ClassPathXmlApplicationContext("demo2/bean.4.xml");
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("demo2/bean4.xml");
        Orders orders = context.getBean("orders", Orders.class);
        System.out.println("第四步 获取创建bean实例对象");
        System.out.println("操作beans：" + orders);

        //手动让bean实例销毁
        context.close();
    }

    @Test
    public void test4() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("demo2/bean5.xml");
        Emp emp = context.getBean("emp", Emp.class);
        emp.test();
        System.out.println(emp);
    }
    
    @Test
    public void testDataSource() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("demo2/bean6.xml");
        DruidDataSource druidDataSource = context.getBean("dataSource", DruidDataSource.class);
        System.out.println("url:" + druidDataSource.getUrl());
        System.out.println("dbPwd:" + druidDataSource.getPassword());
        System.out.println(druidDataSource);
    }
}
