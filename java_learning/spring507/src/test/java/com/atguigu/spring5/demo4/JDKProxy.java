package com.atguigu.spring5.demo4;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JDKProxy {

	protected static Logger logger = LogManager.getLogger(JDKProxy.class);

	public static void main(String[] args) {
		// 创建接口实现类代理对象
		Class[] interfaces = { UserDao.class };
//		Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), interfaces, new InvocationHandler() {
//			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//				return null;
//			}
//		});

        UserDaoImpl userDao = new UserDaoImpl();
        UserDao dao = (UserDao)Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), interfaces, new UserDaoProxy(userDao));
        int result = dao.add(1, 2);
        logger.info("result:"+result);
        
        dao.update("111");
        dao.myMethod();
	}
}

//创建代理对象代码
class UserDaoProxy implements InvocationHandler {
	protected static Logger logger = LogManager.getLogger(UserDaoProxy.class);

	// 1 把创建的是谁的代理对象，把谁传递过来
	// 有参数构造传递
	private Object obj;

	public UserDaoProxy(Object obj) {
		this.obj = obj;
	}

	// 增强的逻辑
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// 方法之前
		logger.info("方法之前执行...." + method.getName() + " :传递的参数..." + Arrays.toString(args));

		// 被增强的方法执行
		Object res = method.invoke(obj, args);

		// 方法之后
		logger.info("方法之后执行...." + obj);
		logger.info("============");
		return res;
	}
}
