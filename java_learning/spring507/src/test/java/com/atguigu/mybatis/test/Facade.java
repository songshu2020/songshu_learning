package com.atguigu.mybatis.test;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.defaults.DefaultSqlSessionFactory;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Facade {
	static String driver = "com.mysql.jdbc.Driver";
	static String url = "jdbc:mysql://98.11.51.152:3306/test?useUnicode=true&characterEncoding=utf8";
	static String user = "root";
	static String password = "root";
	static String validTable = "t_account";
	
	private static Logger logger = LogManager.getLogger(Facade.class);

	
	public static void main(String[] args) throws SQLException {
		
		String id = "mytestEnvironment";
		DataSource dataSource = GetDatasource.getInstance(driver, url, user, password, validTable);
		TransactionFactory transactionFactory = new JdbcTransactionFactory();
		transactionFactory.newTransaction(dataSource.getConnection());
		
		logger.info("数据库连接成功");
		Environment environment = new Environment(id , transactionFactory, dataSource);
		logger.info("environment配置成功：{}" , environment);
		// Configuration
		Configuration configuration = new Configuration(environment );
		
		SqlSessionFactory sqlSessionFactory = new DefaultSqlSessionFactory(configuration);
		
		SqlSession openSession = sqlSessionFactory.openSession();
		logger.info("openSession成功，{}" , openSession);

		String statement = "select * from t_account where id = 1";
		Object selectOne = openSession.selectOne(statement);

		logger.info("query成功，{}" , selectOne);
	}

}
