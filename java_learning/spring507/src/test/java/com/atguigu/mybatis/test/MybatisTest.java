package com.atguigu.mybatis.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.fastjson.JSON;
import com.atguigu.mybatis.taccount.dao.TaccountMapper;
import com.atguigu.mybatis.taccount.domain.Taccount;
import com.atguigu.mybatis.taccount.domain.TaccountExample;
import com.atguigu.mybatis.taccount.domain.TaccountExample.Criteria;

/**
 * mybatis 实现对数据库操作，包括增删改查。
 * 
 * @author lz10272
 * @version 2021-4-13 08:56:29
 *
 */
public class MybatisTest {

	protected Logger logger = LogManager.getLogger(this.getClass());

	@Test
	public void testMybatisCount() {
		logger.info("start init context");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mybatis/applicationContext.xml");
		TaccountMapper mapper = (TaccountMapper) context.getBean("taccountMapper");

		TaccountExample example = new TaccountExample();
		Criteria createCriteria = example.createCriteria();
//		 createCriteria.andIdNotBetween("100", "300");
		
		List<String> values = new ArrayList<String>();
		values.add("1");
		values.add("2");
		values.add("777");
		createCriteria.andIdIn(values);

		long countByExample = mapper.countByExample(example);
		logger.info("countByExample结果-->>" + countByExample);

		example.setOrderByClause("id");

		List<Taccount> selectByExample = mapper.selectByExample(example);
		logger.info("selectByExample:{}", JSON.toJSONString(selectByExample));
		
		String id = "959";
		logger.info("delete result-->>{}" , mapper.deleteByPrimaryKey(id));
		Taccount record = new Taccount();
		record.setId(id);
		record.setMoney(123.456);
		record.setUsername("及垃圾死了");
		int insert = mapper.insert(record);
		
		logger.info("insert result-->{}", insert);
	}
}
