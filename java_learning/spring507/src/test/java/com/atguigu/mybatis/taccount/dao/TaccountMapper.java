package com.atguigu.mybatis.taccount.dao;

import com.atguigu.mybatis.taccount.domain.Taccount;
import com.atguigu.mybatis.taccount.domain.TaccountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TaccountMapper {
    long countByExample(TaccountExample example);

    int deleteByExample(TaccountExample example);

    int deleteByPrimaryKey(String id);

    int insert(Taccount record);

    int insertSelective(Taccount record);

    List<Taccount> selectByExample(TaccountExample example);

    Taccount selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Taccount record, @Param("example") TaccountExample example);

    int updateByExample(@Param("record") Taccount record, @Param("example") TaccountExample example);

    int updateByPrimaryKeySelective(Taccount record);

    int updateByPrimaryKey(Taccount record);
}