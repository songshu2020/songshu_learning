## Feature
> 1、本服务主要用于spring5学习使用（com.atguigu.spring5包下）   README.md   
> 2、学习设计模式使用（com.lzyh.designpatterns包下）    README-DESIGNPATTERNS.md

# All interface:
1、 xxxx    功能:

# 阅读代码

课堂笔记在doc/Spring5框架课堂笔记.pdf    

+ demo1（以下内容在com.atguigu.spring5.demo1中演示）

1、什么是 IOC  
（1）控制反转，把对象创建和对象之间的调用过程，交给 Spring 进行管理  
（2）使用 IOC 目的：为了耦合度降低  
（3）做入门案例就是 IOC 实现  
2、 IOC 底层原理  
（1） xml 解析、工厂模式、反射  

*IOC（BeanFactory 接口）*   
1、 IOC 思想基于 IOC 容器完成， IOC 容器底层就是对象工厂  
2、 Spring 提供 IOC 容器实现两种方式：（两个接口）  
（1） BeanFactory： IOC 容器基本实现，是 Spring 内部的使用接口，不提供开发人员进行用   
** 加载配置文件时候不会创建对象，在获取对象（使用）才去创建对象**     
（2） ApplicationContext： BeanFactory 接口的子接口，提供更多更强大的功能，一般由开发人员进行使用   
** 加载配置文件时候就会把在配置文件对象进行创建 **   
** bean管理  指 1.创建对象；2.注入属性 **   

+ demo2（以下内容在com.atguigu.spring5.demo2中演示） 
   
*注入集合属性 *

*IOC操作 Bean管理（FactoryBean）*       
1、 Spring 有两种类型 bean，一种普通 bean，另外一种工厂 bean（** FactoryBean **）  
2、普通 bean：在配置文件中定义 bean 类型就是返回类型      
3、工厂 bean：在配置文件定义 bean 类型可以和返回类型不一样                     
第一步 创建类，让这个类作为工厂 bean，实现接口 FactoryBean                    
第二步 实现接口里面的方法，在实现的方法中定义返回的 bean 类型      

*IOC 操作 Bean 管理（bean 作用域）*   
scope 属性值： singleton(单实例，默认) ；  prototype(多实例)    
**singleton 和 prototype 区别 **  
第一 singleton 单实例， prototype 多实例     
第二 设置 scope 值是 singleton 时候，加载 spring 配置文件时候就会创建单实例对象；设置 scope 值是 prototype 时候，不是在加载 spring 配置文件时候创建 对象，在调用
getBean 方法时候创建多实例对象     

*IOC 操作 Bean 管理（bean 生命周期）*   
bean 生命周期（从对象创建到对象销毁的过程）     
（1）通过构造器创建 bean 实例（无参数构造）                        
（2）为 bean 的属性设置值和对其他 bean 引用（调用 set 方法）                    
（3）调用 bean 的初始化的方法（需要进行配置初始化的方法）                     
（4） bean 可以使用了（对象获取到了）                       
（5）当容器关闭时候，调用 bean 的销毁的方法（需要进行配置销毁的方法）       
bean 的**后置处理器**， bean 生命周期有七步                         
（1）通过构造器创建 bean 实例（无参数构造）                       
（2）为 bean 的属性设置值和对其他 bean 引用（调用 set 方法）                                                
（3）把 bean 实例传递 bean 后置处理器的方法 postProcessBeforeInitialization(实现BeanPostProcessor接口)                         
（4）调用 bean 的初始化的方法（需要进行配置初始化的方法）                               
（5）把 bean 实例传递 bean 后置处理器的方法 postProcessAfterInitialization(实现BeanPostProcessor接口)                              
（6） bean 可以使用了（对象获取到了）                     
（7）当容器关闭时候，调用 bean 的销毁的方法（需要进行配置销毁的方法）               
   
*IOC 操作 Bean 管理（xml 自动装配）*    
自动装配：根据指定装配规则（属性名称或者属性类型）， Spring 自动将匹配的属性值进行注入   
bean标签属性autowire，配置自动装配
autowire属性常用两个值：     
**byName**根据属性名称注入 ，注入值bean的id值和类属性名称一样        
**byType**根据属性类型注入      

*IOC 操作 Bean 管理(外部属性文件)*     
把外部 properties 属性文件引入到 spring 配置文件中(引入context 名称空间)

+ demo3（以下内容在com.atguigu.spring5.demo3中演示） 

*IOC 操作 Bean 管理(基于注解方式)*      
1、注解（引入aop依赖）：      
（1）注解是代码特殊标记，格式： @注解名称(属性名称=属性值, 属性名称=属性值..)         
（2）使用注解，注解作用在类上面，方法上面，属性上面                     
（3）使用注解目的：简化 xml 配置         
2、Spring 针对 Bean 管理中创建对象提供注解           
（1） @Component                 
（2） @Service                
（3） @Controller                 
（4） @Repository                            
**上面四个注解功能是一样的，都可以用来创建 bean 实例**      
@Qualifier：根据名称进行注入  
这个@Qualifier 注解的使用，和上面@Autowired 一起使用  
@Autowired //根据类型进行注入   
@Qualifier(value = "userDaoImpl1") //根据名称进行注入    
private UserDao userDao;   
 
@Resource：可以根据类型注入，可以根据名称注入
//@Resource //根据类型进行注入
@Resource(name = "userDaoImpl1") //根据名称进行注入
private UserDao userDao;

@Value：注入普通类型属性
@Value(value = "abc")
private String name;

+ demo4（以下内容在com.atguigu.spring5.demo4中演示） 

**面向切面编程（方面）**， 利用 AOP 可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的耦合度降低，提高程序的可重用性，同时提高了开发的效率。通俗描述：不通过修改源代码方式，在主干功能里面添加新功能。   
AOP 底层使用动态代理，有两种情况动态代理：
第一种 有接口情况，使用 JDK 动态代理；
第二种 没有接口情况，使用 CGLIB 动态代理     

Spring 框架一般都是基于 AspectJ 实现 AOP 操作。AspectJ 不是 Spring 组成部分，是独立 AOP 框架，一般把 AspectJ 和 Spirng 框架一起使用，进行 AOP 操作     
 
类java.lang.reflect.Proxy(API规范)  
方法   static Object newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler h)   
          返回一个指定接口的代理类实例，该接口可以将方法调用指派到指定的调用处理程序。   
 方法有三个参数：
第一参数，类加载器；
第二参数，增强方法所在的类，这个类实现的接口，支持多个接口；
第三参数，实现这个接口 InvocationHandler，创建代理对象，写增强的部分  。   
AOP术语（连接点、切入点、通知（增强）、切面）见《课堂笔记.pdf》     
**通知类型**：前置、后置、环绕、异常、最终        

*切入点表达式*   
（1）切入点表达式作用：知道对哪个类里面的哪个方法进行增强；      
（2）语法结构： execution(【权限修饰符】【返回类型】【类全路径】【方法名称】(【参数列表】) )。       
举例 1：对 com.atguigu.dao.BookDao 类里面的 add 进行增强：
execution(* com.atguigu.dao.BookDao.add(..))      
举例 2：对 com.atguigu.dao.BookDao 类里面的所有的方法进行增强：
execution(* com.atguigu.dao.BookDao.* (..))       
举例 3：对 com.atguigu.dao 包里面所有类，类里面所有方法进行增强：
execution(* com.atguigu.dao.*.* (..))            

注：有多个增强类对同一个方法进行增强，设置增强类优先级，在增强类上面添加注解 @Order(数字类型值)，数字类型值越小优先级越高

两种方式：完全使用注解开发和基于xml开发    


+ txdemo（以下内容在com.atguigu.spring5.txdemo中演示）       
   
**JdbcTemplate：** Spring 框架对 JDBC 进行封装，使用 JdbcTemplate 方便实现对数据库操作       
ZklTest.java类的 testCURD 方法中中测试了insert，update，delete，batchUpdate等方法。             
ZklTest.java类的 testGetObject 方法中测试了查询返回基本类型数据(queryForObject)、查询返回自定义实体类(queryForObject)、查询返回自定义实体类集合(query)等方法。  
注：留意query方法入参RowMapper的实现类BeanPropertyRowMapper的构造方法                         

**事务**是数据库操作最基本单元，逻辑上一组操作，要么都成功，如果有一个失败所有操作都失败      
事务四个特性（ACID）（1）原子性(atomicity)、（2）一致性(consistency)、（3）隔离性(isolation)、（4）持久性(durability)    

**事务管理介绍**：      
1、事务添加到 JavaEE 三层结构里面 Service 层（业务逻辑层）        
2、在 Spring 进行事务管理操作有两种方式： 编程式事务管理和声明式事务管理（使用）                          
3、声明式事务管理：                
（1）基于注解方式（使用）             
（2）基于 xml 配置文件方式                          
4、在 Spring 进行声明式事务管理，底层使用 AOP 原理                  
5、 Spring 事务管理 API：接口（PlatformTransactionManager.class），代表事务管理器，这个接口针对不同的框架提供不同的实现类

**propagation**：事务传播行为（多事务方法直接进行调用，这个过程中事务 是如何进行管理的），spring定义了7中事物传播行为。  详见doc/分析图          
**isolation**：事务隔离级别（事务有特性成为隔离性，多事务操作之间不会产生影响。不考虑隔离性产生很多问题。有三个读问题：脏读、不可重复读、虚（幻）读）。  
脏读：一个未提交事务读取到另一个未提交事务的数据  
不可重复读：一个未提交事务读取到另一提交事务修改数据  
虚读：一个未提交事务读取到另一提交事务添加数据   
**timeout**：超时时间，事务需要在一定时间内进行提交，如果不提交进行回滚。默认值是 -1 ，设置时间以秒单位进行计算。       
**readOnly**：是否只读。读：查询操作，写：添加修改删除操作。 默认值 false，表示可以查询，可以添加修改删除操作。设置成 true 之后，只能查询。          
**rollbackFor**：回滚。设置出现哪些异常进行事务回滚。     
**noRollbackFor**：不回滚。设置出现哪些异常不进行事务回滚。                 


+ webflux以下内容在工程springboot2中演示，其中某些内容无法正常运行）

> SpringMVC 采用命令式编程， Webflux 采用异步响应式编程    
> SpringMVC 方式实现，同步阻塞的方式，基于 SpringMVC+Servlet+Tomcat    
> SpringWebflux 方式实现，异步非阻塞 方式，基于 SpringWebflux+Reactor+Netty  

SpringWebflux（基于函数式编程模型）       
（1）在使用函数式编程模型操作时候，需要自己初始化服务器                                
（2）基于函数式编程模型时候，有两个核心接口： RouterFunction（实现路由功能，请求转发给对应的 handler）和 HandlerFunction（处理请求生成响应的函数）。核心任务定义两个函数式接口的实现并且启动需要的服务器。      
（3 ） SpringWebflux 请 求 和 响 应 不 再 是 ServletRequest 和 ServletResponse ， 而 是ServerRequest 和 ServerResponse                 
第一步 把注解编程模型工程复制一份 ，保留 entity 和 service 内容                      
第二步 创建 Handler（具体实现方法）           




