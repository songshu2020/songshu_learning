DWR 是一个 Java 开源库，帮助你实现 Ajax 网站。它可以让你在浏览器中的 Javascript 代码调用 Web 服务器上的 Java，就像在 Java 代码就在浏览器中一样。

DWR应用中，所有的“/dwr/\*”的请求都由这个DWRServlet来处理。

当Web服务器启动的时候，DWRServlet调用init()方法完成初始化，设置日志级别、实例化DWRServlet用到的单例类、读取包括dwr.xml在内的配置文件。当“/dwr/\*”请求送达的时候，DWRServlet的doGet，doPost方法调用processor.handle（req，res）方法，根据请求的URL完成如下事务。



请求URL为“/dwr/”根请求：将请求转向“/dwr/index.html”；

请求URL为“/dwr/index.html”：列出当前客户端可远程调用的全部Java类，页面仅供调试的时候使用；

请求URL为“/dwr/text/\*”：为单独的Java类创建测试页面，页面仅供调试的时候使用；

请求URL为“/dwr/engine.js”或者“/dwr/uril.js”或者“/dwr/deprecated.js”：从dwr.jar包中读取相应Javascript文件的文件流，显示在页面上；

请求URL为“/dwr/interface/\*”：将在dwr.xml中定义的Java类及其方法转化为Javascript函数；

请求URL为“/dwr/exec/”：处理远程Javascript请求；

请求URL为其他：设置http请求状态码为404，返回错误信息为“Page not found. In debug/test mode try viewing /[WEB-APP]/dwr/”。

既定的Java类已经通过DWR转化成可供客户端使用Javascript调用的方法和对象了。接下来就是使用Javascript调用这些暴露的方法和对象了。DWR最大的挑战在于如何将Java同步调用执行的特性与Ajax的异步特性结合起来。

DWR采用了XMLHttpRequest对象的回调机制来解决上述矛盾，即当响应数据从服务器返回是调用用户指定的回调函数。DWR提供两种方式为Javascript指定回调函数，即将回调函数名称加入参数列表，或者在参数列表中增加一个包含数据对象的调用。