# Spring的DAO理念 

​      DAO(Data Access Object)是用于访问数据的对象，虽然我们在大多数情况下，将数据保存在数据库中，但这并不是唯一的选择，你也可以将数据存储到文件中或LDAP中。 DAO不但屏蔽了数据存储的最终介质的不同，也屏蔽了具体的实现技术的不同。

# Spring jdbcTemplate 

Spring中jdbcTemplate的用法实例  --- java持久层
spring自身没有实现连接池，一般都是对第三方连接池的包装，常见的有C3P0，dbcp以及最近比较流行的boneCP等，这几个配置都差不多太多

```xml
	<bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="${jdbc.driverClassName}"/>
        <property name="url" value="${jdbc.url}"/>
        <property name="username" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
    </bean> 
```

其中，batchUpdate适合于批量增、删、改操作；
         update(…)：使用于增、删、改操作；
         execute（）：执行一个独立的sql语句，包括ddl语句；
         queryForInt ：查询出一个整数值
		 

```java
	 jdbcTemplate作为一个bean注入后在编码中可以直接autowrite后使用。可以用下面两种方式取数据：
	 List list = jdbcTemplate.queryForList(sql);
	 List<BankInfo> list2 = jdbcTemplate.query(sql, new BeanPropertyRowMapper(BankInfo.class));

	 logger.debug("查询成功结束，总共有--->> " + list.size());
	 for (int i = 0; i < list.size(); i++) {
		Map map = (Map)list.get(i);
		logger.info("id-->" + map.get("id") + "  bankcode-->" + map.get("bankcode") + "  bankname-->" + map.get("bankname") + "  last_login-->" + map.get("last_login") );
	 }
```

 
		 

# Spring JNDI 

JNDI 即 java命名目录服务



# Spring InitializingBean 

1：spring为bean提供了两种初始化bean的方式，实现InitializingBean接口，实现afterPropertiesSet方法，或者在配置文件中同过init-method指定，两种方式可以同时使用  
2：实现InitializingBean接口是直接调用afterPropertiesSet方法，比通过反射调用init-method指定的方法效率相对来说要高点。但是init-method方式消除了对spring的依赖  
3：如果调用afterPropertiesSet方法时出错，则不调用init-method指定的方法。  

# ContextLoaderListener作用详解 

ContextLoaderListener监听器的作用就是启动Web容器时，自动装配ApplicationContext的配置信息。因为它实现了ServletContextListener这个接口，在web.xml配置这个监听器，启动容器时，就会默认执行它实现的方法。至于ApplicationContext.xml这个配置文件部署在哪，如何配置多个xml文件，书上都没怎么详细说明。现在的方法就是查看它的API文档。在ContextLoaderListener中关联了ContextLoader这个类，所以整个加载配置过程由ContextLoader来完成。看看它的API说明。 

第一段说明ContextLoader可以由 ContextLoaderListener和ContextLoaderServlet生成。如果查看ContextLoaderServlet的API，可以看到它也关联了ContextLoader这个类而且它实现了HttpServlet这个接口。

 第二段，ContextLoader创建的是 XmlWebApplicationContext这样一个类，它实现的接口是WebApplicationContext->ConfigurableWebApplicationContext->ApplicationContext->BeanFactory这样一来spring中的所有bean都由这个类来创建  

第三段,讲如何部署applicationContext的xml文件。  
如果在web.xml中不写任何参数配置信息，默认的路径是/WEB-INF/applicationContext.xml，在WEB-INF目录下创建的xml文件的名称必须是applicationContext.xml；  
如果是要自定义文件名可以在web.xml里加入contextConfigLocation这个context参数：  

```xml
<context-param>  
        <param-name>contextConfigLocation</param-name>  
        <param-value>  
            /WEB-INF/classes/applicationContext-*.xml   
        </param-value>  
    </context-param>  
```

在\<param-value\> \</param-value\>里指定相应的xml文件名，如果有多个xml文件，可以写在一起并一“,”号分隔。上面的applicationContext-*.xml采用通配符，比如这那个目录下有applicationContext-ibatis-base.xml，applicationContext-action.xml，applicationContext-ibatis-dao.xml等文件，都会一同被载入。

由此可见applicationContext.xml的文件位置就可以有两种默认实现：  

第一种：直接将之放到/WEB-INF下，之在web.xml中声明一个listener；  

第二种：将之放到classpath下，但是此时要在web.xml中加入\<context-param\>，用它来指明你的applicationContext.xml的位置以供web容器来加载。按照Struts2 整合spring的官方给出的档案，写成：

```xml
<context-param>  
    <param-name>contextConfigLocation</param-name>  
    <param-value>/WEB-INF/applicationContext-*.xml,classpath*:applicationContext-*.xml</param-value> 
</context-param>
```

# Spring定时任务

method是工作类中要执行的方法  

initial-delay是任务第一次被调用前的延时，单位毫秒  

fixed-delay是上一个调用完成后再次调用的延时  

fixed-rate是上一个调用开始后再次调用的延时（不用等待上一次调用完成）  

cron是表达式，表示在什么时候进行任务调度。  

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:task="http://www.springframework.org/schema/task"
       xsi:schemaLocation="
            http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.1.xsd
            http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.1.xsd
            http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-4.1.xsd">

    <!-- 配置注解扫描 -->
    <context:component-scan base-package="com.rhwayfun.task"/>

    <task:scheduler id="taskScheduler" pool-size="100" />

    <task:scheduled-tasks scheduler="taskScheduler">
        <!-- 每半分钟触发任务 -->
        <task:scheduled ref="app" method="execute1" cron="30 * * * * ?"/>
        <!-- 每小时的10分30秒触发任务 -->
        <task:scheduled ref="app" method="execute2" cron="30 10 * * * ?"/>
        <!-- 每天1点10分30秒触发任务 -->
        <task:scheduled ref="app" method="execute3" cron="30 10 1 * * ?"/>
        <!-- 每月20号的1点10分30秒触发任务 -->
        <task:scheduled ref="app" method="execute4" cron="30 10 1 20 * ?"/>
        <!-- 每年10月20号的1点10分30秒触发任务 -->
        <task:scheduled ref="app" method="execute5" cron="30 10 1 20 10 ?"/>
        <!-- 每15秒、30秒、45秒时触发任务 -->
        <task:scheduled ref="app" method="execute6" cron="15,30,45 * * * * ?"/>
        <!-- 15秒到45秒每隔1秒触发任务 -->
        <task:scheduled ref="app" method="execute7" cron="15-45 * * * * ?"/>
        <!-- 每分钟的每15秒时任务任务，每隔5秒触发一次 -->
        <task:scheduled ref="app" method="execute8" cron="15/5 * * * * ?"/>
        <!-- 每分钟的15到30秒之间开始触发，每隔5秒触发一次 -->
        <task:scheduled ref="app" method="execute9" cron="15-30/5 * * * * ?"/>
        <!-- 每小时的0分0秒开始触发，每隔3分钟触发一次 -->
        <task:scheduled ref="app" method="execute10" cron="0 0/3 * * * ?"/>
        <!-- 星期一到星期五的10点15分0秒触发任务 -->
        <task:scheduled ref="app" method="execute11" cron="0 15 10 ? * MON-FRI"/>
    </task:scheduled-tasks>

</beans>
```



# spring-mybatis学习笔记

什么是mybatis  
MyBatis是支持普通SQL查询，存储过程和高级映射的优秀持久层框架。MyBatis消除了几乎所有的JDBC代码和参数的手工设置以及结果集的检索。MyBatis使用简单的XML或注解用于配置和原始映射，将接口和Java的POJOs（Plan Old Java Objects，普通的Java对象）映射成数据库中的记录.
orm工具的基本思想  
无论是用过的hibernate,mybatis,你都可以法相他们有一个共同点：  

1. 从配置文件(通常是XML配置文件中)得到 sessionfactory.
2. 由sessionfactory  产生 session
3. 在session 中完成对数据的增删改查和事务提交等.
4. 在用完之后关闭session 。
5. 在java 对象和 数据库之间有做mapping 的配置文件，也通常是xml 文件。



# 下载jar包的网站

http://mvnrepository.com/
http://findjar.com
http://sourceforge.net/

# springMVC实现原理

SpringMVC框架执行步骤（SpringMVC使用Servlet嵌入）： 

![](../pic/springMVC实现原理.png)

1、客户端发出一个http请求给web服务器，web服务器对http请求进行解析，如果匹配DispatcherServlet的请求映射路径（在web.xml中指定），web容器将请求转交给DispatcherServlet.   
2、DipatcherServlet接收到这个请求之后将根据请求的信息（包括URL、Http方法、请求报文头和请求参数Cookie等）以及HandlerMapping的配置找到处理请求的处理器（Handler）。  
3-4、DispatcherServlet根据HandlerMapping找到对应的Handler,将处理权交给Handler（Handler将具体的处理进行封装），再由具体的HandlerAdapter对Handler进行具体的调用。  
5、Handler对数据处理完成以后将返回一个ModelAndView()对象给DispatcherServlet。  
6、Handler返回的ModelAndView()只是一个逻辑视图并不是一个正式的视图，DispatcherSevlet通过ViewResolver将逻辑视图转化为真正的视图View。  
7、Dispatcher通过model解析出ModelAndView()中的参数进行解析最终展现出完整的view并返回给客户端。  

# 附：spring家族

![](../pic/spring家族.png)