# 其他学习笔记   
##### 开启JMX，用于jconsole和jvisualvm命令监控使用    
`-Djava.rmi.server.hostname=10.160.13.111  #远程服务器ip，即本机ip`    
`-Dcom.sun.management.jmxremote #允许JMX远程调用，jdk1.6之前需要，之后后面默认开启`   
`-Dcom.sun.management.jmxremote.port=3214  #自定义jmx 端口号`      
`-Dcom.sun.management.jmxremote.ssl=false  # 是否需要ssl 安全连接方式`                      
`-Dcom.sun.management.jmxremote.authenticate=false #是否需要秘钥`        

##### 以下谈论的是Applet的远程调试技术，实际上对于其他java程序一样适用，只需要使用文中参数启动java程序即可                      

-XDebug               启用调试。                    
-Xnoagent             禁用默认sun.tools.debug调试器。                    
-Djava.compiler=NONE  禁止 JIT 编译器的加载。                
-Xrunjdwp             加载JDWP的JPDA参考执行实例。                                       
transport             用于在调试程序和 VM 使用的进程之间通讯。                 
dt_socket             套接字传输。                      
dt_shmem              共享内存传输，仅限于 Windows。                 
server=y/n            VM 是否需要作为调试服务器执行。                          
address=3999          调试服务器的端口号，客户端用来连接服务器的端口号。                 
suspend=y/n           是否在调试客户端建立连接之后启动 VM 。                               

Java远程调试方式有两种方式进行远程调试，为Socket Listen方式和Socket Attach方式                               
                          
Socket Attach方式：远程java程序配置                         
进行配置
`-Xdebug -Xrunjdwp:transport=dt_socket,address=9998,server=y,suspend=n`

先启动远程java程序 然后启动Eclipse远程调试。
缺点 ：只有java程序启动后才能进行调试，无法调试java程序的启动过程，如果要全程调试需要使用下面的方式                         
优点 ：可以随时连接到远程java程序进行调试，没尝试过多机同时调试一个远程java                    
                                             
Standard(Socket Listen)方式调试：                     
配置                             
window系统在(控制面板->其他程序->java)打开"Java Control Panel"配置对话框                   
在打开远程java主机的"Java Control Panel"配置对话框 进行配置                    
`-agentlib:jdwp=transport=dt_socket,suspend=y,address=192.168.228.7:9999`                
address:Eclipse程序所在的主机的IP和调试端口                
                   
先启动Eclipse远程调试，然后启动远程java程序 现在可以正常调试系统了                      
缺点 ：不能随时连接到远程java程序进行调试                           
优点 ：可以调试java程序启动过程      

##### 添加JDK源码
> (**过时**)attach source  C:\Program Files\Java\jdk1.8.0_191\src.zip

##### JDK学习笔记 
> com.lzyh.thread包下为多线程实例   

##### 五分钟读懂UML类图    
>  [查看cnblogs链接](https://www.cnblogs.com/shindo/p/5579191.html)


##### 以下是常见的汉字字符集编码：  
GB2312编码：1981年5月1日发布的简体中文汉字编码国家标准。GB2312对汉字采用双字节编码，收录7445个图形字符，其中包括6763个汉字。     

BIG5编码：台湾地区繁体中文标准字符集，采用双字节编码，共收录13053个中文字，1984年实施。    

GBK编码：1995年12月发布的汉字编码国家标准，是对GB2312编码的扩充，对汉字采用双字节编码。GBK字符集共收录21003个汉字，包含国家标准GB13000-1中的全部中日韩汉字，和BIG5编码中的所有汉字。    

GB18030编码：2000年3月17日发布的汉字编码国家标准，是对GBK编码的扩充，覆盖中文、日文、朝鲜语和中国少数民族文字，其中收录27484个汉字。GB18030字符集采用单字节、双字节和四字节三种方式对字符编码。兼容GBK和GB2312字符集。    

Unicode编码：国际标准字符集，它将世界各种语言的每个字符定义一个唯一的编码，以满足跨语言、跨平台的文本信息转换。    



##### 基本类型  
byte/8  
char/16  
short/16  
int/32  
float/32  
long/64  
double/64  
boolean/~  
boolean 只有两个值： true、 false，可以使⽤ 1 bit 来存储，但是具体⼤⼩没有明确规定。 JVM 会在编
译时期将 boolean 类型的数据转换为 int，使⽤ 1 来表示 true， 0 表示 false。 JVM ⽀持 boolean 数组，
但是是通过读写 byte 数组来实现的















