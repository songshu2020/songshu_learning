# MYBATIS各类学习笔记


## 自动生成mybatis代码
详见 下面的内容，另在spring507中（mybatis）也有生成案例   
实现mybatis接口org.mybatis.generator.api.CommentGenerator可以自定义生成代码的注释   

  

1.使用mybatis生成实体类代码
	

```properties
1.1配置generatorConfig.xml文件，配置比较简单，看文件即可了解。

1.2右键点击"pom.xml"->"Run as"->"Run Configuration"

	1.2.1 Base directory,点击Browse Workspace,选择本工程，值为：${workspace_loc:/bioauth-domain-3.0.1}

	1.2.2 Goals : mybatis-generator:generate 

	1.2.3 配置完成之后点击，run

	1.2.4 刷新工程后可见生成的代码
```


数据库连接配置的 connectionURL 不能直接使用&号，需改为：&amp;	

generateConfig.xml配置文件中
targetRuntime属性有：MyBatis3(默认值)，MyBatis3Simple, Ibatis2Java2, Ibatis2Java5，用于指定生成的代码的运行时环境targetRuntime="MyBatis3Simple"
	
2.提交代码时如发生如下错误的解决。
	

```properties
2.1错误内容：
git.exe push --progress  "origin" master:master

Counting objects: 107, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (51/51), done.
Writing objects: 100% (72/72), 20.30 KiB | 0 bytes/s, done.
Total 72 (delta 21), reused 0 (delta 0)
remote: GitLab: You are not allowed to push code to protected branches on this project.
To http://172.16.8.162/bioauth/bioauth-upgrade.git
! [remote rejected] master -> master (pre-receive hook declined)
error: failed to push some refs to 'http://172.16.8.162/bioauth/bioauth-upgrade.git'
git did not exit cleanly (exit code 1) (1155 ms @ 2016/9/26 16:07:11)

```

```properties
2.2错误解决

在提交文件夹中右键，"TortosieGit" -> "Settings" -> "Git" -> "Edit local .git/config"
在文本中修改，相应分支，修改前
	[branch "master"]
	remote = origin
	merge = refs/heads/master
修改后
	[branch "master"]
	remote = origin master
	merge = refs/heads/master
```

