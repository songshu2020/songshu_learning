package com.lzyh.designpatterns.proxy.dynamic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Client {

	private static Logger logger = LogManager.getLogger(Client.class);
	
	public static void main(String[] args) {
		//创建目标对象
		ITeacherDao target = new TeacherDao();
		
		//给目标对象，创建代理对象, 可以转成 ITeacherDao
		ITeacherDao proxyInstance = (ITeacherDao)new ProxyFactory(target).getProxyInstance();
	
//		 proxyInstance=class com.sun.proxy.$Proxy0 内存中动态生成了代理对象
		logger.info("proxyInstance=" + proxyInstance.getClass());
		
		//通过代理对象，调用目标对象的方法
		proxyInstance.teach();
		
		proxyInstance.sayHello(" tom ");
		
		
		
		//可以对比借鉴com.atguigu.spring5.demo4.JDKProxy.java的jdk代理方式
	}

}
