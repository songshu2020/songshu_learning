package com.lzyh.designpatterns.observer;

import java.util.Observable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class ObserverDemo extends Observable {
	private static final Logger logger = LogManager.getLogger(ObserverDemo.class);

    public static void main(String[] args) {
        ObserverDemo observer = new ObserverDemo();
        //添加观察者
        observer.addObserver((o,arg)->{
        	logger.info("发生变化-->>" + o.countObservers());
        });//lambda表达式
        observer.addObserver((o,arg)->{
        	logger.info("手动被观察者通知，准备改变-->>" + o);
        });
        
        
        
        observer.setChanged(); //数据变化
        observer.notifyObservers(); //通知
        int countObservers = observer.countObservers();
    }
    
    @Test
    public void testObserver() {
    	ObserverDemo observer = new ObserverDemo();
    	observer.addObserver(new ObserverImpl2());
    	observer.addObserver(new ObserverImpl1());
    	
    	observer.setChanged();
    	observer.notifyObservers(new String[] {"haha","哈哈","呵呵"});
    	
    }
}
