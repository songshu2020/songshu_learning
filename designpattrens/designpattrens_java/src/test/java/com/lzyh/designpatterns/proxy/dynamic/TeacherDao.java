package com.lzyh.designpatterns.proxy.dynamic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TeacherDao implements ITeacherDao {

	private Logger logger = LogManager.getLogger(this.getClass());
	
	@Override
	public void teach() {
		logger.info(" ��ʦ�ڿ���.... ");
	}

	@Override
	public void sayHello(String name) {
		logger.info("hello " + name);
	}
	
}
