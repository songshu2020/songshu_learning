package com.lzyh.designpatterns.proxy.cglib;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TeacherDao {

	private Logger logger = LogManager.getLogger(this.getClass());
	
	public String teach() {
		logger.info(" 老师授课中  ， 我是cglib代理，不需要实现接口 ");
		return "hello";
	}
	
	public String myMethod(String param) {
		logger.info(" myMethod  ， 我是cglib代理，不需要实现接口 ");
		return "hello  " + param;
	}
}
