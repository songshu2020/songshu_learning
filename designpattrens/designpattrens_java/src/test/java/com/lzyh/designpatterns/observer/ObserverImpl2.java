package com.lzyh.designpatterns.observer;

import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ObserverImpl2 implements Observer {

	private static final Logger logger = LogManager.getLogger(ObserverImpl2.class);

	
	@Override
	public void update(Observable o, Object arg) {
		logger.info("22发生变化o-->>" + o);
		logger.info("22发生变化arg-->>" + Arrays.deepToString((Object[]) arg));

	}

}
