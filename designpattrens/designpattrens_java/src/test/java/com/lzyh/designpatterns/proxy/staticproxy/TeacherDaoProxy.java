package com.lzyh.designpatterns.proxy.staticproxy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//代理对象,静态代理
public class TeacherDaoProxy implements ITeacherDao{
	private static final Logger logger = LogManager.getLogger(TeacherDaoProxy.class);

	private ITeacherDao target; // 目标对象，通过接口来聚合
	
	
	//构造器
	public TeacherDaoProxy(ITeacherDao target) {
		this.target = target;
	}



	@Override
	public void teach() {
		logger.info("开始静态代理，  完成某些操作。。。。。 ");//方法
		target.teach();
		logger.info("静态代理，提交。。。。。");//方法
	}

}
