# linux下安装oracle、weblogic及其卸载

linux下安装oralce数据库、weblogic及其卸载，开启FTP、telnet服务详见:

《[oracle-weblogic安装卸载过程.md](../oracle-weblogic安装卸载过程/oracle-weblogic安装卸载过程.md)》，同时在【Middleware/weblogic/weblogic 安装配置调优.doc】中也有weblogic的相关介绍。

# vi命令的用法-AIX相关操作

vi命令的用法-AIX相关操作，详见：

《[vi命令的用法-AIX相关操作](../vi命令的用法-AIX相关操作.md)》

# 装Linux分区（生产时）：

```
Swap：分为内存的两倍
/boot：分为200Mb以上（不放心分512MB也行）
SuSE11.2的boot分区为Vfat格式，/boot/efi
/	：剩下的全部分到根（/）

再装系统分区时选择（lvm  逻辑卷）  装完后可动态调整大小
```



## 查看有多少颗cpu（物理）

```bash
#cat /proc/cpuinfo |grep "physical id" |sort |uniq |wc –l
```



## 查看Linux版本信息

```bash
$ cat /etc/SuSE-release
$ lsb_release –a
```



# 命令tcpdump学习

```bash
tcpdump -X -s0 -i eth0 -w test.cap
```

# linux下添加用户-useradd

```bash
useradd  -m（文件夹）  -g   
例如：  #useradd  -m  -g  WebLogic   weblogic（新用户名称）
		#passwd
		#输入新用户密码
		#usermod  -m  -g  weblogic   weblogic
```



# 列出目录 - ls 命令

```bash
# 列出目录：
ls - | grep ‘^p’ 
ls -ld */
ls -lp |grep"/"
ls -l | grep '^d'
ls -F|grep '/$'
```



# sed命令用法例子

## 文件每一行开始加入“|”

```bash
app@linux-hsj5:~/test> sed 's/^/|/g' zkltest.dat > tmp.log
```



## 文件每一行结尾加入“|”

```bash
app@linux-hsj5:~/test> sed 's/$/|/g' zkltest.dat > tmp.dat

第二种方法：
sed -i 's/^M//g' filename
#注意：^M的输入方式是 Ctrl + v ，然后Ctrl + M
```

## linux下打印指定那几行

```bash
$sed -n  '5142,2p'  CCMobile.log.1
```

## sed举例：

（假设我们有一文件名为ab）

```bash
# 删除某行
[root@localhost ruby] # sed '1d' ab              #删除第一行 
[root@localhost ruby] # sed '$d' ab              #删除最后一行
[root@localhost ruby] # sed '1,2d' ab           #删除第一行到第二行
[root@localhost ruby] # sed '2,$d' ab           #删除第二行到最后一行
# 显示某行
[root@localhost ruby] # sed -n '1p' ab           #显示第一行 
[root@localhost ruby] # sed -n '$p' ab           #显示最后一行
[root@localhost ruby] # sed -n '1,2p' ab        #显示第一行到第二行
[root@localhost ruby] # sed -n '2,$p' ab        #显示第二行到最后一行
# 使用模式进行查询
[root@localhost ruby] # sed -n '/ruby/p' ab    #查询包括关键字ruby所在所有行
[root@localhost ruby] # sed -n '/\$/p' ab       #查询包括关键字$所在所有行，使用反斜线\屏蔽特殊含义
# 增加一行或多行字符串
 [root@localhost ruby]# cat ab
 Hello!
 ruby is me,welcome to my blog.
 end
[root@localhost ruby] # sed '1a drink tea' ab  #第一行后增加字符串"drink tea"
 Hello!
 drink tea
 ruby is me,welcome to my blog. 
 end
 [root@localhost ruby] # sed '1,3a drink tea' ab #第一行到第三行后增加字符串"drink tea"
 Hello!
 drink tea
 ruby is me,welcome to my blog.
 drink tea
 end
 drink tea
 [root@localhost ruby] # sed '1a drink tea\nor coffee' ab   #第一行后增加多行，使用换行符\n
 Hello!
 drink tea
 or coffee
 ruby is me,welcome to my blog.
 end
代替一行或多行
 [root@localhost ruby] # sed '1c Hi' ab                #第一行代替为Hi
 Hi
 ruby is me,welcome to my blog.
 end
 [root@localhost ruby] # sed '1,2c Hi' ab             #第一行到第二行代替为Hi
 Hi
 end
# 替换一行中的某部分
格式：sed 's/要替换的字符串/新的字符串/g'   （要替换的字符串可以用正则表达式）
 [root@localhost ruby] # sed -n '/ruby/p' ab | sed 's/ruby/bird/g'    #替换ruby为bird
[root@localhost ruby] # sed -n '/ruby/p' ab | sed 's/ruby//g'        #删除ruby
# 插入
 [root@localhost ruby] # sed -i '$a bye' ab         #在文件ab中最后一行直接输入"bye"
 [root@localhost ruby]# cat ab
 Hello!
 ruby is me,welcome to my blog.
 end
 bye
 # 删除匹配行
 sed -i '/匹配字符串/d'  filename  （注：若匹配字符串是变量，则需要“”，而不是‘’。记得好像是）
 # 替换匹配行中的某个字符串
 sed -i '/匹配字符串/s/替换源字符串/替换目标字符串/g' filename
```



# Linux下压缩命令的使用

## tar：

```bash
压缩：tar –cvf   *.tar    *.jpg
解压缩：tar  -xvf  *.tar
-c:建立压缩档案
-x:解压
-t:查看内容
-r:向压缩归档末尾追加文件
-u:更新原压缩包中的文件

>1、排除某些文件压缩 
>$cat exclude_files   
>zhouk   
>lib32.tar   
>$tar -cvf /tmp/aaa.tar -X exclude_files

>2、压缩某些文件
>$cat fl_list    
>zhouk   
>lib32.tar   
>$tar -cvf /tmp/aaa.tar -L fl_list  
```



# 命令curl

## curl命令post请求

```bash
curl -X POST -F pwd=mypasswordis123 -F aaa=@messages.zip http://172.16.51.16:8080/zklshangchuan/fc/upload
```

# 常用FTP命令

总结一下常用的FTP命令：

```
1. open：与服务器相连接；
2. send(put)：上传文件；
3. get：下载文件；
4. mget：下载多个文件；
5. cd：切换目录；
6. dir：查看当前目录下的文件；  !dir：查看本机目录下文件
7. del：删除文件；
8. bye：中断与服务器的连接。
9. bin
   /*采用二进制传输。如果你要上传下载，这一步很重要，不先执行这个命令，上传下载会很慢。*/

如果想了解更多，可以键入
ftp> help （回车）
查看命令集：
ascii: 设定以ASCII方式传送文件(缺省值)
bell: 每完成一次文件传送,报警提示
binary: 设定以二进制方式传送文件
bye: 终止主机FTP进程,并退出FTP管理方式
case: 当为ON时,用MGET命令拷贝的文件名到本地机器中,全部转换为小写字母
cd: 同UNIX的CD命令
cdup: 返回上一级目录
chmod: 改变远端主机的文件权限
close: 终止远端的FTP进程,返回到FTP命令状态,所有的宏定义都被删除
delete: 删除远端主机中的文件
dir [remote-directory] [local-file]: 列出当前远端主机目录中的文件.如果有本地文件,就将结果写至本地文件
get [remote-file] [local-file]: 从远端主机中传送至本地主机中
help [command]: 输出命令的解释
lcd: 改变当前本地主机的工作目录,如果缺省,就转到当前用户的HOME目录
ls [remote-directory] [local-file]: 同DIR
macdef: 定义宏命令
mdelete [remote-files]: 删除一批文件
mget [remote-files]: 从远端主机接收一批文件至本地主机
mkdir directory-name: 在远端主机中建立目录
mput local-files: 将本地主机中一批文件传送至远端主机
open host [port]: 重新建立一个新的连接
prompt: 交互提示模式
put local-file [remote-file]: 将本地一个文件传送至远端主机中
pwd: 列出当前远端主机目录
quit: 同BYE
recv remote-file [local-file]: 同GET
rename [from] [to]: 改变远端主机中的文件名
rmdir directory-name: 删除远端主机中的目录
send local-file [remote-file]: 同PUT
status: 显示当前FTP的状态
system: 显示远端主机系统类型
```

# 断开某个用户的终端连接

```bash
who查看pts/X
#fuser  -k   /dev/pts/X
```

# NTP手动更新时间 ntpdate

```bash
ntpdate -u [ntpserver]
```



# Linux 下进入带减号-的目录（单引号等） cd

方法一：

```bash
cd -- -23232tmp   (中间两个减号)
```

方法二：

1. **找到目录的inode号**：使用`ls -i`命令找到目录的inode号。

2. **使用find命令删除**：通过inode号来删除目录。

   ```bash
   inode_number=$(ls -i | grep "目录的部分名称" | awk '{print$1}')
   find . -inum $inode_number -exec rm -r {} \;
   ```

   

# 修改硬件时间

hwclock --set --date='03/30/15  19:25'

# 命令iptables 命令学习

SUSE下
iptables -A OUTPUT -p icmp -j DROP
禁止本机向外ping任何IP
可写为shell去动态执行，如
iptables -F
iptables -X
iptables -A OUTPUT -d 98.10.2.227 -p tcp --dport  1521  -j  DROP

# lsof命令学习

```bash
ls  open  files 列出那些进程正在读取文件
例子：lsof | grep oracle | grep FIFO
查看端口是被那个程序占用的
lsof –i:30052
lsof -i tcp:5500
#列出被删除的文件但还被进程占用没有真正释放空间的文件
lsof | grep -i delete
```



# shell中计算字符串的长度

```bash
方法1、echo "abc"| wc -L ，使用wc –L 命令可以获取当前行的长度，
方法2、expr length "abc"
方法3、通过awk + length的方式   echo "zhou" | awk '{print length($0)}'
方法4、通过echo  ${#string} 的方式获取 ，例子：
name = zhoukai
echo ${#name}
```

# 各种命令使用指南

## mount挂载iso文件

```bash
mount -o loop redhat.iso  /dev/cdrom/
# 加挂window共享的NFS文件夹
mount -t nfs -o rw,timeo=2,soft ip:/fileserver /mnt/zkl
# 解挂挂载的路径
umount 98.10.2.137:/fileserver 或 umount /mnt/zkl
# 查看挂载信息
mount
```

## 命令-iostat

查看linux磁盘io

```bash
iostat -d -k 2
参数 -d 表示，显示设备（磁盘）使用状态；-k某些使用block为单位的列强制使用Kilobytes为单位；2表示，数据显示每隔2秒刷新一次。
输出信息的意义：
tps：该设备每秒的传输次数（Indicate the number of transfers per second that were issued to the device.）。"一次传输"意思是"一次I/O请求"。多个逻辑请求可能会被合并为"一次I/O请求"。"一次传输"请求的大小是未知的。

kB_read/s：每秒从设备（drive expressed）读取的数据量；
kB_wrtn/s：每秒向设备（drive expressed）写入的数据量；
kB_read：读取的总数据量；
kB_wrtn：写入的总数量数据量；这些单位都为Kilobytes。
-x 参数
# iostat还有一个比较常用的选项-x，该选项将用于显示和io相关的扩展数据。
```

## 命令-top

命令格式：
top [-] [d] [p] [q] [c] [C] [S]    [n]

```properties
参数说明：
d：  指定每两次屏幕信息刷新之间的时间间隔。当然用户可以使用s交互命令来改变之。
p：  通过指定监控进程ID来仅仅监控某个进程的状态。
q：该选项将使top没有任何延迟的进行刷新。如果调用程序有超级用户权限，那么top将以尽可能高的优先级运行。
S： 指定累计模式
s ： 使top命令在安全模式中运行。这将去除交互命令所带来的潜在危险。
i：  使top不显示任何闲置或者僵死进程。
c：  显示整个命令行而不只是显示命令名
```

在top命令的显示窗口，我们还可以输入以下字母，进行一些交互：
帮助文档如下：

```bash
Help for Interactive Commands - procps version 3.2.7
Window 1:Def: Cumulative mode Off.  System: Delay 4.0 secs; Secure mode Off.
  Z,B       Global: 'Z' change color mappings; 'B' disable/enable bold
  l,t,m     Toggle Summaries: 'l' load avg; 't' task/cpu stats; 'm' mem info
  1,I       Toggle SMP view: '1' single/separate states; 'I' Irix/Solaris mode
  f,o     . Fields/Columns: 'f' add or remove; 'o' change display order
  F or O  . Select sort field
  <,>     . Move sort field: '<' next col left; '>' next col right
  R,H     . Toggle: 'R' normal/reverse sort; 'H' show threads
  c,i,S   . Toggle: 'c' cmd name/line; 'i' idle tasks; 'S' cumulative time
  x,y     . Toggle highlights: 'x' sort field; 'y' running tasks
  z,b     . Toggle: 'z' color/mono; 'b' bold/reverse (only if 'x' or 'y')
  u       . Show specific user only
  n or #  . Set maximum tasks displayed
  k,r       Manipulate tasks: 'k' kill; 'r' renice
  d or s    Set update interval
  W         Write configuration file
  q         Quit
          ( commands shown with '.' require a visible task display window )
Press 'h' or '?' for help with Windows,
h或者?  : 显示帮助画面，给出一些简短的命令总结说明。
k  ：终止一个进程。系统将提示用户输入需要终止的进程PID，以及需要发送给该进程什么样的信号。一般的终止进程可以使用15信号；如果不能正常结束那就使用信号9强制结束该进程。默认值是信号15。在安全模式中此命令被屏蔽。
i：忽略闲置和僵死进程。这是一个开关式命令。
q：  退出程序。
r：  重新安排一个进程的优先级别。系统提示用户输入需要改变的进程PID以及需要设置的进程优先级值。输入一个正值将使优先级降低，反之则可以使该进程拥有更高的优先权。默认值是10。
S：切换到累计模式。
s :  改变两次刷新之间的延迟时间。系统将提示用户输入新的时间，单位为s。如果有小数，就换算成ms。输入0值则系统将不断刷新，默认值是5 s。需要注意的是如果设置太小的时间，很可能会引起不断刷新，从而根本来不及看清显示的情况，而且系统负载也会大大增加。
f或者F :从当前显示中添加或者删除项目。
o或者O  :改变显示项目的顺序。
l: 切换显示平均负载和启动时间信息。即显示影藏第一行
m： 切换显示内存信息。即显示影藏内存行
t ： 切换显示进程和CPU状态信息。即显示影藏CPU行
c：  切换显示命令名称和完整命令行。 显示完整的命令。 这个功能很有用。
M ： 根据驻留内存大小进行排序。
P：根据CPU使用百分比大小进行排序。
T： 根据时间/累计时间进行排序。
W：  将当前设置写入~/.toprc文件中。这是写top配置文件的推荐方法。
```



# linux设置日期、时间、修改默认用户主目录

```bash
root用户登录后执行  date  -s 20141121  

设置日期， date  –s  10:10:10设置时间
修改默认用户主目录  

方法一：root 用户登陆后编辑 /etc/passwd文件	
方法二：命令usermod，
					usermod –d /usr/newfolder –u  uid
```



# 裸硬盘、逻辑卷，卷组学习

```bash
#fdisk –l
多路径聚合后
#multipath –ll
physical  volume   --------    group volume （池子）的对应关系用命令“pvs”查看
#pvs
卷组（VG）和逻辑卷（LV）之间的关系用命令“vgs”查看
#vgs
用命令“lvs”查看各个逻辑卷
#lvs
用命令“vgdisplay”查看卷组详细信息
#vgdisplay
```

# 索引linux文件存储索引学习 -df

```
查看每个inode节点的大小：dumpe2fs -h /dev/sda1
df -i /dev/sda1
索引被沾满会出现磁盘空间还足够但文件写不进去的现象
```

# shell脚本跨机器不执行的解决办法-source

```
查看脚本中#！后的东西
用source  [file-name] 后再执行
```

# Linux下安装jdk1.6

```bash
./jdk1.6*****************出现图形界面安装
配置环境变量：vi  /etc/profile    完成后用命令java  与javac测试
###################  JAVA PATH #######################


export JAVA_HOME=/root/jrockit-jdk1.6.0_45-R28.2.7-4.1.0
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar

执行生效：
# source   /etc/profile

用下面测试用例测试是否安装成功jdk
class Test {

        public static void main(String args[]) {
                System.out.println("Hello world!");
        }
}
```

# Ubuntu学习笔记

```
普通用户登陆进去后输入命令：sudo passwd root，为root用户设置密码，然后就可以切换到root用户了。
修改文件/etc/X11/default-display-manager可以改变启动界面：false为字符界面，/usr/sbin/gdm为图形界面；若在字符界面下输入命令：startx 可切换到图形界面
```



# 虚拟机

## 在虚拟机中安装VMware_tools

点击“安装VMware_tools”，桌面上出现光盘图标时，打开它，右键“解压缩到”/opt，在/opt下找到VMware_install.pl，运行它，一路yes。Ok完成。
*.tar.gz文件先用gunzip命令解压成*.tar文件，然后用tar命令解压





