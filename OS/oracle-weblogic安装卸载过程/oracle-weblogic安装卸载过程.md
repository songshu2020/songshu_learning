﻿# Linux下安装Oracle数据库

## **1.添加oracle用户，dba组**

groupadd dba

useradd –m –g dba oracle

(    groupadd oinstall

useradd  -m  -g  oinstall  -G  dba   oracle )    

## **2.gcc软件包是否安装**

gcc  --version

若没有安装进入yast2搜索gcc安装。

## **3.解压\*.zip文件用unzip命令。**

`   `更改文件所属用户chown -R oracle:dba ./database/

**解压完后当前目录下有个database目录，进去**

export  DISPLAY=98.10.231.28

用root用户执行xhost + 

```bash
linux-zkl:~ # export DISPALY=0:0
linux-zkl:~ # export DISPALY=98.10.231.28 0:0
-bash: export: `0:0': not a valid identifier
linux-zkl:~ # export DISPALY=98.10.65.43 
linux-zkl:~ # export DISPALY=0:0
linux-zkl:~ # xhost   +
access control disabled, clients can connect from any host
```

**可用命令** **xclock**  **来检查是否可以输出到本机**

env | grep DIS

./runInstaller      **必须通过oracle用户登录来执行安装。**

|![](学习随笔.001.png)|
| :- |


|![](学习随笔.002.png)|
| :- |

## **5.配置监听及本地网络服务**

1）oracle用户netca

```bash
oracle@linux-zkl:/opt/oracle/product/11gR1/db> netca

Oracle Net Services 配置:
正在配置监听程序:IPCC
监听程序配置完成。
Oracle Net 监听程序启动:
    正在运行监听程序控制: 
      /opt/oracle/product/11gR1/db/bin/lsnrctl start IPCC
    监听程序控制完成。
    监听程序已成功启动。
成功完成 Oracle Net Services 配置。退出代码是0
oracle@linux-zkl:/opt/oracle/product/11gR1/db>
```

## **6.创建用户**

```bash
create user icd identified by icd default tablespace users Temporary TABLESPACE Temp;
```

**东哥创建用户总结**

## 4，oracle创建用户、授权。

默认的：最高权限用户：system  密码：manager  tiger           

管理员权限用户：sys     密码：change\_on\_install           

普通用户：scott           密码：tiger   

登陆管理员或超级管理员用户可以自己建立属于自己的用户：

`   `命令：create user userName identified by password;

`   `创建用户名为：userName, 密码为 password 的用户分配权限：

grant dba to userName;--授予DBA权限

grant unlimited tablespace to userName;--授予不限制的表空间

grant select any table to userName;--授予查询任何表

grant select any dictionary to userName;--授予 查询 任何字典

## **7.重启服务器后启动监听**

lsnrctl stop

lsnrctl start IPCC

lsnrctl start

## **8.若一个服务器上有两个实例**

若一个服务器上有两个实例，需分两次启动实例，然后启动监听。

1、第一次用sqlplus登陆进去startup，启动的是默认实例

2、启动完之后，export  ORACLE\_SID=第二个实例名

3、再次用sqlplus登陆进去startup

查看数据库的默认实例是什么：

select  instance\_ name  from  v$instance;

Linux下解压war包：jar –xvf \*.war

## **配置数据库开机自启动**

将Oracle和监听的启动程序加入操作系统的自启动程序中，当操作系统重新启动时，Oracle数据库和监听将随操作系统一起启动。

操作步骤

```bash
1.以root用户登录Oracle服务器。

2.修改服务名的配置，修改完成后，按“ESC”键，执行命令：wq保存并退出。 

vi /etc/oratab

按“i”键使文件进入编辑状态。

该文件中含有一条或多条相同格式的条目，格式均为：ORACLE\_SID:$ORACLE\_HOME:<N|Y>，修改数据库服务名（ORACLE\_SID）的条目为“Y”，以ipcc为例。ipcc:/opt/oracle/product/11gR2/db:Y

3.进入“init.d”目录。 

cd /etc/init.d

4.新建“start\_oracle”文件，按“i”键，使文件进入编辑状态。在该文件中添加以下内容，其中/opt/oracle/product/11gR2/db为oracle的实际安装路径，按“ESC”键，执行命令：wq保存并退出。 

vi start\_oracle

#!/bin/bash

\### BEGIN INIT INFO

\# Provides:          oracle automatic start

\# Required-Start:    network

\# Required-Stop:     

\# Default-Start:     3 5

\# Default-Stop:      3 5

\# Short-Description: oracle automatic start

\# Description: oracle automatic start

\### END INIT INFO

ORACLE\_HOME=/opt/oracle/product/11gR2/db（根据实际的环境环境填写安装路径）

export ORACLE\_HOME

PATH=$ORACLE\_HOME/bin:$PATH; export PATH

case $1 in

'start')

`    `su - oracle -c "$ORACLE\_HOME/bin/lsnrctl start"

`    `su - oracle -c "$ORACLE\_HOME/bin/dbstart"

`    `;;

'stop')

`    `su - oracle -c "$ORACLE\_HOME/bin/lsnrctl stop"

`    `su - oracle -c "$ORACLE\_HOME/bin/dbshut"

`    `;;

\*)

`    `echo "usage: $0 {start|stop}"

`    `exit

`    `;;

esac

exit

修改文件的属组。 

chown oracle:dba /etc/init.d/start\_oracle

修改文件的权限。 

chmod 755 start\_oracle
```

7.设置在Linux启动level 3/5时，自动启动oracle。 

chkconfig --add start\_oracle

chkconfig --level 12345 start\_oracle off

chkconfig --level 35 start\_oracle on

说明：标准的Linux运行级别为3或者5，Level3表示启动后进入命令行模式，Level5表示启动后进入图形模式

以下为启动lzetp的脚本：

```bash
xtjk@V_MDC_PROD_VTM_SP2_APP_01:/etc/init.d> cat start_lzetp
#!/bin/bash
### BEGIN INIT INFO
# Provides:          Service LZETP automatic start
# Required-Start:    network
# Required-Stop:     
# Default-Start:     3 5
# Default-Stop:      3 5
# Short-Description: Service LZETP automatic start
# Description: Service LZETP automatic start
### END INIT INFO
#
CUR_TIME=$(date "+%Y-%m-%d %H:%M:%S")
LOG_FILE=/home/autostart_lzetp.log
export JAVA_HOME=/weblogic/jdk1.6.0_191
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
case $1 in
'start')
    echo "[${CUR_TIME}][app][INFO]|LZETPServer is starting."  >> ${LOG_FILE}
    su - app -c "/home/VTM/startLZETPServerImmediately"
    echo "[${CUR_TIME}][app][INFO]|Start LZETPServer success."  >> ${LOG_FILE}
    chmod 644 ${LOG_FILE}
    ;;
'stop')
    echo "[${CUR_TIME}][app][INFO]|LZETPServer is stoping."  >> ${LOG_FILE}
    su - app -c "/home/VTM/shutdownLZETPServerImmediately"
    echo "[${CUR_TIME}][app][INFO]|Stop LZETPServer success."  >> ${LOG_FILE}
    ;;
*)
    echo "usage: $0 {start|stop}"
    exit
    ;;
esac
exit
```

**oracle客户端两个用户同时查一张表权限赋予**

1、sys用户登录SYSDBA

grant select on hurodb.tsmchanneldef to ydmw;

grant select, insert, update, delete on hurodb.THISSMSENDREC to YDMW;

此时就可以用hurodb. THISSMSENDREC去查数据

2、ydmw用户登录，创建同义词（synonyms）

`   `create or replace synonym THISSMSENDREC

`    `for HURODB.THISSMSENDREC;

**配置NFS服务端与客户端**

**服务端**：华为文档搜索“开启NFS服务”

|![](学习随笔.003.png)|
| - |
**在路径cat /var/lib/nfs/etab下有server端的详细配置**

**客户端：**

在客户端执行命令：mount 10.0.0.3:/share/ /share/

解挂umount 执行报错 device is busy 时执行：fuser -km /home/weblogic/app/application/即可解决

要想每次启动机器的时候自动挂载，可使用命令 vi /etc/fstab 编辑，在最后面加上 192.168.109.130:/var/ftp  /nfs/ftp  nfs  defaults 0 0 ，保存退出。这样在每次启动的时候就会自动挂载 192.168.109.130:/var/ftp 这个NFS 共享了。

**创建RAID**

打开电源出现按“CTRL+H”时按下“CTRL+H”，进入WebBIOS，选择Configuration  wizard，选择自动安装，自动安装时三块硬盘为RAID5

**Linux下oracle在浏览器中配置**

浏览器地址栏输入：  https://127.0.0.1:1158/em  或  https://主机名.site:1158/em

Linux启动监听及服务

```bash
Oracle用户登陆，sqlplus / as sysdba   ，执行命令startup；
exit

执行lsnrctl start
startup 报错的处理办法

cp /opt/oracle/admin/hurodb/pfile/init.ora.11172014151347 /opt/oracle/product/11gR1/db/dbs/initorcl.ora

SQL> shutdown abort;

SQL> conn /as sysdba;
Connected.
SQL>
```

# linux下安装WebLogic

```bash
groupadd weblogic
useradd –m –g weblogic weblogic
passwd weblogic
chmod   a+x   WebLogic****
java -jar wls1036\_generic.jar
```

图形界面完成安装后配置域   (选默认)

![](学习随笔.004.png)


![](学习随笔.005.png)

**（总共5个选项的情况下选第一个和第三个）**

可手工配置域

/home/weblogic/Oracle/Middleware/wlserver\_10.3/common/bin

./config.sh

**配置数据源：**

选择“服务”——JDBC——数据源

**启动服务器：**

**# /root/Oracle/Middleware/user\_projects/domains/VTM/bin**

**# ./srartWebLogic.sh&**

**#nohup ./startWebLogic.sh & tail -f nohup.out**

**（以服务形式启动，不挂在当前shell进程下 nohup ./startWebLogic.sh &）**

完成后在IE地址栏中输入    <http://ip1:7001/console>  进入配置界面（输入名webLogic和密码weblogic123）

**启动weblogic不成功时检查是否个root用户启动过，解决办法：**

**一、#chown -R  weblogic.weblogic /home/weblogic/Oracle/**

**二、$cd  $weblogic\_home/user\_projects/domains/VTM**

**此目录下创建文件boot.properties内容：**

username=weblogic

password=weblogic123

**三、$cd $weblogic\_home/** **user\_projects/domains/VTM/servers/AdminServer创建security目录，在此目录下创建文件boot.properties内容：**

username=weblogic

password=weblogic123

`     `**更改后首次启动特别慢，请耐心等待**

**启动weblogic出现日志路径（permission  denied）解决办法**

**# chmod -Rf 1777 /home/weblogic/Oracle/**

**查看weblogic服务是否开启：**

命令：   ps  -ef | grep java   查看java进程，weblogic的每一个服务器就是一个Java进程

**weblogic新建服务器**

控制台新建服务器，在 $WEBLOGIC\_HOME/servers/新建服务器NAME下创建security，下面创建boot.properties ，将用户名密码改为明文，重新启动

**linux下安装WebLogic后调出QuickStart**

cd   /root/Oracle/Middleware/wlserver\_10.3/common/bin

./config.sh

linux下卸载WebLogic

cd   /root/Oracle/Middleware/wlserver\_10.3/uninstall

./uninstall.sh

rm   -f   \*\*

weblogic控制台启动监视仪表盘

http://ip:7001/console/dashboard

Linux下开启FTP服务：

华为文档搜索“才”，选择“开启FTP服务”。

Suse-Linux下开启telnet服务

华为文档搜索“开启Telnet服务”。

**查看某个软件是否安装:    rpm -qa | grep telnet**

