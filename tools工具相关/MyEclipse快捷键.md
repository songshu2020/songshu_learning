# Ctrl+**  

Ctrl+1     快速修复  
Ctrl+D     删除当前行  
Ctrl+Q     定位到最后编辑的地方  
Ctrl+L      定位到某行  
Ctrl+O      快速显示Outline  
Ctrl+T       快速显示当前类的继承结构  
Ctrl+W      关闭当前的editor  
Ctrl+K       快速定位到下一个  
Ctrl+E      快速显示当前Editor的下拉列表  
Ctrl+J       正向增量查找  
Ctrl+Z      返回到修改前的状态  
Ctrl+Y      与上面操作相反  
Ctrl+/        注释当前行，再按则取消注释  
Ctrl+M      切换窗口的大小  

#  Ctrl+Shift+**  

Ctrl+Shift+E     显示管理当前所有打开的View管理器    
Ctrl+Shift+/       自动注释代码        JSP中也可以  
Ctrl+Shift+\       取消已经注释的代码  
Ctrl+Shift+F4    关闭所有打开的Editor  
Ctrl+Shift+X      把当前选中的文本全部变为大写  
Ctrl+Shift+Y      把当前选中的文本全部变为小写  
Ctrl+Shift+F       格式化当前代码  

# F3-F9  

F3   跳到声明或定义的地方  
F5   单步调试进入函数内部  
F6   单步调试不进入断点  
F7   由函数内部返回到调用处  
F8   一直执行到下一个断点    

#######################  java 中将Map中的值取出的方法  ##########################  
方法一：  
	

```java
Collection cc = resultmap.values();
Iterator it = cc.iterator();
for(;it.hasNext();)
{
    System.out.println( "" + it.next());
}
```

方法二
	

```java
Set<String> key = resultmap.keySet();
for(Iterator it = key.iterator();it.hasNext();)
{
    String mapkey = (String) it.next(); 
    System.out.println("\n" + mapkey + " = " + resultmap.get(mapkey));
}
```

###################################################################



