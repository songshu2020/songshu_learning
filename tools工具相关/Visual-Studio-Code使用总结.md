# 好用插件

插件下载地址：

[Extensions for Visual Studio family of products | Visual Studio Marketplace](https://marketplace.visualstudio.com/)

## Cline

对接各大AI大模型实现自动编码、代码检查等功能

## Translation

中英文翻译，对接百度api

## Markmap-脑图

 markdown格式的脑图，可以导出为html、svg格式的文件。

通过google浏览器也可安装markmap。

## Markdown All in One

markdown格式文件



# 对接大模型编程

豆包大模型（国内的一款**多模态**AI大模型）**将设计图转换为文字描述**。豆包大模型通过视觉输入（如图像）和自然语言处理能力，将图像中的UI元素解析并转换成可供DeepSeek V3理解的文字描述，形成了完整的文字输入

## 对接deepseek

对接deepseek，api方式：

![](../pic/deepseek-ai-code.png)

## 对接通义灵码

下载插件”TONGYI Lingma“安装

登录阿里云账号即可使用

