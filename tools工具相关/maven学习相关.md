以下是本人学习maven笔记

# 一、安装本地jar包

```bash
mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.1.0 -Dpackaging=jar -Dfile=e:\ojdbc6-11.2.0.1.0.jar
mvn install:install-file -DgroupId=org.mybatis.generator -DartifactId=mybatis-generator-maven-plugin -Dversion=1.3.7 -Dpackaging=jar -Dfile=e:\mybatis-generator-maven-plugin-1.3.7.jar
mvn install:install-file -DgroupId=org.mybatis.generator -DartifactId=mybatis-generator-core -Dversion=1.3.7 -Dpackaging=jar -Dfile=e:\mybatis-generator-core-1.3.7.jar

mvn install:install-file -DgroupId=com.dcfs.client -DartifactId=ESBClientGM_JAVA -Dversion=1.0.0 -Dpackaging=jar -Dfile=E:\LZKF\maven-install-jars\ESBClientGM_JAVA.jar
mvn install:install-file -DgroupId=com.dcfs.client -DartifactId=security_tools -Dversion=1.0.0 -Dpackaging=jar -Dfile=E:\LZKF\maven-install-jars\security_tools.jar
```



# 二、清除本地已安装的jar包

```bash
//第一步,从Maven本地仓库删除jar
//清除某个jar
mvn dependency:purge-local-repository -DmanualInclude="groupId:artifactId"
mvn dependency:purge-local-repository -DmanualInclude="com.oracle:ojdbc6"

//清除多个属于不同groupId的jar
mvn dependency:purge-local-repository -DmanualInclude="groupId1:artifactId1,groupId2:artifactId2,..."
mvn dependency:purge-local-repository -DmanualInclude="com.oracle:ojdbc6"

//第二步,阻止Maven对已删除的jar进行reResolve
mvn dependency:purge-local-repository -DreResolve=false
```

