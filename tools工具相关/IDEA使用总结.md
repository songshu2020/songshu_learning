# 快捷键

以下是常用的 IntelliJ IDEA 快捷键（适用于 Windows/Linux，Mac 用户可以将 Ctrl 替换为 Cmd，Alt 替换为 Option）：

## 1.导航相关

双击 Shift：搜索任何内容（包括类、文件、符号等）

Ctrl + N：查找类

Ctrl + Shift + N：查找文件

Ctrl + Alt + Shift + N：查找符号

Alt + 左/右箭头：在打开的文件之间切换

Ctrl + E：最近打开的文件

Ctrl + Shift + E：最近编辑的文件

Ctrl + F12：文件结构弹窗

Ctrl + B / Ctrl + 点击：跳转到定义

Alt + F1：选择目标（例如在项目工具窗口中查看当前文件）



## 2.编辑相关

Ctrl + D：复制当前行或选中内容

Ctrl + Y：删除当前行 - 重做

Ctrl + X：剪切当前行或选中内容

Ctrl + Shift + V：从剪贴板历史中粘贴

Ctrl + /：单行注释

Ctrl + Shift + /：块注释

Ctrl + W：扩大选中范围

Ctrl + Shift + W：缩小选中范围

Alt + Shift + 上/下箭头：上下移动当前行

Ctrl + Alt + L：格式化代码

Ctrl + Alt + O：优化导入

Tab / Shift + Tab：缩进/反缩进



## 3.代码辅助

Ctrl + Space：代码补全

Ctrl + Shift + Space：智能代码补全

Ctrl + P：参数提示

Ctrl + Q：查看文档

Alt + Enter：快速修复提示

Ctrl + Shift + F10：运行当前文件

Ctrl + Alt + T：包围代码（例如 try-catch）

Ctrl + Shift + 上/下箭头：方法间跳转



## 4.查找和替换

Ctrl + F：在当前文件中查找

Ctrl + R：在当前文件中替换

Ctrl + Shift + F：全局查找

Ctrl + Shift + R：全局替换



## 5.调试

F8：跳过（Step Over）

F7：进入（Step Into）

Shift + F7：强制进入（Force Step Into）

Shift + F8：跳出（Step Out）

Alt + F9：运行到光标处

Alt + F8：计算表达式

Ctrl + F8：切换断点



## 6.版本控制

Ctrl + K：提交代码

Ctrl + T：从版本库更新代码

Alt +  (Grave)：版本控制弹出菜单



## 7.窗口管理

Ctrl + Shift + F12：隐藏/显示所有工具窗口

Alt + 1：项目窗口

Alt + 4：运行窗口

Alt + 9：版本控制窗口

# 好用插件

## MybatisX

将 XXXMapper.java 与 XXXMapper.xml 方法相互关联，点方法名就可以相互跳转

## Translation

中英文翻译，对接百度api

## RestfulTool

1 根据 URL 直接跳转到对应的方法定义 ( Ctrl \ or Ctrl Alt N );  
2 提供了一个 Services tree 的显示窗口;  
3 一个简单的 http 请求工具  
4 在请求方法上添加了有用功能: 复制接口URL、复制接口入参...  
5 其他功能: java 类上添加Convert to JSON功能，格式化 json 数据 ( Windows: Ctrl + Enter; Mac: Command + Enter )  
增强  
6 支持 Http Method 过滤  
7 支持依赖包中的接口也会检索出来  

## Alibaba Cloud Toolkit 

Alibaba Cloud Toolkit 可以帮助开发者更高效地部署、测试、开发和诊断应用。Cloud Toolkit与主流IDE及阿里云其他产品无缝集成，帮助您大大简化应用部署到服务器，尤其是阿里云服务器中的操作。您还可以通过其内嵌的**Arthas**程序诊断、Terminal Shell终端和MySQL执行器等工具，简化应用开发、测试和诊断的过程。

## nodeJS 

在 idea 上直接运行 js 文件，当 WebStorem 一样使用

## Vue.js



## GitToolBox



## grep-console 

控制台彩色日志输出（根据不同的日志等级）

## maven-search

快速查找maven依赖，支持gav模糊查找、支持类名查找jar.  
快捷键 ：Shift + Control + M 或者 Shift + Command + M  
