# 一、人工智能AI

**人工智能AI** 包含 **机器学习ML** 包含 **深度学习DL**

# 二、机器学习流程：

1、数据获取；

2、特征工程；（深度学习是机器学习的一部分，特征如何提取最难）

3、建立模型；搭建模型是要适应数据集的大小。CNN最好是channle不断递增，shape不断减少。

4、评估与应用；

深度学习：学什么样的特征是最合适的，怎样去提特征。

## 机器学习常规套路：

1、收集数据并给定标签；

2、训练一个分类器；

3、测试、评估。

## 一个典型的神经网络训练过程包括以下几点：

输入-卷积层-池化层-全连接层-输出

1.定义一个包含可训练参数的神经网络

2.迭代整个输入

3.通过神经网络处理输入

4.计算损失(loss)

5.反向传播梯度到神经网络的参数

6.更新网络的参数（最简单的更新规则就是随机梯度下降），典型的用一个简单的更新方法：weight = weight - learning\_rate \*gradient

一个典型的 CNN 结构看起来是这样的：

Input -> Conv -> ReLU -> Conv -> ReLU -> Pool -> ReLU -> Conv -> ReLU -> Pool -> Fully Connected

即：**输入-卷积-ReLU-卷积-ReLU-池化-ReLU-卷积-ReLU-池化-全连接**

卷积神经网络-百科： <https://baike.baidu.com/item/%E5%8D%B7%E7%A7%AF%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C/17541100?fr=ge_ala>

# 三、深度学习的应用

无人驾驶汽车，计算机视觉，自然语言处理，人脸识别，医学。

计算机视觉任务：图像分类。一张图片被表示成**三维数组**的形式，每个像素的值从0到255。面临的挑战：照射角度，形状改变，部分遮蔽，背景混入。

下载各类离线：

[Index of /anaconda/cloud/pytorch/linux-64/ | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/linux-64/)
