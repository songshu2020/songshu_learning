云博士的AI课堂
29种本地部署大模型和调用的工具平台分类与总结 原创

2024-05-01 20:46:54
阅读量9.4k  收藏51   28赞

码龄18年

目前为止，下面是常用的各种大模型本地部署工具和平台，按照不同性质可以分为以下几类和描述。

# （1）综合部署与服务管理平台

​       这一类包括提供从模型训练到部署、管理和服务化的一站式解决方案的平台。这些工具通常支持模型的全生命周期管理，适用于需要高度集成和自动化的企业级部署。

主要包括：Ollama，LM Studio，Ray Serve，GPT4ALL，vLLM，HuggingFace TGI，OpenLLM，LMDeploy, FastChat, LangChain。

# （2）模型推理优化工具

此类工具专注于提高模型的推理效率，通过硬件加速、算法优化等方式减少推理时间和资源消耗，适用于性能敏感的应用。

主要包括：TensorRT-LIm, FasterTransformer, DeepSpeed-MII, CTranslate2，FlexFlow Server MLC LLM，XInference。

# （3）专用/特定任务模型部署框架

这些工具通常针对特定的业务场景或模型类型进行优化，提供特定领域解决方案。

主要包括：H2OGPT, PrivateGPT, Text Generation Inference，mlc-llm，QMoE。

# （4）通用的机器学习和深度学习库

这类库提供广泛的模型支持和开发工具，使开发者能够轻松地访问、训练和部署各种预训练模型。

主要包括：PyTorch Transformer库，Hugging Face Transformers。

# （5）特定语言实现

针对特定编程语言优化的工具，通常提供了更好的性能和更深的系统集成能力。目前主要是C或C++语言。

主要包括：llama.cpp，koboldcpp，PowerInfer，chatglm.cpp，qwen.cpp。

其中最方便的人人都可以部署的三种傻瓜方式包括Ollama，GPT4ALL和LM Studio，都支持window，MAC和linux操作系统，搜索官网直接下载安装应用即可。 

  下面对29种每个工具或平台进行一个概述。

1. Ollama
     特色描述：
       高度定制化服务：为企业提供根据其特定需求定制化的模型解决方案。
       强大的集成能力：支持与现有系统的无缝集成，提供丰富的API支持。
       实时优化和调整：平台支持模型在生产环境中的实时优化和动态调整。
2. LM Studio
     特色描述：
       图形用户界面：提供直观的图形界面，简化模型管理和部署过程。
       预训练模型库：包含丰富的预训练模型，支持快速部署和易于使用。
       团队协作工具：支持多用户协作，便于团队成员共同开发和管理模型。
3. Ray Serve
     特色描述：
       分布式架构：支持高效的分布式部署，轻松处理大规模并发请求。
       框架无关：可以与多种机器学习框架集成，如TensorFlow, PyTorch等。
       易于扩展：设计用于支持容易扩展和维护的部署。
4. GPT4ALL
     特色描述：
       支持多语言：适合国际化部署，支持多种语言的GPT模型。
       灵活的扩展性：根据业务需求调整模型规模和计算资源。
       成本效率：提供多种定价策略，帮助用户有效控制成本。
5. vLLM
     特色描述：
       虚拟化资源管理：优化资源分配，提高模型运行效率。
       支持大规模部署：设计用于处理大规模模型的有效部署和管理。
       高度定制的服务：提供客户特定的解决方案，满足不同的业务需求。
6. HuggingFace TGI (Transformers Generative Inference)
     特色描述：
       易于接入：提供简单的API接入，支持快速部署Hugging Face库的模型。
       优化的推理性能：专为生成型任务优化，确保高效推理。
       广泛的模型支持：支持广泛的预训练生成模型，如GPT, BERT等。
7. OpenLLM
     特色描述：
       开放源代码：鼓励社区贡献和共享，促进创新和改进。
       灵活的部署选项：支持云端和本地部署，以满足不同的业务需求。
       支持多种模型格式：兼容多种模型格式，方便用户迁移和使用。
8. LMDeploy
     特色描述：
       一键部署：简化部署流程，支持一键部署到多个环境。
       自动化管理：提供自动化工具，帮助用户管理和监控部署的模型。
       高可用性：设计以确保部署的模型具有高可靠性和可用性。
9. FastChat
     特色描述：
       实时交互优化：专为需要快速响应的聊天应用优化。
       轻量级部署：低资源消耗，适合移动和嵌入式设备。
       易于集成：提供API和工具，便于与其他应用和服务集成。
10. LangChain
     特色描述：
     链式模型集成：支持将多个模型链式集成，创建复杂的AI应用。
     模块化设计：提供模块化组件，便于用户根据需求定制和扩展。
     自然语言处理专优：专注于提升自然语言处理任务的效果和效率。
11. TensorRT-LIm
      特色描述：
      专为NVIDIA GPU优化：提高在NVIDIA GPU上的运行效率，专门针对深度学习推理进行优化。
      降低延迟：通过优化神经网络的图结构来减少推理时间。
      提升吞吐量：支持批量处理和多流输入，有效提高处理速度和吞吐量。
12. FasterTransformer
      特色描述：
      Transformer模型专优：专门为Transformer架构设计的优化工具，提供更快的处理速度。
      支持多种硬件：兼容NVIDIA GPU，提供CUDA加速。
      高效率的推理：优化了Transformer的注意力机制和层处理，提高了运行效率。
13. DeepSpeed-MII
      特色描述：
      超大规模模型支持：适用于训练和推理超大规模深度学习模型，如多亿参数的Transformer模型。
      资源优化：显著减少模型运行所需的内存和计算资源。
      易于集成：提供与现有深度学习框架如PyTorch的兼容性。
14. CTranslate2
      特色描述：
      机器翻译专用：专为机器翻译任务设计，提供高效的推理引擎。
      支持多种框架：兼容OpenNMT模型，支持从PyTorch和TensorFlow转换。
      高性能翻译：优化了翻译速度和资源使用，提高翻译质量。
15. FlexFlow Server
      特色描述：
      并行训练优化：通过灵活的并行策略自动优化深度学习模型的并行训练。
      动态调整：根据硬件和任务动态调整并行策略，以达到最优性能。
      支持多种硬件平台：兼容多种GPU和CPU配置，增强其通用性和适用范围。
16. MLC LLM
      特色描述：
      针对大型语言模型优化：专为大型语言模型如GPT-3等设计的推理加速。
      高效处理：优化处理流程，减少延迟，提升响应速度。
      支持多平台部署：适用于云端和本地部署，提供灵活的部署选项。
17. XInference
      特色描述：
      跨框架支持：支持多种深度学习框架，如TensorFlow, PyTorch等。
      自动化优化：自动优化模型推理路径，提高效率和减少资源消耗。
      适用于多种应用：能够处理从图像识别到自然语言处理等多种类型的AI任务。
18. H2OGPT
      特色描述：
      企业级部署：专为企业环境优化的GPT模型，提供稳定和可扩展的部署选项。
      定制化模型训练：支持企业特定数据的模型定制，确保模型输出与业务需求高度相关。
      集成学习能力：集成其他H2O.ai产品的机器学习功能，增强模型的智能和适用性。
19. PrivateGPT
      特色描述：
      隐私保护：使用先进的加密和隐私保护技术，确保敏感数据在训练和推理过程中的安全。
      适用于敏感领域：特别适合在医疗、法律和金融等隐私要求高的行业使用。
      高度兼容性：支持与现有安全框架和政策的兼容，无缝集成进企业现有系统。
20. Text Generation Inference
      特色描述：
      专为文本生成优化：提供针对各种文本生成任务（如聊天机器人、内容创作等）的优化推理解决方案。
      高效率推理：优化算法以减少生成任务的延迟，提高响应速度。
      支持多种语言模型：可适用于多种预训练语言模型，如GPT, BERT等。
21. mlc-llm
      特色描述：
      针对大型语言模型的优化：专为处理大型语言模型设计，提高推理效率和速度。
      低延迟推理：优化模型架构和计算流程，以实现低延迟的模型推理。
      灵活部署：支持云和本地部署，满足不同环境下的需求。
22. QMoE (Quality Mixture of Experts)
      特色描述：
      专家系统集成：通过组合多个“专家”模型来提高特定任务的处理质量和效率。
      动态路由算法：使用动态路由算法自动决定哪个专家模型在特定情况下最适合处理任务。
      适用于复杂多任务：特别适用于需要处理多种任务或有复杂需求的应用场景。
23. PyTorch Transformer库
      特色描述：
      广泛的模型支持：支持多种Transformer架构，包括BERT、GPT等流行模型。
      灵活性与可扩展性：允许研究人员和开发者自定义和扩展模型，适应不同的研究需求和应用场景。
      强大的社区和资源：得益于PyTorch的广泛使用，这个库享有丰富的教程、工具和社区支持。
24. Hugging Face Transformers
      特色描述：
      丰富的预训练模型库：提供超过数千种预训练模型，涵盖多种语言和任务。
      易于使用的API：简化了模型下载、训练和部署的过程，使得AI技术更加易于接入。
      框架兼容：支持PyTorch、TensorFlow和JAX，适用于各种机器学习项目。
25. llama.cpp
      特色描述：
      针对C++优化：在C++环境中提供高效的执行，适合需要嵌入式系统或资源受限环境的应用。
      性能高效：通过底层优化确保在高性能计算场景中的效率。
      易于集成：设计简洁，易于与其他C++项目和系统集成。
26. koboldcpp
      特色描述：
      模块化设计：高度模块化的架构，便于用户根据需求进行自定义和扩展。
      跨平台支持：支持多种操作系统，提供灵活的部署选项。
      社区支持：虽为较新的工具，但已开始建立一个活跃的用户和开发者社区。
27. PowerInfer
      特色描述：
      针对推理优化：专门设计以提高深度学习模型在生产环境中的推理性能。
      支持多种硬件：优化了对不同硬件平台的支持，包括GPU和CPU。
      高性能计算：为需要极端计算性能的应用场景提供支持。
28. chatglm.cpp
      特色描述：
      专为聊天模型设计：优化聊天和对话系统的模型推理，提高响应速度和交互质量。
      C++实现：在C++环境中实现，保证了高效的性能和良好的系统兼容性。
      可定制性：提供灵活的接口，允许开发者根据具体需求调整模型行为和参数。
29. qwen.cpp
      特色描述：
      通用性和灵活性：设计为通用的机器学习部署工具，可适用于多种模型和任务。
      高效的数据处理：优化数据处理流程，确保即使在大数据环境下也能保持高效性能。
      强大的扩展性：支持用户根据项目需求添加新功能或改进现有功能。



# 大模型的模型格式及优缺点等信息

你在魔搭（ModelScope）下载的模型文件后缀为 `.safetensor`，这是一种由 Hugging Face 社区提出的**安全张量存储格式**。以下从模型格式分类、适用工具及部署方案等方面详细说明：

---

### **一、常见大模型格式及特点**
| **格式**         | **说明**                                                     | **典型用途**              | **代表工具支持**                                       |
| ---------------- | ------------------------------------------------------------ | ------------------------- | ------------------------------------------------------ |
| **.safetensors** | Hugging Face 推出的安全格式，避免代码执行漏洞，仅存储张量数据，**不支持恶意代码** | 安全分发 PyTorch 模型     | Hugging Face Transformers, vLLM, text-generation-webui |
| **.bin/.pth**    | PyTorch 原生权重格式，可能包含完整模型或分片权重             | 原生 PyTor 训练/推理      | PyTorch, vLLM, FastChat                                |
| **.gguf**        | llama.cpp 推出的量化格式，支持元数据嵌入，替代旧版 .ggml     | 低资源本地推理            | Ollama, llama.cpp, kobold.cpp                          |
| **.h5/.keras**   | TensorFlow/Keras 的 HDF5 格式，适合完整模型保存              | TensorFlow 生态训练       | TensorFlow Serving, TFLite                             |
| **.onnx**        | 跨框架标准化格式，支持硬件加速优化                           | 跨平台部署（CPU/GPU/TPU） | ONNX Runtime, TensorRT                                 |
| **.pt**          | PyTorch 序列化文件，可能包含模型+代码（需谨慎加载）          | 完整模型保存（含架构）    | PyTorch, TorchServe                                    |

---

### **二、.safetensors 格式详解**
#### **1. 核心特性**
- **安全性**：不存储 Python 代码，防止反序列化攻击（如 Pickle 漏洞）
- **高效性**：零拷贝加载，支持分片存储大模型
- **兼容性**：与 Hugging Face 生态无缝集成（需配合 `config.json` 使用）

#### **2. 典型部署工具**
- **直接加载工具**：
  - **Hugging Face Transformers**：通过 `from_pretrained()` 直接加载
    ```python
    from transformers import AutoModel
    model = AutoModel.from_pretrained("path/to/model", use_safetensors=True)
    ```
  - **vLLM**：支持加载 `.safetensors` 分片模型
    ```bash
    python -m vllm.entrypoints.api_server \
        --model path/to/model \
        --tensor-parallel-size 2
    ```
  - **text-generation-webui**：通过界面直接选择 `.safetensors` 文件

- **格式转换工具**：
  - 转换为 PyTorch 格式：
    ```python
    from safetensors import safe_open
    from torch import save
    
    with safe_open("model.safetensors", framework="pt") as f:
        state_dict = f.keys()
    save(state_dict, "pytorch_model.bin")
    ```
  - 转换为 GGUF（供 Ollama 使用）：
    ```bash
    python llama.cpp/convert.py --input-model model.safetensors --outfile model.gguf
    ```

---

### **三、不同格式对应部署工具推荐**
| **模型格式**     | **适用工具**                    | **硬件要求**            | **典型场景**       |
| ---------------- | ------------------------------- | ----------------------- | ------------------ |
| **.safetensors** | vLLM, Hugging Face Transformers | GPU（推荐 >=16GB）      | 生产环境 API 服务  |
| **.gguf**        | Ollama, llama.cpp               | CPU/低显存 GPU          | 个人本地交互式使用 |
| **.bin/.pth**    | PyTorch, FastChat               | GPU（需完整显存）       | 研究/自定义训练    |
| **.onnx**        | ONNX Runtime, TensorRT          | 多平台加速（如 Jetson） | 边缘设备部署       |

---

### **四、关键部署流程对比**
#### **1. 直接使用 .safetensors**
- **优点**：无需转换，直接利用 Hugging Face 生态
- **步骤示例（vLLM）**：
  1. 下载模型文件（含 `model.safetensors` 和 `config.json`）
  2. 启动 API 服务：
     ```bash
     python -m vllm.entrypoints.api_server \
         --model /path/to/model_dir \
         --trust-remote-code \
         --max-model-len 4096
     ```
  3. 调用 REST API：
     ```bash
     curl http://localhost:8000/generate \
         -d '{"prompt": "Hello!", "max_tokens": 50}'
     ```

#### **2. 转换为 GGUF 供 Ollama 使用**
- **优点**：显著降低显存占用，适合消费级硬件
- **转换步骤**：
  1. 安装转换工具：
     ```bash
     pip install llama-cpp-python[convert]
     ```
  2. 执行量化转换：
     ```bash
     python -m llama_cpp.convert \
         --input-model /path/to/model.safetensors \
         --output-model model-q4_k.gguf \
         --quantize q4_k
     ```
  3. 创建 Ollama Modelfile：
     ```dockerfile
     FROM ./model-q4_k.gguf
     PARAMETER temperature 0.7
     ```
  4. 构建并运行：
     ```bash
     ollama create my-model -f Modelfile
     ollama run my-model
     ```

---

### **五、选型建议**
1. **优先保持原生格式**：
   - 若使用 vLLM 或 Transformers，直接使用 `.safetensors` 避免转换损失
   - 需要高性能推理时，配合 FlashAttention-2 等优化
2. **需要低资源部署**：
   - 转换为 GGUF 格式，使用 Ollama 或 llama.cpp
   - 量化等级选择（q4_k vs q8_0 的精度/速度权衡）
3. **企业级扩展需求**：
   - ONNX + TensorRT 实现极致优化
   - 使用 vLLM 的 Tensor Parallelism 实现多卡扩展

---

### **六、模型格式转换工具链**
- **safetensors ↔ PyTorch**：
  - 官方工具：`safetensors.torch.load_file()` / `save_file()`
- **HuggingFace → GGUF**：
  - `llama.cpp` 的 `convert.py`（支持 LLaMA/Mistral 架构）
- **PyTorch → ONNX**：
  - `torch.onnx.export()` + 优化脚本
- **TensorFlow → PyTorch**：
  - 使用 `transformers` 库的 `convert_tf_checkpoint_to_pytorch`

---

### **总结**
- `.safetensors` 是当前 Hugging Face 生态推荐的**安全权重格式**，适合直接用于 vLLM 或 Transformers 部署
- 若需在**低配置设备运行**，转换为 GGUF 格式并使用 Ollama/llama.cpp 是更优选择
- 工具链的灵活性使得不同格式可以相互转换，建议根据硬件条件和延迟要求灵活选择方案