# 各类学习笔记

记录了本人工作学习过程中的笔记及心得。

中间件，通常指“应用中间件”，是一种**独立的系统软件或服务程序，能“独立部署和运行”，且处于“应用程序”和“基础设施或运行环境”之间，不直接处理业务逻辑**，而是为上层“使用广泛的应用程序”提供“运行时服务”。补充一点：大部分中间件是通过网络与应用程序连接。

根据这个定义，可以确定和排除很多的东西，比如:

- 排除Dubbo，Spring，gRPC，因为不是独立部署和运行的软件或服务，应该归类为应用框架/库。


- 排除Maven和NPM，一是因为它们不能独立运行，二是因为它们不是为应用程序提供运行时服务，应该归类为**研发工具**。


- 排除NodeJS和Java，因为它们是应用程序运行的基础环境，而自身不能独立运行，所以应该归类为**基础环境。**


- 排除Git、Gitlab、Nexus，因为不是为应用程序提供运行时服务，可以归类为**研发工具/平台**。


- 排除PostMan、IBM ClearCase、JMeter等，因为不是为应用程序提供运行时服务，可以归类为**测试工具/平台**。


- 排除Jenkins，Logstash，因为不是为应用程序提供运行时服务，可以归类为**运维工具/平台**。


另外，有一些比较难区分：

- 像Tomcat、Nginx，比较特殊（不是通过网络与应用程序连接），但由于是专门为应用程序提供运行支持，所以也属于特殊中间件，只是更接近于基础设施。


- 而NodeJS的进程管理工具PM2，虽然也是为应用程序提供运行支持，但是更偏底层（应用无感知和交互），所以归类为基础设施比较好。


- Cinder/Ceph/NAS块存储软件，主要应用场景，已经抽象成了基础设施（块存储），应用程序没有直接调用Cinder/Ceph的API，而是通过操作系统API间接去访问，所以归类为基础设施比较好。同理，Calico等网络组件，也是基础设施。


- 但是对象存储或者偏对象存储使用的存储软件，例如FastDFS、SeaweadFS，归类为中间件比较好。


而所谓的**“软件基础设施和运行环境**”，是**指操作系统，块存储、网络组件，以及其他直接或间接为应用程序“提供运行条件或底层网络、存储及硬件相关功能”的软件或服务程序**。下面举例分析，有一些比较容易混淆：

- 像Docker、Kubernetes，VMWare等，属于基础设施。


- JDK、NodeJS等属于基础设施。


- 上面提到的PM2，以及iptables、LVS等组件，属于基础设施。


- 像Zabbix、Prometheus，不属于基础设施，因为它们不属于常规意义上为应用程序“提供运行条件或底层网络和存储相关功能”的软件。将其归类为运维工具比较好（但若是将Prometheus当做时序数据库来使用，则可以归为中间件）。


- 显卡驱动，属于基础设施。


**注意**，基础设施，包括软件基础设施 和 硬件基础设施。其中，软件基础设施，包括了操作系统、存储管理软件和网络管理软件，包括了管理硬件的驱动及嵌入式软件。

综上，IT公司所有的软件/程序，可归类为：

- 应用程序/平台（面向客户的）


- 研发工具/平台（面向开发、运维、测试等使用的工具/平台）


- 应用中间件


- 软件基础设施

## 目录

### 开源协议种类及注意事项

[开源协议种类及注意事项](./开源协议种类及注意事项.md)

### ascii码表

- [ascii码表](./ascii/README-ASCII.md) - ascii码表

### 设计模式      

- [designpattrens ](./designpattrens/README-DESIGNPATTERNS.md) - designpattrens  

### java_learning   

- [DWR ](./java_learning/DWR框架学习.md) - DWR框架学习
- [Ant之build.xml详解](./java_learning/Ant之build.xml详解.md)
- [web.xml文件中的listener-filter-servlet加载顺序及其详解](./java_learning/web.xml中的listener-filter-servlet加载顺序及其详解.md)
- [注解（Annotation）自定义](./java_learning/注解自定义.md)
- [spring507 ](./java_learning/spring507/README-SPRING.md) - spring5.0.7版本学习内容
- [springmvc](./java_learning/springmvc.md)
- [mybatis ](./java_learning/mybatis/README-mybatis.md)  - mybatis框架学习
- [jdk method ](./java_learning/jdk_method/README-jdkMethod.md)  - jdk method，java方法相关学习
- [SpringCloud学习笔记](./java_learning/spring507/SpringCloud学习笔记.md)  - gateway,SpringCloud
- [其他内容 ](./java_learning/README-OTHERS.md)  - 其他内容(jvm、jconsole等)

### 基础设施/运行环境

#### 微服务架构

[微服务架构](./IaaS/微服务架构.md) - 概念，服务架构的演进，微服务架构的优缺点介绍，什么情况下采用微服务架构

### 操作系统   
- [linux ](./OS/Linux/README-linux.md) - linux常用命令、各类linux系统学习
- [unix ](./OS/Unix/README-unix.md) -unix
- [windows ](./OS/Windows/README-WINDOES.md) --windows
- [vi命令的用法-AIX相关操作](OS/vi命令的用法-AIX相关操作.md)

### 中间件

#### apache

- [apache](./Middleware/apache/apache安装配置文档.md)

#### docker

- [docker ](./Middleware/docker/README-DOCKER.md)  

#### 数据库  

- [mysql数据库 ](./Middleware/mysql/README-MYSQL.md)
- [国产数据库](./Middleware/database/国产数据库.md)

#### nginx

- [nginx](./Middleware/nginx/NGINX.md)  

#### oracle

- [oracle](./Middleware/oracle/oracle.md)  - oracle相关学习手册

#### Prometheus+grafana

- [Prometheus+grafana](./Middleware/prometheus+grafana/Prometheus.md)  普罗米修斯和grafana学习笔记

#### redis

- [redis](./Middleware/redis/README-REDIS.md)

#### tomcat

- [tomcat ](./Middleware/tomcat/README-TOMCAT.md)  

#### weblogic

- weblogic   - weblogic安装配置调优.doc 

#### Zookeeper

- [Zookeeper](./Middleware/Zookeeper/Zookeeper.md)

#### tuxedo

- [tuxedo](./Middleware/tuxedo/tuxedo.md)

#### MQ

- [MQ学习随笔](./Middleware/MQ/MQ学习随笔.md)
- [WebSphere-MQ入门指南](./Middleware/MQ/WebSphere-MQ入门指南.md)

#### Elasticsearch

- [elasticsearch常用应用场景](./Middleware/elasticsearch/elasticsearch常用应用场景.md)



### protocol-各类协议

- [protocol](./protocol/README-PROTOCOL.md) - 各类协议学习-RTC-webRTC
- [网络流媒体协议相关](./protocol/网络流媒体协议相关.md) -RTC-RTP-RTCP-RTSP-RTMP-HLS
- [SNMP协议](./protocol/SNMP协议学习.md)

### tools-工具相关

淘宝镜像，网址为：https://registry.npmmirror.com/binary.html， 可以下载python解释器、node、chrome及chromedriver、edgedriver等

- [Iass-Pass-Sass.md](./tools工具相关/Iass-Pass-Sass.md)
- [MyEclipse快捷键](./tools工具相关/MyEclipse快捷键.md)
- [各类工具](./tools工具相关/README-tools.md) - Jenkins-docker- 服务注册与发现
- [IDEA使用总结](./tools工具相关/IDEA使用总结.md)    包括IDEA好用插件、快捷键记录等
- [maven学习相关](./tools工具相关/maven学习相关.md)
- [网络抓包](./tools工具相关/网络抓包.md)
- [加解密、签名验签](./README-encry-sign.md)
- [Visual-Studio-Code使用总结](./tools工具相关/Visual-Studio-Code使用总结.md)  包括好用插件、对接大模型编程等
- [正则表达式学习记录](./tools工具相关/正则表达式学习记录.md)
- [国内常用镜像站地址](./国内常用镜像站地址.md) 



### ML-AIGC-（机器学习AI）

[人工智能](ML-AIGC/人工智能.md)  人工智能基本知识等学习总结

```tex
LLM是“大语言模型”（Large Language Model）的缩写，是一种利用大量数据训练的、能够理解和生成自然语言文本的深度学习模型。
```

[大语言模型LLM](ML-AIGC/大语言模型LLM.md)   自己对LLM及二分类的学习笔记

[本地部署大语言模型LLM](ML-AIGC/本地部署大语言模型LLM.md)   本地部署LLM，摘自CSDN

[机器学习自学总结（拷贝gitee）](ML-AIGC/机器学习自学总结（拷贝gitee）.md)

【有道云笔记】[机器学习.mindmap](https://note.youdao.com/s/6ZLcTjp1)

[学术资源导航（免费数据库）](./ML-AIGC/学术资源导航-免费数据库.md) 

[变现全路径手册（工具包拆解）](./ML-AIGC/变现全路径手册.md)
