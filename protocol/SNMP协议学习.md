# SNMP协议学习

**SNMP（Simple Network Management Protocol，简单网络管理协议）** 是一种用于管理和监控网络设备的协议。它允许网络管理员收集、组织和修改网络设备（如路由器、交换机、服务器、打印机等）的信息，并支持对这些设备进行远程管理和故障排查。

---

### SNMP 的核心概念

1. **管理站（Manager）**：
   - 运行 SNMP 管理软件的设备，用于监控和管理网络设备。
   - 例如：网络监控系统（如 PRTG、Zabbix、Nagios）。

2. **代理（Agent）**：
   - 运行在被管理设备上的软件，负责收集设备信息并响应管理站的请求。
   - 例如：路由器、交换机、服务器等设备上的 SNMP 服务。

3. **管理信息库（MIB，Management Information Base）**：
   - 一个层次化的数据库，定义了被管理设备的各种信息（如 CPU 使用率、内存状态、接口流量等）。
   - 每个信息项通过唯一的 **OID（Object Identifier，对象标识符）** 标识。

4. **OID（Object Identifier）**：
   - 一个唯一的标识符，用于表示 MIB 中的特定信息。
   - 例如：`1.3.6.1.2.1.1.1` 表示设备的系统描述。

5. **SNMP 操作**：
   - **GET**：管理站从代理获取信息。
   - **SET**：管理站修改代理的配置或状态。
   - **TRAP**：代理主动向管理站发送告警信息。
   - **GETNEXT**：管理站获取下一个 OID 的信息。
   - **GETBULK**：管理站批量获取信息（SNMPv2c 和 SNMPv3 支持）。

---

### SNMP 的版本

1. **SNMPv1**：
   - 最早的版本，功能简单，安全性较低（使用社区字符串作为认证）。
   - 仅支持 GET、SET 和 TRAP 操作。

2. **SNMPv2c**：
   - 增加了批量操作（GETBULK）和更多的错误码。
   - 仍然使用社区字符串作为认证，安全性较低。

3. **SNMPv3**：
   - 增加了安全性功能，支持身份验证（Authentication）和加密（Encryption）。
   - 提供了更细粒度的访问控制。

---

### SNMP 的典型应用

1. **网络监控**：
   - 监控网络设备的性能指标（如带宽、CPU 使用率、内存使用率等）。
   - 例如：PRTG、Zabbix 等工具通过 SNMP 收集设备数据。

2. **故障排查**：
   - 通过 TRAP 消息实时接收设备的告警信息。
   - 例如：设备宕机、接口故障等。

3. **配置管理**：
   - 远程修改设备的配置（如修改路由表、启用/禁用接口等）。

4. **资产管理**：
   - 收集设备的硬件和软件信息（如型号、序列号、固件版本等）。

---

### SNMP 的安全性

- **SNMPv1 和 SNMPv2c**：
  - 使用社区字符串（Community String）作为认证，默认值为 `public`（只读）和 `private`（读写）。
  - 社区字符串以明文传输，容易被窃听和伪造。

- **SNMPv3**：
  - 支持用户身份验证（如 MD5、SHA）和数据加密（如 DES、AES）。
  - 提供更安全的访问控制机制。

---

### 示例：SNMP 命令

1. **使用 `snmpget` 获取设备信息**：
   ```bash
   snmpget -v2c -c public 192.168.1.1 1.3.6.1.2.1.1.1.0
   ```
   - 获取设备的系统描述。

2. **使用 `snmptrap` 发送 TRAP 消息**：
   ```bash
   snmptrap -v2c -c public 192.168.1.100 '' 1.3.6.1.4.1.12345.1.0.1 1.3.6.1.4.1.12345.1.0.1 s "High CPU usage"
   ```
   - 向管理站发送一条自定义的 TRAP 消息。

---

### 总结
SNMP 是网络管理的重要工具，广泛应用于设备监控、故障排查和配置管理。不同版本的 SNMP 在功能和安全性上有所差异，推荐使用 **SNMPv3** 以提高安全性。





以下内容摘自：  https://mp.weixin.qq.com/s/_P1qtHvUXho-m79HIJVRVw

## 一.SNMP 协议

### 1.协议介绍

snmp 协议是日常使用的较多的一种协议，绝大多数网络设备/存储等都支持 snmp 协议，通过此协议可以实现设备状态的监控及管理。

### 2.主要组成

SNMP 协议包括以下三个部分

![图片](./640.png)

**SNMP Agent**：负责处理 snmp 请求，主要包括 get，set 等操作。可通过此接口查询设备的运行状态（使用较多），或者变更配置（使用较少），默认使用 UDP 161 端口  
**SNMP Trap**： snmp 通知消息，主动发送消息到管理端。如设备故障，端口 down 等都会实时发送消息到接收端。默认使用 UDP 162 端口  
**SNMP MIB**：MIB 代表管理信息库，是按层次结构组织的信息的集合,定义了设备内被管理对象的属性。  

## 二.SNMPTrap 通知

### 1.SNMPTT 介绍

SNMPTT (SNMP Trap Translator) 是一个 perl 语言编写的用来处理 snmptrap 消息的程序，可与 snmptrapd 程序（www.net-snmp.org）一起使用，基本流程如下[1]

![图片](641.png)

snmptt 负责处理 net-snmp 接收到的 trap 消息，通过 snmptt 命令或 snmptthandler 进行处理，解析消息之后可将消息写入文件或数据库。

### 2.安装 SNMPTT

操作系统为 Debian 12 x64，使用 apt 方式安装 snmptt，安装 snmptt 及相关组件

```bash
apt-get install snmptt libnet-syslogd-perl libnet-ip-perl
```

即可完成安装。

### 3.MIB文件

snmp trap 消息如果不翻译,原始内容可能是这样的

```
{
  "Version":2,
"TrapType":0,
"OID":null,
"Other":null,
"Community":"public",
"Username":"",
"Address":"172.16.1.64:49692",
"VarBinds":{
".1.3.6.1.2.1.1.3.0":7908527690000000,
".1.3.6.1.2.1.2.2.1.1.18":18,
".1.3.6.1.2.1.2.2.1.2.18":"Vlanif103",
".1.3.6.1.2.1.2.2.1.7.18":2,
".1.3.6.1.2.1.2.2.1.8.18":2,
".1.3.6.1.6.3.1.1.4.1.0":[1,3,6,1,6,3,1,1,5,3]
},
"VarBindOIDs":[
".1.3.6.1.2.1.1.3.0",
".1.3.6.1.6.3.1.1.4.1.0",
".1.3.6.1.2.1.2.2.1.1.18",
".1.3.6.1.2.1.2.2.1.7.18",
".1.3.6.1.2.1.2.2.1.8.18",
".1.3.6.1.2.1.2.2.1.2.18"
]
}
```

如果需要知道上述 oid 的具体内容及数值意义就需要对照 MIB 文件对消息进行“翻译”。snmptt 需要加载对应配置文件，对 trap 消息进行翻译。 snmptt 提供了 snmpttconvertmib 工具，snmpttconvertmib 是一个 Perl 脚本，它将读取 MIB 文件并将 TRAP-TYPE（v1）或 NOTIFICATIONATION-TYPE（v2）定义转换为 SNMPTT 可读的配置文件，从而实现对 trap 消息的翻译，基本命令如下：

```bash
/usr/bin/snmpttconvertmib --in=/usr/share/snmp/mibs/CPQHOST.mib --out=/etc/snmp/snmptt.conf.compaq --net_snmp_perl
```

上传snmptrap的 mib 文件到/usr/share/snmp/mibs/ 本例为CPQHOST.mib， /etc/snmp/snmptt.cong.compaq为转换完输出的配置文件

由于一般情况设备 mib 可能有多个，建议转换为一个配置文件中，便于管理，可使用以下命令进行批量转换。

```bash
for i in HUAWEI*
do
/usr/bin/snmpttconvertmib --in=$i --out=/etc/snmp/snmptt.conf.huawei --net_snmp_perl
done
```

转换完成后，可配置为snmptt的解析文件。

### 4.配置 snmptt

snmptt 配置文件有 2 个

- /etc/snmp/snmptt.ini snmptt 主要配置文件
- /etc/snmp/snmptt.conf 系统默认的策略文件，包括一些基本的端口 up/down 的配置

修改默认的 snmptt.ini 配置文件，主要修改以下几个地方

```
mode = standalone
multiple_event = 0
net_snmp_perl_enable = 1
translate_log_trap_oid = 1
syslog_enable = 0
```

在文件末尾可添加snmpttconvertmib转换完成的规则文件

```
[TrapFiles]
# A list of snmptt.conf files (this is NOT the snmptrapd.conf file).  The COMPLETE path
# and filename.  Ex: '/etc/snmp/snmptt.conf'
snmptt_conf_files = <<END
/etc/snmp/snmptt.conf
/etc/snmp/snmptt.cong.compaq
END
```

snmptt 有二种模式，分别为

- standalone 每次收到 trap 消息时读取 snmptt.ini 配置文件调用 snmptthandler 来处理 trap 消息
- daemon 顾名思义守护进程方式，启动 snmptrap 时，初始化时一次性读取 snmptt.ini 二种方式各有区别，选择使用即可，具体区别可查看官方说明 http://snmptt.sourceforge.net/docs/snmptt.shtml#Installation-Unix

由于这里测试使用，经常修改 snmptt.ini 配置文件，如果使用 daemon 模式，那么每次修改 snmptt.ini 配置文件就需要重启 snmptt，因此这里我使用 standalone 模式。

### 5.配置 snmptrapd

snmptrap 消息为主动通知，因此需要配置服务来接收设备发送过来的trap 消息。snmptrapd接收trap消息后，通过 traphandle 调用 snmptt 来对 trap 消息进行处理。 安装snmptrapd

```
apt-get install snmptrapd -y
```

修改/etc/snmp/snmptrapd.conf 配置文件,添加如下

```
disableAuthorization yes
traphandle default /usr/sbin/snmptt
```

启动 snmptrap 服务

```
systemctl start snmptrapd
systemctl enable snmptrapd
```

### 6.规则配置

通常一条trap规则结构如下

```
EVENT hwSecLOGINSucced .1.3.6.1.4.1.2011.6.122.62.2.1"Status Events"Normal
FORMAT $aA $*
SDESC

The user login succeeded.
Variables:
1: hwSecLOGINUser
Syntax="OCTETSTR"
Descr="The user name."
2: hwSecLOGINIP
Syntax="OCTETSTR"
Descr="The User IP address."
3: hwSecLOGINTime
Syntax="OCTETSTR"
Descr="The User login time."
4: hwSecLOGINType
Syntax="OCTETSTR"
Descr="The User access type."
5: hwSecLOGINLevel
Syntax="INTEGER32"
Descr="The User login level."
EDESC
......
```

-  EVENT开头为事件类型，以及对应的snmptrap oid，Normal为事件级别
-  FORMAT 为按照snmptt定义的内部变量进行snmptrap告警内容的格式化
-  SDESC到EDESC为事件描述
-  EXEC 收到事件后可调用外部脚本

这里以配置交换机登录告警为例子，配置一个Trap规则，添加如内容到snmptt默认规则文件/etc/snmp/snmptt.conf末尾

```
EVENT LOGINSucced .1.3.6.1.4.1.2011.5.25.207.2.0.2 "Status Events" Warning
FORMAT 登录设备成功!用户名:$1登录ip:$2用户界面:$3认证方式:$4
EXEC /usr/local/bin/send_snmptt_alert.sh snmptrap "$s" "$aR" "$o" "$Fz"
SDESC
LOGINSucced 
EDESC
```

规则解释

-  .1.3.6.1.4.1.2011.5.25.207.2.0.2 为事件oid，Warning为告警级别（与Flashduty平台对应，枚举值：Critical：严重，Warning：警告，Info：提醒，Ok：恢复）
-  FORMAT格式化成可读的语言，$为消息变量
-  EXEC 执行脚本

收到此trap消息后，会执行/usr/local/bin/send_snmptt_alert.sh脚本，并传入后面的参数,此处参数为snmptt内置参数和 FROMAT 里通用，具体可在 http://snmptt.sourceforge.net/docs/snmptt.shtml#SNMPTT.CONF-FORMAT 查看，常用的几个如下：

-  $aA snmp trap 的代理 IP，即源 IP
-  $o 数字oid形式
-  $* 所有变量
-  $n n 为数字，表示变量次序

系统snmptrapd收到trap消息后会交给snmptt处理，snmptt根据配置的规则进行对应trap消息的处理工作。如未匹配到任何Trap规则,也可配置一条默认规则

```
EVENT default .* "Normal" "Warning"
EXEC /usr/local/bin/send_snmptt_alert.sh snmptrap "$s" "$aR" "$o" "$*"
```

未匹配到的任何规则的trap也会执行/usr/local/bin/send_snmptt_alert.sh脚本。